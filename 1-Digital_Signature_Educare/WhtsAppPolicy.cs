﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OracleClient;
using System.Net;
using System.IO;
using Newtonsoft.Json;


//[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Digital_Signature_Educare
{  
    public class WhtsAppPolicy
    {        
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");

        OracleConnection Con = null;
        OracleCommand Cmd = null;

        #region --------- Upload Policy Schduler PDF to AMCom --------------------
        public void UploadPolicyPDF(string PolicyHolderName, string PolicyNo, string MobileNo, string EmailID, string PolicyPDFPath)
        {            
            try
            {               
                string serviceURL = Convert.ToString(ConfigurationManager.AppSettings["AMCOM_PDFUploadService"].Trim());                              

                //---------- Call Upload File Service -----------------
                string[] result = UploadPolicyPDF(serviceURL, PolicyPDFPath, MobileNo);// EmailID);      // Upload Policy PDF to AWS server. 

                if (!string.IsNullOrEmpty(result[1]))  // "data" parameter from AMCom file upload service response contains path of Policy PDF on AWS server.
                {
                    //----- Push Message Service ----------------
                    WhatsAppPolicy(result, MobileNo, PolicyHolderName, PolicyNo);
                } 
            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp - Issue while Uploading Policy PDF : " + ex.Message);
            }
        }
        #endregion       

        public string[] UploadPolicyPDF(string requestURL, string fileName, string mobileNo)
        {
            string[] strResult = new string[3];
            try
            {
                //----------- Before consume the AMCom FileUpload service to upload Policy PDF DMS server to AWS server insert info. into DB -------               
                int dbInsertId = FileUploadInsertInDB(fileName, mobileNo); //emailId);

                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(fileName); // You need to do this download if your file is on any other server otherwise you can convert that file directly to bytes  
                Dictionary<string, object> postParameters = new Dictionary<string, object>();

                // Add your parameters here  
                //-------- Need to supply value one of three following parameters. ----1. emailId, 2. mobile or 3. userId---------
               // postParameters.Add("emailId", emailId);
                postParameters.Add("mobile", mobileNo);
                postParameters.Add("file", new FormUpload.FileParameter(bytes, Path.GetFileName(fileName), "application/pdf"));

                string userAgent = "Someone";   // not in use.
                FormUpload objFormUpload = new FormUpload();
                HttpWebResponse webResponse = objFormUpload.MultipartFormPost(requestURL, userAgent, postParameters); //, headerkey, headervalue);
                // Process response  
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                string returnResponseText = responseReader.ReadToEnd();
                webResponse.Close();

                dynamic results = JsonConvert.DeserializeObject<dynamic>(returnResponseText);

                strResult[0] = Convert.ToString(results["msg"]);
                strResult[1] = Convert.ToString(results["data"]);
                strResult[2] = Convert.ToString(dbInsertId);

                //------ After consume the AMCom FileUpload service to upload Policy PDF DMS server to AWS server update info. into DB --------           

                if (dbInsertId > 0)
                {
                    int dbUpdateRecordCount = FileUploadUpdateInDB(dbInsertId, strResult[0], strResult[1]);
                }                
            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp - Issue while copy Policy PDF DMS to AWS server :: " + fileName + " :: " + mobileNo + " :: " + ex.Message);
            }
            return strResult;
        }

        #region ------------ Send Policy Schduler PDF with other info to AMCom -------------------
        public void WhatsAppPolicy(string[] PDFResult, string MobileNo, string PolicyHolderName, string PolicyNo)
        {
            string serviceURL = Convert.ToString(ConfigurationManager.AppSettings["AMCOM_PDFPathService"].Trim());   
            string PDFUploadResult = "";
            int pushMessageID = 0;

            try
            {               
                StringBuilder jsonString = new StringBuilder();
                jsonString.Append(@"{""whatsapp"": {""unregisteredUsers"": [{""un_email"": """",""un_mobile"": """);
                jsonString.Append(MobileNo);
                jsonString.Append(@""",""templateKey"": ""PolicySchedule_1"",");
                jsonString.Append(@"""attachments"":{""fileName"": ""Policy Schedule"",""fileUrl"": """);
                jsonString.Append(PDFResult[1]);
                jsonString.Append(@"""},""params"":{""whiteList"":{""1"": """);
                jsonString.Append(PolicyHolderName);
                jsonString.Append(@""",""2"": """);
                jsonString.Append(PolicyNo);
                jsonString.Append(@""",""3"":""bit.ly/2xeq3ua""}}}]}}");

                //----------- Consume Stored Proc SP_WhatsApp_PM_Insert here to Insert  -----------                
                try
                {
                    pushMessageID = PushMessageInsertInDB(Convert.ToInt32(PDFResult[2]), jsonString.ToString(), PolicyHolderName, PolicyNo, MobileNo);
                }
                catch (Exception ex)
                {
                    logger.Error("WhatsApp Push Message service - Issue while inserting information into DB : " + ex.Message);
                }

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(serviceURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("serverKey", ConfigurationManager.AppSettings["ServerKey"].ToString());
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonString);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    PDFUploadResult = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp - Issue while sending information : " + ex.Message);
            }

            dynamic results = JsonConvert.DeserializeObject<dynamic>(PDFUploadResult);
            string strResult = Convert.ToString(results["msg"]);
            string strData = Convert.ToString(results["data"]);

            //--------- Consume Stored Proc SP_WhatsApp_PM_Update to Update -----------------
            try
            {
                if (pushMessageID > 0)
                {
                    int dbUpdateRecordCount = PushMessageUpdateInDB(pushMessageID, strResult, strData);
                }
            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp Push Message service - Issue while updating information into DB : " + ex.Message); 
            }            
        }
        #endregion

        public int FileUploadInsertInDB(string FileName, string MobileNo)
        {
            Con = new OracleConnection(ConfigurationManager.AppSettings["OracleApolloT"].ToString());
            Cmd = new OracleCommand();

            int PID = 0;
            try
            {             
                //------------ADO DOT NET ----------------                                             
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand("SP_WhatsApp_FU_Insert", Con);
                Cmd.CommandType = CommandType.StoredProcedure;                
                Cmd.Parameters.Add("p_FilePath", OracleType.NVarChar).Value = FileName;
                Cmd.Parameters.Add("p_MobileNo", OracleType.NVarChar).Value = MobileNo;            
                Cmd.Parameters.Add("p_ID", OracleType.Int32, 15);
                Cmd.Parameters["p_ID"].Direction = ParameterDirection.Output;               
                int row = Cmd.ExecuteNonQuery();             
                PID = Convert.ToInt32(Cmd.Parameters["p_ID"].Value.ToString());
            }
            catch (Exception ex)
            {                
                logger.Error("WhatsApp File Upload to AWS service - got issue while inserting information into DB :: " + FileName + " :: " + MobileNo + " :: " + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return PID;
        }

        public int FileUploadUpdateInDB(int FUId, string ResponseMsg, string ResponseData)
        {
            Con = new OracleConnection(ConfigurationManager.AppSettings["OracleApolloT"].ToString());
            Cmd = new OracleCommand();
            int intRowsAffected = 0;
            try
            {
                //------------ADO DOT NET ----------------                                             
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand("SP_WhatsApp_FU_Update", Con);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("p_WAFUID", OracleType.Int32).Value = FUId;
                Cmd.Parameters.Add("p_WAFUResponseMsg", OracleType.NVarChar).Value = ResponseMsg;
                Cmd.Parameters.Add("p_WAFUResponseData", OracleType.NVarChar).Value = ResponseData;
                //Cmd.Parameters.Add("p_RowsAffected", OracleType.Int32, 15);
                //Cmd.Parameters["p_RowsAffected"].Direction = ParameterDirection.Output;
                intRowsAffected = Cmd.ExecuteNonQuery();
                //intRowsAffected = Convert.ToInt32(Cmd.Parameters["p_RowsAffected"].Value.ToString());

            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp File Upload to AWS service - got issue while Updating information into DB :: " + FUId + " :: " + ResponseMsg + " :: " + ResponseData + " :: " + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return intRowsAffected;
        }

        public int PushMessageInsertInDB(int FUID, string PMRequest, string PolicyHolderName, string PolicyNo, string MobileNo)
        {
            Con = new OracleConnection(ConfigurationManager.AppSettings["OracleApolloT"].ToString());
            Cmd = new OracleCommand();

            int PID = 0;
            try
            {
                //------------ADO DOT NET ----------------                                             
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand("SP_WhatsApp_PM_Insert", Con);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("p_WAFU_ID", OracleType.Int32).Value = FUID;
                Cmd.Parameters.Add("p_WAPM_Request", OracleType.NVarChar).Value = PMRequest;
                Cmd.Parameters.Add("p_WAPM_PolicyHolderName", OracleType.NVarChar).Value = PolicyHolderName;
                Cmd.Parameters.Add("p_WAPM_PolicyNo", OracleType.NVarChar).Value = PolicyNo;
                Cmd.Parameters.Add("p_WAPM_MobileNo", OracleType.NVarChar).Value = MobileNo;
                Cmd.Parameters.Add("p_ID", OracleType.Int32, 15);
                Cmd.Parameters["p_ID"].Direction = ParameterDirection.Output;
                int row = Cmd.ExecuteNonQuery();
                PID = Convert.ToInt32(Cmd.Parameters["p_ID"].Value.ToString());
            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp Push Message service - got issue while inserting information into DB :: " + FUID + " :: " + PMRequest + " :: " + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return PID;
        }

        public int PushMessageUpdateInDB(int PMID, string PMRespnseMsg, string PMRespnseData)
        {
            Con = new OracleConnection(ConfigurationManager.AppSettings["OracleApolloT"].ToString());
            Cmd = new OracleCommand();
            int intRowsAffected = 0;
            try
            {               
                //------------ADO DOT NET ----------------                                             
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand("SP_WhatsApp_PM_Update", Con);
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("p_WAPMID", OracleType.Int32).Value = PMID;
                Cmd.Parameters.Add("P_WAPMResponseMsg", OracleType.NVarChar).Value = PMRespnseMsg;
                Cmd.Parameters.Add("P_WAPMResponseData", OracleType.NVarChar).Value = PMRespnseData;
               // Cmd.Parameters.Add("p_RowsAffected", OracleType.Int32, 15);
               // Cmd.Parameters["p_RowsAffected"].Direction = ParameterDirection.Output;
                intRowsAffected = Cmd.ExecuteNonQuery();
                //intRowsAffected = Convert.ToInt32(Cmd.Parameters["p_RowsAffected"].Value.ToString());
            }
            catch (Exception ex)
            {
                logger.Error("WhatsApp Push Message service - got issue while updateing information into DB :: " + PMID + " :: " + PMRespnseMsg + " :: " + PMRespnseData + " :: " + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return intRowsAffected;
        }
       
        //------ These values are required when consume AMCom FileUpload & PushMessage services. ------------
        public int CheckInputValues(string[] WhatsAppInputValues)
        {           
            int PID = 0;
            foreach (string strVal in WhatsAppInputValues)
            {
                if (string.IsNullOrEmpty(strVal))
                {
                    Con = new OracleConnection(ConfigurationManager.AppSettings["OracleApolloT"].ToString());
                    Cmd = new OracleCommand();                 
                    try
                    {
                        //------------ADO DOT NET ----------------                                             
                        if (Con.State != ConnectionState.Open)
                        {
                            Con.Open();
                        }
                        Cmd = new OracleCommand("SP_WhatsApp_IMP_Insert", Con);
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.Parameters.Add("p_IMP_PolicyHolderName", OracleType.NVarChar).Value = WhatsAppInputValues[0];
                        Cmd.Parameters.Add("p_IMP_PolicyNo", OracleType.NVarChar).Value = WhatsAppInputValues[1];
                        Cmd.Parameters.Add("p_IMP_MobileNo", OracleType.NVarChar).Value = WhatsAppInputValues[2];
                        Cmd.Parameters.Add("p_IMP_EmailID", OracleType.NVarChar).Value = WhatsAppInputValues[3];
                        Cmd.Parameters.Add("p_IMP_FilePath", OracleType.NVarChar).Value = WhatsAppInputValues[4];
                        Cmd.Parameters.Add("p_ID", OracleType.Int32, 15);
                        Cmd.Parameters["p_ID"].Direction = ParameterDirection.Output;
                        int row = Cmd.ExecuteNonQuery();
                        PID = Convert.ToInt32(Cmd.Parameters["p_ID"].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        logger.Error("WhatsAppImproper Values - get issue while inserting Improper Values into DB :: " + WhatsAppInputValues[0] + " :: " + WhatsAppInputValues[1] + " :: " + WhatsAppInputValues[2] + " :: " + " :: " + WhatsAppInputValues[13] + " :: " + WhatsAppInputValues[4] + " :: " + ex.Message);
                    }
                    finally
                    {
                        Con.Close();
                        Con.Dispose();
                    }
                    break;
                }
            }
            return PID;
        }

        public bool CheckForAllowedUserName(string UserName)
        {
            UserName = UserName.ToLower();
            bool UserNameExist = false;           

            string All_User_Name = Convert.ToString(ConfigurationManager.AppSettings["UserName"]).ToLower();
            All_User_Name = All_User_Name.Replace(" ", "");
            string[] All_User_Name_Arry = All_User_Name.Split(new char[] { ',' });
            if (All_User_Name_Arry.Contains(UserName))
            {
                UserNameExist = true;                
            }

            return UserNameExist;
        }
    }
}
