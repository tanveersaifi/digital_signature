﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OracleClient;
//using System.Data.OracleClient;
using System.Configuration;
using System.Net;
using Ionic.Zip;
using Ionic.Zlib;
using System.Net.Mail;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using dbAutoTrack.PDFWriter;
using dbAutoTrack.PDFWriter.Graphics;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Net.Mime;
using System.Net;
using DigitalSignature;

namespace Digital_Signature_Educare
{
    class BuyOnline2
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
        public static string policy_Number;
        public static string Email_id;
        public static string Main_member;
        public static int serialno = 1;
        public static string DMS_Image_ID;
        public static string fileArray = string.Empty;
        public static string Mobile_number = string.Empty;
        public static string Agent_Code = string.Empty;
        public static string PRODUCT_CODE = string.Empty;
        #region < On_line_Mailer >
        public void On_line_Mailer()
        {
            try
            {
                logger.Info("------------------BuyOnline 2 process start----------------");

                Delete_PDF();
                DataTable dt1 = new DataTable();


                dt1 = Policy_schedule_Data("Select distinct pol_number, email_id,main_member_name, max(dms_image_id) dms_image_id,MobileNo,AGENTCODE,Product_Code from TBL_Digital_Signature_mailer where status=0 and email_id is not null and Product_Code not in (Select PRODUCT_CODE from tbl_mailer_productlist where STATUS =1 and PRODUCT_SHORT_CODE not in ('HWT','HDFCOFF') ) and dms_image_id is not null and businesstype='Online' and Trunc(created_date) between Trunc(sysdate-3) and trunc(sysdate) group by pol_number, email_id,main_member_name,MobileNo,AGENTCODE,Product_Code ");

                //dt1 = Policy_schedule_Data("Select distinct pol_number, email_id,main_member_name, max(dms_image_id) dms_image_id,MobileNo,AGENTCODE,Product_Code from TBL_Digital_Signature_mailer where status=0 and email_id is not null  and username  in (select distinct username from tbl_usertable where bstatus=1) and Product_Code not in ('11173','11174','11175','11176','11200','11201','11202','11203','33003','34001','34002','34003','34004','34005','11228','11229','11230','11231','22300','22301','22302','22303','12765','12766') and dms_image_id is not null and businesstype='Online' and Trunc(created_date) between Trunc(sysdate-3) and trunc(sysdate) group by pol_number, email_id,main_member_name,MobileNo,AGENTCODE ");
               
                int Noofrecords = dt1.Rows.Count;

                Console.WriteLine("Total Numbers of Buyonline Records  : " + Noofrecords);
                logger.Info("Total Buyonline Policy Number method 2 : " + dt1.Rows.Count.ToString());

                if (dt1.Rows.Count != 0) // If datatable Has More than 0 record
                {
                    for (int x = 0; x < dt1.Rows.Count; x++)   // Loop Upto last record from Data Table
                    {
                        policy_Number = dt1.Rows[x]["pol_number"].ToString();   // Get Policy number
                        Email_id = dt1.Rows[x]["email_id"].ToString(); //get Email -ID
                        Main_member = dt1.Rows[x]["main_member_name"].ToString();
                        DMS_Image_ID = dt1.Rows[x]["dms_image_id"].ToString().Trim();
                        Mobile_number = dt1.Rows[x]["MobileNo"].ToString().Trim();
                        Agent_Code = dt1.Rows[x]["AGENTCODE"].ToString().Trim();
                        PRODUCT_CODE = dt1.Rows[x]["Product_Code"].ToString().Trim();
                        logger.Info("Policy Number : " + dt1.Rows[x][0].ToString());
                        string Location = Convert.ToString(ConfigurationSettings.AppSettings["FileArray"].Trim());
                        fileArray = Location + DMS_Image_ID + "";
                        bool ISAXW = dt1.Rows[x]["pol_number"].ToString().Contains("AXW");
                        if (ISAXW==false)
                        {
                            DMS_Get_Image(policy_Number);
                        }
                    }                   
                }
                else
                {
                    logger.Info("Record Not Found ");  // if  Record Not found 
                }               
            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message);                
            }
        }
        #endregion



        #region < DMS_Get_Image >

        public static void DMS_Get_Image(string ProspectNumber)
        {
            try
            {
                String moduleName = Convert.ToString(ConfigurationSettings.AppSettings["Img_moduleName"].Trim());
                String contextRoot = Convert.ToString(ConfigurationSettings.AppSettings["Img_contextRoot"].Trim());
                String repository = Convert.ToString(ConfigurationSettings.AppSettings["Img_repository"].Trim());
                String user = Convert.ToString(ConfigurationSettings.AppSettings["Img_user"].Trim());
                String password = Convert.ToString(ConfigurationSettings.AppSettings["Img_password"].Trim());

                ContextFactory contextFactory = ContextFactory.Instance;
                IServiceContext context = contextFactory.NewContext();
                RepositoryIdentity repoId = new RepositoryIdentity();

                repoId.RepositoryName = repository;
                repoId.UserName = user;
                repoId.Password = (password);
                context.AddIdentity(repoId);
                IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);
                ServiceFactory serviceFactory = ServiceFactory.Instance;
                IListDocument service = serviceFactory.GetRemoteService<IListDocument>(registeredContext, moduleName, contextRoot);
                int type = 2;

                string[] arr = { "adkv_printer_doc" };//adkv_printer_doc
                List<string> docTypes = new List<string>(arr);
                List<ListBean> policySchdLst = service.GetListOfDocuments(ProspectNumber, type, docTypes);

                object obj = policySchdLst;

                if (policySchdLst.Count >= 1)
                {
                    bool find = false;
                    for (int i = 0; i < policySchdLst.Count; i++)
                    {
                        //if (Convert.ToString(policySchdLst[i].objectName).Contains("Proposer"))//Policy Schedule
                        //{
                        // update image Id in Database For Particualr Policy Number 
                        //Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate,DMS_Image_id='" + policySchdLst[i].objectId + "' where pol_number ='" + policy_Number + "' ;");
                        find = true;
                        GetDMSReports(policySchdLst[i].objectId, ProspectNumber);
                        break;
                        //}
                    }
                }
                else if (policySchdLst.Count == 0)
                {
                    DataTable dt1 = new DataTable();
                    dt1 = Policy_schedule_Data("SELECT cch.STRPOLNBR , cch.STRIMAGEID,cch.STRFILENAME,CRM.STRDESC    FROM COM_CONTACT_HST@w2e CCH,    COM_REPORTS_M@w2e CRM    WHERE     CCH.LREPSEQ=CRM.LREPSEQ    and cch.STRPOLNBR ='" + ProspectNumber.ToString() + "'    and lower(CRM.STRDESC) like '%schedule%'");
                    if (dt1.Rows.Count > 0)
                    {
                        GetDMSReports(dt1.Rows[0][1].ToString(), ProspectNumber);
                    }
                }
                else
                {
                    string Image_Error = "Image Id Not Found";
                    logger.Info(Image_Error);
                    Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + Image_Error + "' where pol_number ='" + policy_Number + "' ;");
                }
            }
            catch (Exception ex)
            {
               
                logger.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }
        }

        #endregion

        #region < GetDMSReports >
        public static void GetDMSReports(string strImageId, string ProspectNumber)
        {
            logger.Info("GetDMSreport Function call...");
            string strFileName = string.Empty;
            try
            {
                String moduleName = Convert.ToString(ConfigurationSettings.AppSettings["File_moduleName"].Trim());
                String contextRoot = Convert.ToString(ConfigurationSettings.AppSettings["File_contextRoot"].Trim());
                String repository = Convert.ToString(ConfigurationSettings.AppSettings["File_repository"].Trim());
                String user = Convert.ToString(ConfigurationSettings.AppSettings["File_user"].Trim());
                String password = Convert.ToString(ConfigurationSettings.AppSettings["File_password"].Trim());

                ContextFactory contextFactory = ContextFactory.Instance;
                IServiceContext context = contextFactory.NewContext();
                RepositoryIdentity repoId = new RepositoryIdentity();

                repoId.RepositoryName = repository;
                repoId.UserName = user;
                repoId.Password = (password);
                context.AddIdentity(repoId);

                IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);

                ServiceFactory serviceFactory = ServiceFactory.Instance;
                IObjectService service = serviceFactory.GetRemoteService<IObjectService>(registeredContext, moduleName, contextRoot);

                ContentTransferProfile transferProfile = new ContentTransferProfile();
                transferProfile.TransferMode = ContentTransferMode.MTOM;
                transferProfile.Geolocation = "";
                registeredContext.SetProfile(transferProfile);

                ObjectIdentity objectIdentity = new ObjectIdentity(repository);
                ObjectId objId = new ObjectId(strImageId);
                objectIdentity.Value = objId;
                byte[] byteArray;
                ContentProfile contentProfile = new ContentProfile();
                contentProfile.FormatFilter = FormatFilter.ANY;

                OperationOptions operationOptions = new OperationOptions();
                operationOptions.ContentProfile = contentProfile;

                operationOptions.SetProfile(contentProfile);
                ObjectIdentitySet objectIdSet = new ObjectIdentitySet();

                List<ObjectIdentity> objIdList = objectIdSet.Identities;
                objIdList.Add(objectIdentity);
                DataPackage dataPackage = service.Get(objectIdSet, operationOptions);
                DataObject dataObject = dataPackage.DataObjects[0];
                Emc.Documentum.FS.DataModel.Core.Content.Content resultContent = dataObject.Contents[0];


                if (resultContent.CanGetAsFile())
                {
                    strFileName = strImageId + "." + Convert.ToString(resultContent.Format);
                    byteArray = resultContent.GetAsByteArray();
                    string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                    string date = DateTime.Now.ToString("dd-MM-yyyy");
                    string FileName = ProspectNumber+".pdf";
                    string Inputpath = savelocation + "On_Line\\" + date + @"\";
                    if (!Directory.Exists(Inputpath))
                    {
                        Directory.CreateDirectory(Inputpath);
                    }
                    try
                    {
                        File.WriteAllBytes(Inputpath + FileName, byteArray);
                        string policy_number = policy_Number;
                        string E_mailID = Email_id;
                        sendMail(E_mailID, Inputpath + FileName, "Apollo Munich Health Insurance Policy Schedule : " + policy_number, policy_number, Main_member, Agent_Code, PRODUCT_CODE);
       
                    }
                    catch (Exception ex)
                    {
                        string error = "Exception at the time of write policy schedule";
                        logger.Error(ex.Message);
                        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + error.ToString() + "' where pol_number ='" + ProspectNumber + "' ;");
                    }
                }
            }
            catch (Exception ex)
            {
                string error = "DMS Report Exception";
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + error.ToString() + "' where pol_number ='" + ProspectNumber + "' ;");
                logger.Error("DMS Report Exception  : " + ex.Message);
            }
        }
        #endregion

        #region < sendMail >

        public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number, string main_member, string agentCode, string PRODUCT_CODE)
        {
            try
            {
                string strPath = string.Empty;
                string Imapath = string.Empty;
                Imapath = Convert.ToString(ConfigurationSettings.AppSettings["IMGPATH"].Trim());
                logger.Info("Send Mail Function Call...");
                int flag = 0;
                if (PRODUCT_CODE == "34011" || PRODUCT_CODE == "34012" || PRODUCT_CODE == "34013" || PRODUCT_CODE == "34014")
                {
                    strPath = Convert.ToString(ConfigurationSettings.AppSettings["HDFCofflinePath"].Trim());
                    flag = 2;
                }
                else
                {
                    DataTable agentcodeHDFCdt = new DataTable();
                    agentcodeHDFCdt = Policy_schedule_Data("Select Agentcode from tbl_agentcode where agentcode='" + agentCode + "' and ISEMAIL=0");
                    if (agentcodeHDFCdt.Rows.Count > 0)
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["HDFCofflinePath"].Trim());
                        flag = 2;
                    }
                    else
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["AMHI"].Trim());
                        flag = 1;
                    }
                    DataTable OR_POS_dt = new DataTable();
                    OR_POS_dt = Policy_schedule_Data("Select Agentcode from tbl_agentcode where agentcode='" + agentCode + "' and ISEMAIL=9");
                    if (OR_POS_dt.Rows.Count > 0)
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["OR_POS_path"].Trim());
                        flag = 3;
                    }  
                }
                
                string strBody = File.ReadAllText(strPath);

                MailMessage message = new MailMessage();
                string msg_from = "relay<relay@apollomunichinsurance.com>";
                message.From = new MailAddress(msg_from);
                string MessageTo = mailTo;
                // message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));
               // string MessageTo = "vishal.saxena1@apollomunichinsurance.com";
                bool agent = false;
                if (agentCode != "" && agentCode != null)
                {
                    DataTable agentcodedt = new DataTable();
                    agentcodedt = Policy_schedule_Data("Select Agentcode,email_id from tbl_agentcode where agentcode='" + agentCode + "' and ISEMAIL=1");
                    if (agentcodedt.Rows.Count > 0)
                    {
                        agent = true;
                        string agentcode = agentcodedt.Rows[0][0].ToString();
                        string Emailaddress = agentcodedt.Rows[0][1].ToString();
                        message.CC.Add(new MailAddress(Emailaddress));
                        // message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));
                    }
                }

                //string MessageTo = "vishal.saxena1@apollomunichinsurance.com";
                //message.CC.Add(new MailAddress("mukesh.tiwari@apollomunichinsurance.com"));
                //message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));                
                message.To.Add(new MailAddress(MessageTo));
                message.Subject = subject;
                message.Body = strBody;                

                #region <BuyOnline>
                if (flag == 1)
                {
                LinkedResource logo = new LinkedResource(Imapath+"AMHI\\img01.jpg");
                logo.ContentId = "img01";
                LinkedResource header = new LinkedResource(Imapath+"AMHI\\img02.jpg");
                header.ContentId = "img02";
                LinkedResource Step_process = new LinkedResource(Imapath+"AMHI\\img03.jpg");
                Step_process.ContentId = "img03";
                LinkedResource policy = new LinkedResource(Imapath+"AMHI\\img04.jpg");
                policy.ContentId = "img04";
                LinkedResource newimg1 = new LinkedResource(Imapath+"AMHI\\img05.jpg");
                newimg1.ContentId = "img05";
                LinkedResource newimg2 = new LinkedResource(Imapath+"AMHI\\img06.jpg");
                newimg2.ContentId = "img06";
                LinkedResource newimg3 = new LinkedResource(Imapath+"AMHI\\img07.jpg");
                newimg3.ContentId = "img07";
                LinkedResource newimg4 = new LinkedResource(Imapath+"AMHI\\icon_01.png");
                newimg4.ContentId = "icon_01";
                LinkedResource newimg5 = new LinkedResource(Imapath+"AMHI\\icon_02.png");
                newimg5.ContentId = "icon_02";
                LinkedResource newimg6 = new LinkedResource(Imapath+"AMHI\\icon_03.png");
                newimg6.ContentId = "icon_03";
                LinkedResource newimg7 = new LinkedResource(Imapath+"AMHI\\icon_04.png");
                newimg7.ContentId = "icon_04";
                LinkedResource newimg8 = new LinkedResource(Imapath+"AMHI\\fb.jpg");
                newimg8.ContentId = "fb";

                LinkedResource newimg9 = new LinkedResource(Imapath+"AMHI\\gp.jpg");
                newimg9.ContentId = "gp";

                LinkedResource newimg10 = new LinkedResource(Imapath+"AMHI\\in.jpg");
                newimg10.ContentId = "in";

                LinkedResource newimg11 = new LinkedResource(Imapath+"AMHI\\tw.jpg");
                newimg11.ContentId = "tw";

                LinkedResource newimg12 = new LinkedResource(Imapath+"AMHI\\ytube.jpg");
                newimg12.ContentId = "ytube";


                AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                av1.LinkedResources.Add(logo);
                av1.LinkedResources.Add(header);
                av1.LinkedResources.Add(Step_process);
                av1.LinkedResources.Add(policy);
                av1.LinkedResources.Add(newimg1);
                av1.LinkedResources.Add(newimg2);
                av1.LinkedResources.Add(newimg3);
                av1.LinkedResources.Add(newimg4);
                av1.LinkedResources.Add(newimg5);
                av1.LinkedResources.Add(newimg6);
                av1.LinkedResources.Add(newimg7);
                av1.LinkedResources.Add(newimg8);
                av1.LinkedResources.Add(newimg9);
                av1.LinkedResources.Add(newimg10);
                av1.LinkedResources.Add(newimg11);
                av1.LinkedResources.Add(newimg12);

                message.AlternateViews.Add(av1);
                }
                #endregion

                #region HDFC Offline
                else if (flag == 2)
                {
                    LinkedResource Img1 = new LinkedResource(Imapath+"HDFC Offline\\topn.jpg");
                    Img1.ContentId = "topn";
                    LinkedResource Img2 = new LinkedResource(Imapath+"HDFC Offline\\bullet.png");
                    Img2.ContentId = "bullet";
                    LinkedResource Img3 = new LinkedResource(Imapath+"HDFC Offline\\arrow2.jpg");
                    Img3.ContentId = "arrow2";
                    LinkedResource Img4 = new LinkedResource(Imapath+"HDFC Offline\\icoa.png");
                    Img4.ContentId = "icoa";
                    LinkedResource Img5 = new LinkedResource(Imapath+"HDFC Offline\\icob.png");
                    Img5.ContentId = "icob";
                    LinkedResource Img6 = new LinkedResource(Imapath+"HDFC Offline\\icoc.png");
                    Img6.ContentId = "icoc";
                    LinkedResource Img7 = new LinkedResource(Imapath+"HDFC Offline\\ic1.png");
                    Img7.ContentId = "ic1";
                    LinkedResource Img8 = new LinkedResource(Imapath+"HDFC Offline\\ic2.png");
                    Img8.ContentId = "ic2";
                    LinkedResource Img9 = new LinkedResource(Imapath+"HDFC Offline\\ic3.png");
                    Img9.ContentId = "ic3";
                    LinkedResource Img10 = new LinkedResource(Imapath+"HDFC Offline\\ic4.png");
                    Img10.ContentId = "ic4";
                    LinkedResource Img11 = new LinkedResource(Imapath+"HDFC Offline\\ic5.png");
                    Img11.ContentId = "ic5";
                    LinkedResource Img12 = new LinkedResource(Imapath+"HDFC Offline\\ic7.png");
                    Img12.ContentId = "ic7";
                    LinkedResource Img13 = new LinkedResource(Imapath+"HDFC Offline\\ic8.png");
                    Img13.ContentId = "ic8";
                    LinkedResource Img14 = new LinkedResource(Imapath+"HDFC Offline\\ic11.png");
                    Img14.ContentId = "ic11";


                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(Img1);
                    av1.LinkedResources.Add(Img2);
                    av1.LinkedResources.Add(Img3);
                    av1.LinkedResources.Add(Img4);
                    av1.LinkedResources.Add(Img5);
                    av1.LinkedResources.Add(Img6);
                    av1.LinkedResources.Add(Img7);
                    av1.LinkedResources.Add(Img8);
                    av1.LinkedResources.Add(Img9);
                    av1.LinkedResources.Add(Img10);
                    av1.LinkedResources.Add(Img11);
                    av1.LinkedResources.Add(Img12);
                    av1.LinkedResources.Add(Img13);
                    av1.LinkedResources.Add(Img14);

                    message.AlternateViews.Add(av1);

                }
                #endregion

                #region OR POS
                else if (flag == 3)
                {
                    LinkedResource Img1 = new LinkedResource(Imapath + "OR_POS\\topn.jpg");
                    Img1.ContentId = "topn";
                    LinkedResource Img2 = new LinkedResource(Imapath + "OR_POS\\bullet.png");
                    Img2.ContentId = "bullet";
                    LinkedResource Img3 = new LinkedResource(Imapath + "OR_POS\\arrow2.jpg");
                    Img3.ContentId = "arrow2";
                    LinkedResource Img4 = new LinkedResource(Imapath + "OR_POS\\icoa.png");
                    Img4.ContentId = "icoa";
                    LinkedResource Img5 = new LinkedResource(Imapath + "OR_POS\\icob.png");
                    Img5.ContentId = "icob";
                    LinkedResource Img6 = new LinkedResource(Imapath + "OR_POS\\icoc.png");
                    Img6.ContentId = "icoc";
                    LinkedResource Img7 = new LinkedResource(Imapath + "OR_POS\\ic1.png");
                    Img7.ContentId = "ic1";
                    LinkedResource Img8 = new LinkedResource(Imapath + "OR_POS\\ic2.png");
                    Img8.ContentId = "ic2";
                    LinkedResource Img9 = new LinkedResource(Imapath + "OR_POS\\ic3.png");
                    Img9.ContentId = "ic3";
                    LinkedResource Img10 = new LinkedResource(Imapath + "OR_POS\\ic4.png");
                    Img10.ContentId = "ic4";
                    LinkedResource Img11 = new LinkedResource(Imapath + "OR_POS\\ic5.png");
                    Img11.ContentId = "ic5";
                    LinkedResource Img12 = new LinkedResource(Imapath + "OR_POS\\ic7.png");
                    Img12.ContentId = "ic7";
                    LinkedResource Img13 = new LinkedResource(Imapath + "OR_POS\\ic8.png");
                    Img13.ContentId = "ic8";
                    LinkedResource Img14 = new LinkedResource(Imapath + "OR_POS\\ic11.png");
                    Img14.ContentId = "ic11";


                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(Img1);
                    av1.LinkedResources.Add(Img2);
                    av1.LinkedResources.Add(Img3);
                    av1.LinkedResources.Add(Img4);
                    av1.LinkedResources.Add(Img5);
                    av1.LinkedResources.Add(Img6);
                    av1.LinkedResources.Add(Img7);
                    av1.LinkedResources.Add(Img8);
                    av1.LinkedResources.Add(Img9);
                    av1.LinkedResources.Add(Img10);
                    av1.LinkedResources.Add(Img11);
                    av1.LinkedResources.Add(Img12);
                    av1.LinkedResources.Add(Img13);
                    av1.LinkedResources.Add(Img14);

                    message.AlternateViews.Add(av1);

                }
                #endregion

                message.IsBodyHtml = true;

                Attachment attachment = new System.Net.Mail.Attachment(FilePath);

                attachment.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                message.Attachments.Add(attachment);
                logger.Info("Attach all documents...");
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient(); // Live                
                client.Send(message);
                logger.Info("Succesfully Mail Sent to : " + mailTo);
                if (agent == true)
                {
                    string CCmail = message.CC.ToString();
                    //string BCCmail = message.Bcc.ToString();
                    logger.Info("CC Mail to :" + CCmail.ToString());
                    //logger.Info("Bcc Mail to :" + BCCmail.ToString());
                }
                //Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS) values ('" + Policy_Number + "','" + Mobile_number + "','0') ;");
                // Insert_Log(" INSERT INTO tbl_digital_signature_mail_hst (POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,status,SENT_DATE,CREATED_DATE,DMS_IMAGE_ID) ( select POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,1 status,sysdate,CREATED_DATE,DMS_IMAGE_ID from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "') ;");
                // Delete_Log(" Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "' ;");
                Update_Log("update tbl_digital_signature_mailer set status =1 , sent_date = sysdate  where pol_number ='" + Policy_Number + "' and BUSINESSTYPE is not null ;");
                // Report for End user Console
                Console.WriteLine(+serialno + ". " + "E-mail Send for the : " + Policy_Number + " E-Mail ID - " + mailTo);
                serialno++;
            }
            catch (Exception ex)
            {
                // update Exception For Sending mail To user
                logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
                Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='Email Sending Failed due to exception' where  email_id ='" + mailTo + "' and pol_number ='" + Policy_Number + "' ;");
                //throw;
            }
            finally
            {

            }
        }

        #endregion

        #region < Policy_schedule_Data >
        private static DataTable Policy_schedule_Data(string strquery)
        {
            DataTable dtReport = new DataTable();
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = strquery;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = Cmd;
                da.Fill(dtReport);
                return dtReport;
            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + strquery);
                //throw;
                return dtReport;
            }

        }
        #endregion

        #region < Update_Log >
        private static void Update_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Insert_Log >
        private static void Insert_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Delete_Log >
        private static void Delete_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        public static void Delete_PDF()
        {
            string savelocation1 = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
            string date1 = DateTime.Now.AddDays(-3).ToString("dd-MM-yyyy");

            string Inputpath1 = savelocation1 + "\\On_Line\\" + date1;
            string Inputpath2 = savelocation1 + "\\vishal\\" + date1;
            string Inputpath3 = savelocation1 + "\\Educare_PROD\\" + date1;
            string Inputpath4 = savelocation1 + "\\Health_On_PROD\\" + date1;
            
            if (Directory.Exists(Inputpath1))
            {
                Directory.Delete(Inputpath1, true);
            }
            if (Directory.Exists(Inputpath2))
            {
                Directory.Delete(Inputpath2, true);
            }
            if (Directory.Exists(Inputpath3))
            {
                Directory.Delete(Inputpath3, true);
            }
            if (Directory.Exists(Inputpath4))
            {
                Directory.Delete(Inputpath4, true);
            }
        }
    }
}
