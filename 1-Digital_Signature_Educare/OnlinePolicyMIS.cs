﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Net;
using System.Data.OracleClient;
using System.IO;

namespace Digital_Signature_Educare
{
    class OnlinePolicyMIS
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");

        public void OnlinePolicyMISreport()
        {
            try
            {
                logger.Info("=============MIs Report Job Start =============");



                string MISPath = Convert.ToString(ConfigurationSettings.AppSettings["NotYetstamp"].Trim());
                string Exception = Convert.ToString(ConfigurationSettings.AppSettings["Exception"].Trim());
                string path1 = MISPath;           
                int MISreport = 0;
                int exception = 0;   
                FileInfo file1 = new FileInfo(path1);
                logger.Info("Checking file Exist or not");               
                if (file1.Exists)
                {
                    file1.Delete();
                }
                string From_Date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.AddDays(-1));
                string strtitle = "Please find below Policy Schedule & E-card Mailer report for the " + From_Date;
                string sub1 = "MIS Report of online policy received and mail status";              
                MISreport = MISReport(MISPath);
                exception = Exceptionreport(Exception);
                SendMail(sub1, strtitle, MISPath, MISreport, exception, Exception);                
                logger.Info("Mail sending done.....");
            }
            catch (Exception ex)
            {
                logger.Error("Error : " + ex.Message);
            }
            finally
            {                
            }
        }
        
        public static Boolean SendMail(string strsubject, string strtitle, string filepath,int attachIsempty,int excpetion,string Exception)
        {
            try
            {
                logger.Info("Send Mail Function call...");
                DataTable dt = new DataTable();
                StringBuilder sb1 = new StringBuilder("");
                dt = Generate_Mail(@" SELECT DISTINCT TotalElixir,TotalPdfStamp,(Totalpolicy+Totalhstpolicy)TotalPolicy,(TotalDigital+TotalhstDigital)TotalDigital,EmailIDempty,TotalError,Totalnotyetstamp
                                    FROM
                                    (SELECT
                                      (SELECT count(strpolnbr) from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%')TotalElixir,
  
                                      (select count(distinct strpolnbr) from pdf_stamp_log@w2e where dtpolissue between trunc(sysdate-1) and trunc(sysdate) and exceptionmsg='x' and strpolnbr in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))TotalPdfStamp,
  
                                      (select count(distinct  pol_number) from tbl_digital_signature_mailer where pol_number in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))Totalpolicy,
  
                                      (select count( distinct pol_number) from tbl_digital_signature_mail_hst where pol_number in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))Totalhstpolicy,
  
                                      (select count(distinct pol_number) from tbl_digital_signature_mailer where status=1 and pol_number in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))TotalDigital,
  
                                      (select count(distinct pol_number) from tbl_digital_signature_mail_hst where status=1 and pol_number in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))TotalhstDigital,
  
                                      (select count(distinct pol_number) from tbl_digital_signature_mailer where email_id is null and pol_number in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))EmailIDempty,
  
                                      (select count(distinct pol_number) from tbl_digital_signature_mailer where email_id is not null and error_log is not null and status=0 and pol_number in
                                      (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%'))TotalError,
  
                                      (select count(distinct strpolnbr) from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%' and 
                                      strpolnbr not in (select strpolnbr from pdf_stamp_log@w2e where exceptionmsg='x'))Totalnotyetstamp
    
                                      FROM dual
                                      ) a");

                if (dt.Rows.Count > 0 && dt != null)
                {
                    dt = dt.DefaultView.ToTable();
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total issued policy in Elixir</td>");
                    sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(dt.Rows[0]["TotalElixir"]) + "</td>");                    
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total policies stamped</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["TotalPdfStamp"])) + "</td>");                        
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total policies not yet stamped</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["Totalnotyetstamp"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total policy in mailer table</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(dt.Rows[0]["TotalPolicy"]) + "</td>");                        
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total Email Send</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["TotalDigital"])) + "</td>");                        
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Email id not available</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["EmailIDempty"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Exception</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["TotalError"])) + "</td>");
                        
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    
                    sb1.AppendFormat("</tr>");

                    String strPath = ConfigurationSettings.AppSettings["MISRecordshtml"].ToString().Trim();
                    //String strPath = @"F:\Digital_Educare_PROD\online_tracker.htm";
                    string strBody = File.ReadAllText(strPath);
                    strBody = strBody.Replace("<easycapstatus>", sb1.ToString());
                    strBody = strBody.Replace("<name>", "All");
                    strBody = strBody.Replace("<strtitle>", strtitle);
                    string From_Date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.AddDays(-1));
                    strBody = strBody.Replace("<date123>", From_Date);

                    MailMessage message = new MailMessage();

                    //string from = ConfigurationSettings.AppSettings["from"].ToString().Trim();
                    string msg_from = "relay<relay@apollomunichinsurance.com>";

                    //string pwd = ConfigurationSettings.AppSettings["Password"].ToString().Trim();
                    //string msg_from = from;
                    message.From = new MailAddress(msg_from);

                    message.To.Add("amal.ghosh@apollomunichinsurance.com");
                    message.To.Add("amit.singh2@apollomunichinsurance.com");                    
                    message.To.Add("vishal.saxena1@apollomunichinsurance.com");
                    message.To.Add("shalsha3@in.ibm.com");
                    message.To.Add("sachin.paswan@apollomunichinsurance.com");
                    message.CC.Add("ravi.kuhar@apollomunichinsurance.com");                                        
                    message.CC.Add("suvir.chopra@apollomunichinsurance.com");

                    message.CC.Add("mukesh.tiwari@apollomunichinsurance.com");
                    message.CC.Add("sauraku1@in.ibm.com");
                    message.CC.Add("ashuksin@in.ibm.com");
                    message.CC.Add("Abhay.Kumar@apollomunichinsurance.com");
                    message.CC.Add("Punit.Kumar@apollomunichinsurance.com");
                    message.CC.Add("vikbsing@in.ibm.com");
                    message.CC.Add("anupamak@in.ibm.com"); 

                    message.Subject = strsubject;
                    message.Body = strBody;
                    message.IsBodyHtml = true;
                    if (attachIsempty == 1)
                    {
                        Attachment attachmentmde = new System.Net.Mail.Attachment(filepath);
                        attachmentmde.Name = filepath.Substring(filepath.LastIndexOf('\\') + 1); ;
                        message.Attachments.Add(attachmentmde);
                    }
                    if (excpetion == 1)
                    {
                        Attachment attachment = new System.Net.Mail.Attachment(Exception);
                        attachment.Name = Exception.Substring(Exception.LastIndexOf('\\') + 1); ;
                        message.Attachments.Add(attachment);
                    }
                    SmtpClient client = new SmtpClient(); 
                    client.Send(message);
                }
                return true;              
            }
            catch (Exception ex)
            {
                logger.Error("Exception :" + ex.Message.ToString());
                return false;
            }
        }

        public static int TotalMailFailureData(string filepath)
        {
            int status = 0;
            DataTable dtFailure = new DataTable();
            dtFailure = Generate_Mail(@"select distinct   pol_number ,main_member_name,(select  ccc.STRCONTACTNBR from com_pol_prod_dtl@W2E cppd , COM_CLIENT_CONTACT@W2E ccc where strpolnbr = pol_number and NSACCD =1 and cppd.STRCLIENTCD = ccc.STRCLIENTCD and ccc.STRCONTACTTYPECD=7 and rownum =1 ) mobile ,username,email_id,  (CASE   WHEN email_id IS  NULL  THEN 'Email Id Not Available'  else  error_log  END) AS error_log
            from tbl_digital_signature_mailer where status=0  and TRUNC(created_date)=TRUNC(sysdate)");
            if(dtFailure.Rows.Count>0)
            {
            dtFailure.Columns["pol_number"].ColumnName = "Policy No";
            dtFailure.Columns["username"].ColumnName = "User Name";
            dtFailure.Columns["email_id"].ColumnName = "Email Id";
            dtFailure.Columns["main_member_name"].ColumnName = "Proposar Name";
            dtFailure.Columns["mobile"].ColumnName = "Mobile";
            dtFailure.Columns["error_log"].ColumnName = "Error Detail";
            ExportToExcel obj = new ExportToExcel();

            obj.ExportToExcelSheet(dtFailure, filepath);
              status=1;
            }
            return status;
        }

        public static int MISReport(string filepath)
        {
            int status = 0;
            DataTable dtMIS = new DataTable();
            dtMIS = Generate_Mail(@"   select strpolnbr from com_policy_m@w2e where strpolnbr not like 'AX%' and trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and 
                                       strpolnbr not in (select strpolnbr from pdf_stamp_log@w2e where exceptionmsg='x')");
            if (dtMIS.Rows.Count > 0)
            {
                dtMIS.Columns["strpolnbr"].ColumnName = "Policy No";
                //dtMIS.Columns["email_id"].ColumnName = "Email Id";
                //dtMIS.Columns["main_member_name"].ColumnName = "Main Memeber name";
                //dtMIS.Columns["policy_issue_date"].ColumnName = "Policy Issue Date";
                //dtMIS.Columns["created_date"].ColumnName = "Created Date";
                //dtMIS.Columns["sent_date"].ColumnName = "Send Date";
                //dtMIS.Columns["status"].ColumnName = "Status";
                //dtMIS.Columns["error_log"].ColumnName = "Error Detail";
                ExportToExcel obj = new ExportToExcel();
                obj.ExportToExcelSheet(dtMIS, filepath);
                status = 1;
            }
            return status;
        }

        public static int Exceptionreport(string filepath)
        {
            int status = 0;
            DataTable dtMIS = new DataTable();
//            dtMIS = Generate_Mail(@"   select pol_number,Email_id,error_log from tbl_digital_signature_mailer where email_id is not null and error_log is not null and status=0 and pol_number in
//  (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO'))");

         dtMIS = Generate_Mail(@"    select distinct pol_number,Email_id,error_log, 
                              case status when 0 then 'Not send' else 'Send succesfully' end Status
                              from tbl_digital_signature_mailer 
                              where pol_number in
                              (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%')
                              union all
                              select distinct pol_number,Email_id,error_log, 
                              case status when 0 then 'Not send' else 'Send succesfully' end status
                              from tbl_digital_signature_mail_hst
                              where pol_number in
                              (SELECT strpolnbr from com_policy_m@w2e where trunc(dtpolissue)=trunc(sysdate-1) and upper(strsourcecd) in  ('PORTAL','AMPOWER','AMPOWER-DO') and strpolnbr not like 'AX%')");
    
            if (dtMIS.Rows.Count > 0)
            {
                dtMIS.Columns["pol_number"].ColumnName = "Policy No";
                dtMIS.Columns["Email_id"].ColumnName = "Email Id";
                dtMIS.Columns["error_log"].ColumnName = "Error Status";
                dtMIS.Columns["Status"].ColumnName = "Send Status";
                //dtMIS.Columns["policy_issue_date"].ColumnName = "Policy Issue Date";
                //dtMIS.Columns["created_date"].ColumnName = "Created Date";
                //dtMIS.Columns["sent_date"].ColumnName = "Send Date";
                //dtMIS.Columns["status"].ColumnName = "Status";
                //dtMIS.Columns["error_log"].ColumnName = "Error Detail";
                ExportToExcel obj = new ExportToExcel();
                obj.ExportToExcelSheet(dtMIS, filepath);
                status = 1;
            }
            return status;
        }

        public static DataTable Generate_Mail(string strquery)
        {
            try
            {
                logger.Info("Database Slect Comand"+strquery);

                DataTable dt = new DataTable();
                string oradb = ConfigurationSettings.AppSettings["OracleApolloT"].ToString().Trim();

                OracleConnection conn = new OracleConnection(oradb);
                conn.Open();

                OracleCommand oraCom;
                OracleDataAdapter oraDa;
                oraDa = new OracleDataAdapter();

                oraCom = new OracleCommand(strquery, conn);
                oraCom.CommandType = CommandType.Text;
                oraDa.SelectCommand = oraCom;

                oraDa.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                logger.Error("MIs Report  : " + ex.Message);
                throw;
            }
        }
 

        public class ExportToExcel
        {
            public void ExportToExcelSheet(DataTable myDataTable, string filePath)
            {
                try
                {
                    // Create the DataGrid and perform the databinding 
                    var myDataGrid = new System.Web.UI.WebControls.DataGrid();
                    myDataGrid.HeaderStyle.Font.Bold = true;
                    myDataGrid.DataSource = myDataTable;
                    myDataGrid.DataBind();
                    string myFile = filePath;
                    var myFileStream = new FileStream(myFile, FileMode.Create, FileAccess.ReadWrite);
                    // Render the DataGrid control to a file 
                    using (var myStreamWriter = new StreamWriter(myFileStream))
                    {
                        using (var myHtmlTextWriter = new System.Web.UI.HtmlTextWriter(myStreamWriter))
                        {
                            myDataGrid.RenderControl(myHtmlTextWriter);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.Write("Error coming in Export");
                }
                finally
                {

                    Console.Write("Executed Successfully");

                }
            }
        }

    }
}
