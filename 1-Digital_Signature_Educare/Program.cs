﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Net;
using Ionic.Zip;
using Ionic.Zlib;
using System.Net.Mail;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using dbAutoTrack.PDFWriter;
using dbAutoTrack.PDFWriter.Graphics;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;

namespace Digital_Signature_Educare
{
    class Program
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
        static void Main(string[] args)
        {
            try
            {                
                logger.Info("============= Job Start =============");

                string hours = DateTime.Now.Hour.ToString();
                //string hours = DateTime.Now.TimeOfDay.ToString();
                TimeSpan currenttime = DateTime.Now.TimeOfDay;
                TimeSpan start = TimeSpan.Parse("10:00");
                TimeSpan end = TimeSpan.Parse("10:16");
                TimeSpan start1 = TimeSpan.Parse("09:00"); //09:00
                TimeSpan end1 = TimeSpan.Parse("09:40");   //09:40


                if (currenttime >= start && currenttime <= end)
                {
                    OnlinePolicyMIS online1 = new OnlinePolicyMIS();
                    online1.OnlinePolicyMISreport();
                }
                if (currenttime >= start1 && currenttime <= end1)
                {
                    logger.Info("============= buyonline 2 class execute =============");
                    BuyOnline2 newonline = new BuyOnline2();
                    newonline.On_line_Mailer();
                }
                Educare educare = new Educare();
                educare.Educare_Mailer();

                Buy_Online online = new Buy_Online();
                online.On_line_Mailer();
            }
            catch (Exception ex)
            {
                logger.Error("Error  : " + ex.Message);

            }
        }
    }
}
