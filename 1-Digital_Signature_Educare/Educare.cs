﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Net.Mail;
using System.Linq;
using System.Net.Mime;
using System.Net;
using System.Text;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.DataModel.Core.Context;
using System.Collections.Generic;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Content;
using DigitalSignature;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Digital_Signature_Educare
{
	class Educare
	{
		public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
		public static string policy_Number;
        public static string policy_Number_offline;
		public static string Email_id;
        public static string Email_id_Offline;
		public static string Main_member;
		public static int serialno = 1;
		public static string DMS_Image_ID;
		public static int SUM_INSURED = 0;
		public static string ScheduleFileName = string.Empty;
		public static string Policy_schedulepath = string.Empty;
		public static string Prospect_number = string.Empty;
		public static string Mobile_number = string.Empty;
        public static string Application_no = string.Empty;
        public static int IsDMS=0;
        public static string agentCode;
        public static string fileArray = string.Empty;
        public static string fileArray1 = string.Empty;
        public static string DMS_imageID = string.Empty;
        public static string ProspectNo = string.Empty;
        public static string Product_No = string.Empty;
        public static int offline_flag = 0;
		DataTable newdt = new DataTable();
		DataTable Recorddt = new DataTable();
		DataTable chkduprecorddt = new DataTable();
        DataTable dt_offline = new DataTable();
        DataTable dt = new DataTable();

        WhtsAppPolicy objWhtsAppPolicy = null;

		#region < Educare_Mailer >
		public void Educare_Mailer()
		{
           
            try
            {
                string domain_check = Convert.ToString(ConfigurationManager.AppSettings["domain_check"].Trim());

                logger.Info("=============Educare Production Job Start =============");

                string squery = "SELECT cpd.strpolnbr policyno,"
                                          + "cpm.strcreatedby,"
                                          + "cce.stremailaddr,"
                                          + "cpd.dtappln dtappln,"
                                         + " 0 status,"
                                         + " cch.strfilename filename,"
                                          + "(SELECT dsa"
                                         + " FROM com_pol_prod_dtl@W2E"
                                         + " WHERE strprodcd IN ('11200','11201','11202','11203')"
                                         + " AND nsaccd       =1 "
                                        + "  AND strpolnbr    = cpd.strpolnbr"
                                         + " ) SI ,"
                                         + " (SELECT strprodcd"
                                         + " FROM com_pol_prod_dtl@W2E"
                                         + " WHERE nsaccd  =1 and  nprodtype=1"
                                         + " AND strpolnbr = cpd.strpolnbr"
                                        + "  AND rownum    =1"
                                        + "  ) product_Code ,"
                                        + "  (SELECT STRPROPNBR FROM nb_prop_pol_lnk@W2E WHERE STRPOLNBR = cpd.strpolnbr"
                                         + " ) STRPROPNBR,"
                                          + "  (select  ccc.STRCONTACTNBR from com_pol_prod_dtl@W2E cppd , COM_CLIENT_CONTACT@W2E ccc "
                                          + "  where strpolnbr = cpd.strpolnbr and NSACCD =1 and cppd.STRCLIENTCD = ccc.STRCLIENTCD and ccc.STRCONTACTTYPECD=7 and rownum =1 ) mobile ,cpm.strservagentcd AgentCode,"
                                          + " (select strapplnnbr from com_policy_dtl@W2E where strpolnbr = cpd.strpolnbr)application_number"
                                        + " FROM hsasys.com_policy_dtl@W2E cpd,"
                                         + " hsasys.com_contact_hst@W2E cch ,"
                                         + " com_client_email @W2E cce ,"
                                         + " com_policy_m @W2E cpm"
                                        + " WHERE "
                                        + " cpd.strpolnbr            = cch.strpolnbr"
                                        + " AND cch.strpolnbr            = cpm.strpolnbr"
                                        + " AND cce.strclientcd          = cch.strclientcd"
                                        + " AND cch.strfilename IS NOT NULL"
                                        + " AND cpm.dtpolcomc   IS NOT NULL"
                                        + " AND cpd.strpolnbr   IN"
                                         + " (SELECT psl.strpolnbr"
                                        + "  FROM pdf_stamp_log@W2E psl"
                                        + "  WHERE length(dtpolissue)=9  and psl.exceptionmsg='x'"
                                        //       + " AND psl.dtpolissue BETWEEN TRUNC(TO_DATE(sysdate-1,'DD-MM-YY')) AND TRUNC(TO_DATE(sysdate,'DD-MM-YY'))"
                                        + "    AND trunc(to_date(psl.dtpolissue,'dd-mm-yy')) BETWEEN TRUNC(sysdate-1) AND TRUNC(sysdate)) "     //changed by neeraj
                                        + " AND cpd.strpolnbr NOT IN (SELECT pol_number FROM tbl_digital_signature_mailer UNION ALL SELECT pol_number FROM tbl_digital_signature_mail_hst) ";


                newdt = Policy_schedule_Data(squery);

                if (newdt.Rows.Count > 0)
                {
                    for (int p = 0; p < newdt.Rows.Count; p++)
                    {
                        string NewPolicyNo = newdt.Rows[p][0].ToString();
                        string Username = newdt.Rows[p][1].ToString();
                        string NewEmail = newdt.Rows[p][2].ToString();
                        string CreatedDate = newdt.Rows[p][3].ToString();
                        string FileId = newdt.Rows[p][5].ToString();
                        string SI = newdt.Rows[p][6].ToString();
                        string Product_Code = newdt.Rows[p][7].ToString();
                        string Prospect_No = newdt.Rows[p][8].ToString();
                        string Mobile_No = newdt.Rows[p][9].ToString();
                        string AgentCode = newdt.Rows[p][10].ToString();
                        string Application_no = newdt.Rows[p][11].ToString();

                        if (SI == null || SI == "")
                        {
                            SI = null;
                        }
                        if (Product_Code == "33003")
                        {                                                     
                             chkduprecorddt = Policy_schedule_Data("SELECT pol_number FROM tbl_digital_signature_mailer where pol_number='" + NewPolicyNo + "' UNION ALL SELECT pol_number FROM tbl_digital_signature_mail_hst where pol_number='" + NewPolicyNo + "'");
                             if (chkduprecorddt.Rows.Count <= 0)
                             {
                                 Insert_Log(" INSERT INTO tbl_digital_signature_mailer (POL_NUMBER,USERNAME,EMAIL_ID,ERROR_LOG,status,CREATED_DATE,DMS_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No,BUSINESSTYPE,MobileNo,AGENTCODE,APPLICATIONNO) values ('" + NewPolicyNo.ToString() + "','" + Username.ToString() + "','" + NewEmail.ToString() + "',null,'0',sysdate,'" + FileId.ToString() + "','" + SI + "','" + Product_Code + "','" + Prospect_No + "','Educare','" + Mobile_No + "','" + AgentCode + "','" + Application_no + "') ;");
                                 logger.Info("Record insert into Mailer Table : policy Number : '" + NewPolicyNo.ToString() + "'");
                                 Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS) values ('" + NewPolicyNo.ToString() + "','" + Mobile_No + "','0') ;");
                                 DataTable chkdt = new DataTable();
                                 chkdt = Policy_schedule_Data("select strcreatedby from  com_policy_m@W2E where strpolnbr= '" + NewPolicyNo.ToString() + "' and strcreatedby in (select username from tbl_usertable)");
                                 if (chkdt.Rows.Count > 0)
                                 {
                                     Update_Log("update tbl_digital_signature_mailer set businesstype='Online' where pol_number='" + NewPolicyNo.ToString() + "';");
                                 }                                 
                             }                           
                        }                        
                    }
                }
                dt_offline = Policy_schedule_Data("Select distinct pol_number,email_id,main_member_name,agentcode,dms_image_id,PROSPECT_NO,PRODUCT_CODE,applicationno from TBL_Digital_Signature_mailer where status=0 and email_id is not null and dms_image_id is not NULL and  businesstype is not null and Product_Code in ('33003','34001','34002','34003','34004','34005') and username not in (select distinct username from tbl_usertable where bstatus=1) and trunc(Created_date) between trunc(sysdate-1) and trunc(sysdate) ");

                //dt = Policy_schedule_Data("Select distinct pol_number, email_id,main_member_name,dms_image_id,SUM_INSURED,Prospect_No,MobileNo,applicationno from tbl_digital_signature_mailer where BUSINESSTYPE is not null and email_id IS NOT NULL and dms_image_id IS NOT NULL and status=0 and Product_Code in ('33003','34001','34002','34003','34004','34005') and username in (select distinct username from tbl_usertable where bstatus=1) and trunc(Created_date) between trunc(sysdate-1) and trunc(sysdate) ");
                //dt = Policy_schedule_Data("Select distinct pol_number, email_id,main_member_name,dms_image_id,SUM_INSURED,Prospect_No,MobileNo,applicationno, USERNAME from tbl_digital_signature_mailer where BUSINESSTYPE is not null and email_id IS NOT NULL and dms_image_id IS NOT NULL and status=0 and Product_Code in ('33003','34001','34002','34003','34004','34005') and username in (select distinct username from tbl_usertable where bstatus=1) and trunc(Created_date) between trunc(sysdate-1) and trunc(sysdate) ");        // commented by Tanveer to get null email id items too
                dt = Policy_schedule_Data("Select distinct pol_number, email_id,main_member_name,dms_image_id,SUM_INSURED,Prospect_No,MobileNo,applicationno, USERNAME from tbl_digital_signature_mailer where BUSINESSTYPE is not null and dms_image_id IS NOT NULL and status=0 and Product_Code in ('33003','34001','34002','34003','34004','34005') and username in (select distinct username from tbl_usertable where bstatus=1) and trunc(Created_date) between trunc(sysdate-1) and trunc(sysdate) ");

                logger.Info("Total Educare Offline Policy Number : " + dt_offline.Rows.Count.ToString());
				logger.Info("Total Educare Online Policy Number : " + dt.Rows.Count.ToString());

                string WhatsAppMainMember = string.Empty;
                string WhatsAppEmail = string.Empty;

                string User_Name = string.Empty;
                int ImproperRecordNo = 0;

                //Online policy Mailer
				if (dt.Rows.Count > 0)
				{
					for (int x = 0; x < dt.Rows.Count; x++)
					{
                        WhatsAppMainMember = string.Empty;
                        WhatsAppEmail = string.Empty;

						policy_Number = dt.Rows[x]["pol_number"].ToString();
                        Email_id = "";// dt.Rows[x]["email_id"].ToString();
						Main_member = dt.Rows[x]["main_member_name"].ToString();
						DMS_Image_ID = dt.Rows[x]["dms_image_id"].ToString().Trim();
						//SUM_INSURED = Convert.ToInt32(dt.Rows[x]["SUM_INSURED"]);
						Prospect_number = dt.Rows[x]["Prospect_No"].ToString();
						Mobile_number = dt.Rows[x]["MobileNo"].ToString();
                        Application_no = dt.Rows[x]["applicationno"].ToString();
                        User_Name = dt.Rows[x]["USERNAME"].ToString().Trim();
                        logger.Info("Policy Number : " + dt.Rows[x][0].ToString());
						string Location = Convert.ToString(ConfigurationManager.AppSettings["FileArray"].Trim());
						fileArray = Location + DMS_Image_ID + "";

                        if (string.IsNullOrEmpty(Main_member))
                        {
                            WhatsAppMainMember = "Customer";
                        }
                        else
                        {
                            WhatsAppMainMember = Main_member;
                        }

                        if (string.IsNullOrEmpty(Email_id))
                        {
                            WhatsAppEmail = "Email not available";
                        }
                        else
                        {
                            WhatsAppEmail = Email_id;
                        }

                        if (domain_check == "Y")
                        {
                            bool isapollo = Email_id.Contains("@apollomunichinsurance.com");
                            if (isapollo == true)
                            {
                                if (!string.IsNullOrEmpty(Email_id))
                                {
                                    GetWebsiteHtml(fileArray, policy_Number + ".pdf", "Online");
                                }

                                //-------- WhatsApp only selected UserIDs ---------- 
                                objWhtsAppPolicy = new WhtsAppPolicy();

                                if (!string.IsNullOrEmpty(User_Name))
                                {
                                    if (objWhtsAppPolicy.CheckForAllowedUserName(User_Name))
                                    {
                                        ImproperRecordNo = objWhtsAppPolicy.CheckInputValues(new string[5] { WhatsAppMainMember, policy_Number, Mobile_number, WhatsAppEmail, fileArray });

                                        if (ImproperRecordNo == 0)  // all five variables contain values.
                                        {
                                            objWhtsAppPolicy.UploadPolicyPDF(WhatsAppMainMember, policy_Number, Mobile_number, Email_id, fileArray);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                logger.Info("mail id doesnt have apollomunichinsurace.com domain");
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Email_id))
                            {
                                GetWebsiteHtml(fileArray, policy_Number + ".pdf", "Online");
                            }

                            //-------- WhatsApp only selected UserIDs ---------- 
                            objWhtsAppPolicy = new WhtsAppPolicy();

                            if (!string.IsNullOrEmpty(User_Name))
                            {
                                if (objWhtsAppPolicy.CheckForAllowedUserName(User_Name))
                                {
                                    ImproperRecordNo = objWhtsAppPolicy.CheckInputValues(new string[5] { WhatsAppMainMember, policy_Number, Mobile_number, WhatsAppEmail, fileArray });

                                    if (ImproperRecordNo == 0)  // all five variables contain values.
                                    {
                                        objWhtsAppPolicy.UploadPolicyPDF(WhatsAppMainMember, policy_Number, Mobile_number, Email_id, fileArray);
                                    }
                                }
                            }
                        }
                           

                            
					}
				}
				else
				{
					logger.Info("Record Not Found For Online");
				}

               // Offline policy mailer

                if (dt_offline.Rows.Count != 0)
                {
                    for (int x = 0; x < dt_offline.Rows.Count; x++) 
                    {
                        agentCode = dt_offline.Rows[x]["agentcode"].ToString();
                        policy_Number_offline = dt_offline.Rows[x]["pol_number"].ToString();
                        Email_id_Offline = dt_offline.Rows[x]["email_id"].ToString();
                        Main_member = dt_offline.Rows[x]["main_member_name"].ToString();
                        DMS_imageID = dt_offline.Rows[x]["dms_image_id"].ToString().Trim();
                        ProspectNo = dt_offline.Rows[x]["PROSPECT_NO"].ToString().Trim();
                        Product_No = dt_offline.Rows[x]["PRODUCT_CODE"].ToString().Trim();
                        Application_no = dt_offline.Rows[x]["applicationno"].ToString().Trim();
                        logger.Info("Policy Number:" + dt_offline.Rows[x]["pol_number"].ToString());
                        string Location = Convert.ToString(ConfigurationSettings.AppSettings["FileArray"].Trim());
                        fileArray1 = Location + DMS_imageID + "";
                        if (domain_check == "Y")
                        {
                            bool isapollo = Email_id.Contains("@apollomunichinsurace.com");
                            if (isapollo == true)
                            {
                                GetWebsiteHtml(fileArray1, policy_Number_offline + ".pdf", "Offline");
                            }
                            else
                            {
                                logger.Info("mail id doesnt have apollomunichinsurace.com domain");
                            }
                        }
                        else
                        {
                            GetWebsiteHtml(fileArray1, policy_Number_offline + ".pdf", "Offline");
                        }
                                            
                    }
                    logger.Info("Batch Completed Successfully");
                }
                else
                {
                    logger.Info("Record Not Found for Offline ");
                }
			}
			catch (Exception ex)
			{
				logger.Error("Policy_schedule_Data  : " + ex.Message);
			}
		}
		#endregion

		#region < GetWebsiteHtml >
		private static void GetWebsiteHtml(string url, string strfilene,string Mode)
		{
			try
			{
				byte[] bytes = null;
				GenerateArchivePDF(bytes, strfilene,Mode);
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' and businesstype is not null ;");
			}
		}
		#endregion

		#region < GenerateArchivePDF >
		protected static void GenerateArchivePDF(byte[] buffer, string strfilene,string mode)
		{
            try
            {
            #region <Online>
                if (mode == "Online")
                            {
                                logger.Info("ON-line function call");
                                string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                                string date = DateTime.Now.ToString("dd-MM-yyyy");
                                string FileName = strfilene;
                                int flag = 0;
                                string Inputpath = savelocation + "Educare_PROD\\" + date + @"\";
                                if (!Directory.Exists(Inputpath))
                                {
                                    Directory.CreateDirectory(Inputpath);
                                }
                                try
                                {
                                    logger.Info("Policy schedule download function");
                                    logger.Info(fileArray);
                                    logger.Info(Inputpath + FileName);
                                    WebClient webClient = new WebClient();
                                    webClient.DownloadFile(fileArray, Inputpath + FileName);
                                    webClient.Dispose();
                                    flag = 1;
                                    if (flag == 1)
                                    {
                                        string policy_wording = Convert.ToString(ConfigurationSettings.AppSettings["Educare_policy_wording"]).Trim();
                                        string policy_number = policy_Number;
                                        string Subject = "Apollo Munich Health Insurance Policy : Educare " + policy_number;
                                        string Proposal=string.Empty;
                                        logger.Info("Send mail function call...");
                                        sendMail(Email_id, Inputpath + FileName, Subject, policy_number, Main_member, policy_wording, Proposal, mode);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string Err = "Failure to write PDF";
                                    logger.Error(Err.ToString());
                                    Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' and businesstype is not null ;");
                                }
                            }
            #endregion

            #region <Offline>
            if (mode == "Offline")
            {
                logger.Info("OFF-line function call");
                string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                string FileName = strfilene;
                int offline_flag = 0;
                string Inputpath = savelocation + "Educare_PROD\\" + date + @"\";
                if (!Directory.Exists(Inputpath))
                {
                    Directory.CreateDirectory(Inputpath);
                }
                try
                {
                    logger.Info(fileArray1);
                    logger.Info(Inputpath + FileName);
                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(fileArray1, Inputpath + FileName);
                    webClient.Dispose();
                    //flag = 1;
                    try
                    {
                        offline_flag = 1;
                        //DMS_Get_Image(Application_no);
                        //if (IsDMS == 0)
                        //{
                        //    offline_flag = 1;
                        //}
                        //else
                        //{
                        //    offline_flag = 0;
                        //    logger.Error("Unable to Download proposal form from DMS service");
                        //    Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Unable to Download proposal form from DMS service' where pol_number ='" + policy_Number_offline + "'  and businesstype is not null ;");
                        //}
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                        Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='Unable to Download proposal form from DMS service' where pol_number ='" + policy_Number_offline + "'  and businesstype is not null ;");
                    }
                    if (offline_flag == 1)
                    {
                        string policy_wording = Convert.ToString(ConfigurationSettings.AppSettings["Educare_policy_wording"]).Trim();
                        string policy_number = policy_Number_offline;
                        string Subject = "Apollo Munich Health Insurance Policy : Educare " + policy_number;
                        string proposal=Policy_schedulepath + ScheduleFileName;
                        sendMail(Email_id_Offline, Inputpath + FileName, Subject, policy_number, Main_member, policy_wording, proposal,mode);
                    }
                }
                catch (Exception ex)
                {
                    string Err = "Failure to write PDF";
                    logger.Error(Err.ToString());
                    Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number_offline + "' and businesstype is not null ;");
                }
            }
            #endregion
            }
            catch (Exception ex)
            {
                logger.Info("GenerateArchivePDF Exception + '"+mode+"'");
                Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' and businesstype is not null ;");
            }
		}
		#endregion

		#region < DMS_Get_Image >

        public static void DMS_Get_Image(string Application_no)
		{
			try
			{
				String moduleName = Convert.ToString(ConfigurationSettings.AppSettings["Img_moduleName"].Trim());
				String contextRoot = Convert.ToString(ConfigurationSettings.AppSettings["Img_contextRoot"].Trim());
				String repository = Convert.ToString(ConfigurationSettings.AppSettings["Img_repository"].Trim());
				String user = Convert.ToString(ConfigurationSettings.AppSettings["Img_user"].Trim());
				String password = Convert.ToString(ConfigurationSettings.AppSettings["Img_password"].Trim());

				ContextFactory contextFactory = ContextFactory.Instance;
				IServiceContext context = contextFactory.NewContext();
				RepositoryIdentity repoId = new RepositoryIdentity();

				repoId.RepositoryName = repository;
				repoId.UserName = user;
				repoId.Password = (password);
				context.AddIdentity(repoId);
				IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);
				ServiceFactory serviceFactory = ServiceFactory.Instance;
				IListDocument service = serviceFactory.GetRemoteService<IListDocument>(registeredContext, moduleName, contextRoot);
				int type = 2;

				string[] arr = {"adkv_proposal_doc"};//adkv_printer_doc
				List<string> docTypes = new List<string>(arr);
                List<ListBean> policySchdLst = service.GetListOfDocuments(Application_no, type, docTypes);
                
				object obj = policySchdLst;

				if (policySchdLst.Count >= 1)
				{
					bool find = false;
					for (int i = 0; i < policySchdLst.Count; i++)
					{
						//if (Convert.ToString(policySchdLst[i].objectName).Contains("Proposer"))//Policy Schedule
						//{
						// update image Id in Database For Particualr Policy Number 
						//Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate,DMS_Image_id='" + policySchdLst[i].objectId + "' where pol_number ='" + policy_Number + "' ;");
						find = true;
                        GetDMSReports(policySchdLst[i].objectId, Application_no);
						break;
						//}
					}
				}
				else
				{
                    IsDMS = 1;
                    //int offline_flag = 0;
					string Image_Error = "Proposal form Not Found in DMS";
					logger.Info(Image_Error);
                    Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + Image_Error + "' where pol_number ='" + policy_Number_offline + "' and businesstype is not null ;");
				}
			}
			catch (Exception ex)
			{
                IsDMS = 1;
				logger.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number_offline + "' and businesstype is not null ;");
			}
		}

		#endregion

		#region < GetDMSReports >
        public static void GetDMSReports(string strImageId, string Application_no)
		{
			string strFileName = string.Empty;
			try
			{
				String moduleName = Convert.ToString(ConfigurationSettings.AppSettings["File_moduleName"].Trim());
				String contextRoot = Convert.ToString(ConfigurationSettings.AppSettings["File_contextRoot"].Trim());
				String repository = Convert.ToString(ConfigurationSettings.AppSettings["File_repository"].Trim());
				String user = Convert.ToString(ConfigurationSettings.AppSettings["File_user"].Trim());
				String password = Convert.ToString(ConfigurationSettings.AppSettings["File_password"].Trim());

				ContextFactory contextFactory = ContextFactory.Instance;
				IServiceContext context = contextFactory.NewContext();
				RepositoryIdentity repoId = new RepositoryIdentity();

				repoId.RepositoryName = repository;
				repoId.UserName = user;
				repoId.Password = (password);
				context.AddIdentity(repoId);

				IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);

				ServiceFactory serviceFactory = ServiceFactory.Instance;
				IObjectService service = serviceFactory.GetRemoteService<IObjectService>(registeredContext, moduleName, contextRoot);

				ContentTransferProfile transferProfile = new ContentTransferProfile();
				transferProfile.TransferMode = ContentTransferMode.MTOM;
				transferProfile.Geolocation = "";
				registeredContext.SetProfile(transferProfile);

				ObjectIdentity objectIdentity = new ObjectIdentity(repository);
				ObjectId objId = new ObjectId(strImageId);
				objectIdentity.Value = objId;
				byte[] byteArray;
				ContentProfile contentProfile = new ContentProfile();
				contentProfile.FormatFilter = FormatFilter.ANY;

				OperationOptions operationOptions = new OperationOptions();
				operationOptions.ContentProfile = contentProfile;

				operationOptions.SetProfile(contentProfile);
				ObjectIdentitySet objectIdSet = new ObjectIdentitySet();

				List<ObjectIdentity> objIdList = objectIdSet.Identities;
				objIdList.Add(objectIdentity);
				DataPackage dataPackage = service.Get(objectIdSet, operationOptions);
				DataObject dataObject = dataPackage.DataObjects[0];
				Emc.Documentum.FS.DataModel.Core.Content.Content resultContent = dataObject.Contents[0];

				ScheduleFileName = string.Empty;
				Policy_schedulepath = string.Empty;

				if (resultContent.CanGetAsFile())
				{
					strFileName = strImageId + "." + Convert.ToString(resultContent.Format);
					byteArray = resultContent.GetAsByteArray();
					string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
					string date = DateTime.Now.ToString("dd-MM-yyyy");
                    string type = resultContent.Format.ToString();
                    ScheduleFileName = Application_no + "." + type;
                    Policy_schedulepath = savelocation + "Educare_Policy_Schedule\\" + date + @"\";
					if (!Directory.Exists(Policy_schedulepath))
					{
						Directory.CreateDirectory(Policy_schedulepath);
					}
					try
					{
						File.WriteAllBytes(Policy_schedulepath + ScheduleFileName, byteArray);
                        IsDMS = 0;
					}
					catch (Exception ex)
					{
                        IsDMS = 1;
						logger.Error("Failure to write PDF");
                        Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number_offline + "' ;");
					}
				}
				else
				{
                    IsDMS = 1;
					string Error = "PDF File Not Found ";
					logger.Info(Error.ToString());
                    Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + Error + "' where pol_number ='" + policy_Number_offline + "' and businesstype is not null ;");
				}
			}
			catch (Exception ex)
			{
                IsDMS = 1;
                Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number_offline + "' and businesstype is not null ;");
				logger.Error("DMS Report Exception  : " + ex.Message);
			}
		}
		#endregion

		#region < Policy_schedule_Data >
		private static DataTable Policy_schedule_Data(string strquery)
		{
			DataTable dtReport = new DataTable();
			try
			{
				OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
				OracleCommand Cmd = new OracleCommand();
				Cmd.Connection = Con;
				Cmd.CommandText = strquery;
				OracleDataAdapter da = new OracleDataAdapter(); 
				da.SelectCommand = Cmd;
				da.Fill(dtReport);
				return dtReport;
			}
			catch (Exception ex)
			{
				logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + strquery);
				//throw;
				return dtReport;
			}

		}
		#endregion

		#region < sendMail >

        public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number1, string main_member, string Educare_PW, string Proposal,string mode)
		{
			try
			{
                string strPath = Convert.ToString(ConfigurationSettings.AppSettings["Educare"].Trim());
				//string strPath = Directory.GetCurrentDirectory().Replace("bin\\Debug", "") + Convert.ToString(ConfigurationSettings.AppSettings["Educare"].Trim());
				string strBody = File.ReadAllText(strPath);
				MailMessage message = new MailMessage();
				string msg_from = "relay<relay@apollomunichinsurance.com>";
				message.From = new MailAddress(msg_from);
				string MessageTo = mailTo;
                //string MessageTo = "vishal.saxena1@apollomunichinsurance.com";
				//message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));              
				message.To.Add(new MailAddress(MessageTo));
				message.Subject = subject;
				message.Body = strBody;
				message.IsBodyHtml = true;

				Attachment attachment = new Attachment(Educare_PW);
				attachment.Name = Educare_PW.Substring(Educare_PW.LastIndexOf('\\') + 1);
				message.Attachments.Add(attachment);

				if (!string.IsNullOrEmpty(FilePath))
				{
					Attachment attachment1 = new Attachment(FilePath);
					attachment1.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
					message.Attachments.Add(attachment1);
				}
                if (mode == "Offline")
                {
                    if (!string.IsNullOrEmpty(Proposal))
                    {                        
                        Attachment attachment2 = new Attachment(Proposal);
                        attachment2.Name = Proposal.Substring(Proposal.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment2);
                        logger.Info("Proposal form attach...");
                    }
                }
                logger.Info("Attach all documents..");
				SmtpClient client = new SmtpClient(); // Live                
				client.Send(message);
				message.Dispose();
				logger.Info("Mail Sent : " + mailTo);

                Update_Log("update tbl_digital_signature_mailer set status =1 , sent_date = sysdate  where pol_number ='" + Policy_Number1 + "' and businesstype is not null ;");
                Insert_Log("Insert into tbl_digital_signature_mail_hst (select * from tbl_digital_signature_mailer where pol_number ='" + Policy_Number1 + "' and businesstype is not null and status =1); ");
                logger.Info("Record insert into History table");
                Delete_Log("Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number1 + "' ;");
                logger.Info("Record Delete from main table");
                //Insert_Log(" INSERT INTO tbl_digital_signature_mail_hst (POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,status,SENT_DATE,CREATED_DATE,DMS_IMAGE_ID,ELIXIR_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No,applicationno) ( select POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,null,1 status,sysdate,CREATED_DATE,DMS_IMAGE_ID,ELIXIR_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No,applicationno from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "') ;");
				 //Delete_Log(" Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "' ;");
                
               // Update_Log("update tbl_digital_signature_mailer set status =1 , sent_date = sysdate  where pol_number ='" + Policy_Number1 + "' ;");
                
                Console.WriteLine(+serialno + ". " + "E-mail Send for the : " + Policy_Number1 + " E-Mail ID - " + mailTo);
				serialno++;
			}
			catch (Exception ex)
			{
				logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
                Update_Log("update tbl_digital_signature_mailer set sent_date = sysdate , error_log ='Error in mail sending' where  email_id ='" + mailTo + "' and pol_number ='" + Policy_Number1 + "' and businesstype is not null ;");
			}
			finally
			{

			}
		}

		#endregion

		#region < Update_Log >
		private static void Update_Log(string sQuery)
		{
			try
			{
				OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
				OracleCommand Cmd = new OracleCommand();

				int row = 0;
				sQuery = "Begin " + sQuery + " End;";
				if (Con.State != ConnectionState.Open)
				{
					Con.Open();
				}
				Cmd = new OracleCommand(sQuery, Con);
				Cmd.CommandType = CommandType.Text;
				row = Cmd.ExecuteNonQuery();
				Con.Close();
				Con.Dispose();

			}
			catch (Exception ex)
			{
				logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
				// throw;
			}
		}
		#endregion

		#region < Insert_Log >
		private static void Insert_Log(string sQuery)
		{
			try
			{
				OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
				OracleCommand Cmd = new OracleCommand();

				int row = 0;
				sQuery = "Begin " + sQuery + " End;";
				if (Con.State != ConnectionState.Open)
				{
					Con.Open();
				}
				Cmd = new OracleCommand(sQuery, Con);
				Cmd.CommandType = CommandType.Text;
				row = Cmd.ExecuteNonQuery();
				Con.Close();
				Con.Dispose();

			}
			catch (Exception ex)
			{
				logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
				// throw;
			}
		}
		#endregion

		#region < Delete_Log >
		private static void Delete_Log(string sQuery)
		{
			try
			{
				OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
				OracleCommand Cmd = new OracleCommand();

				int row = 0;
				sQuery = "Begin " + sQuery + " End;";
				if (Con.State != ConnectionState.Open)
				{
					Con.Open();
				}
				Cmd = new OracleCommand(sQuery, Con);
				Cmd.CommandType = CommandType.Text;
				row = Cmd.ExecuteNonQuery();
				Con.Close();
				Con.Dispose();

			}
			catch (Exception ex)
			{
				logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
				// throw;
			}
		}
		#endregion

	}
}
