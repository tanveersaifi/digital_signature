﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5472
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigitalSignature
{
    using System;
    using System.Collections.Generic;
    using Emc.Documentum.FS.Runtime;
    using Emc.Documentum.FS.Runtime.Context;
    using System.Runtime.Serialization;

    /// <exclude/>
    public partial class ListDocumentPortClient : IListDocument
    {

        /// <exclude/>
        public IServiceContext GetServiceContext()
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    ///<![CDATA[This class acts as a facade on the ListDocument SBO and will form the part of DFS web services.
    /// @author IBM]]>
    ///</summary>
    public interface IListDocument : IContextHolder
    {

        /// <summary>
        ///<![CDATA[ This method returns the brava view url for the given objectId.
        ///</summary>
        /// <param name="objectId"></param>
        /// <returns>
        ///Brava URL
        ///]]>
        ///</returns>
        string GetBravaURL(string objectId);

        /// <summary>
        ///<![CDATA[ This method returns list of documents of specific types under the
        /// Aplication or Policy folders.
        ///</summary>
        /// <param name="docNumber">-
        ///            Either application Number or Policy Number based on <type>
        ///            parameter.</param>
        /// <param name="type">-
        ///            Type = 1 - Pass Application No, 2 - Pass Policy Number</param>
        /// <param name="docTypes">-
        ///            Document types</param>
        /// <returns>
        ///]]> 
        ///</returns>
        List<ListBean> GetListOfDocuments(string docNumber, int type, List<System.String> docTypes);

        /// <summary>
        ///<![CDATA[ This method retrieves comma separated name of the policy document and object id of the document.
        ///</summary>
        /// <param name="policyNo"></param>
        /// <returns>
        ///Name of the policy document if found else returns null. In case
        ///         of empty or null policy number also returns null.
        ///]]>
        ///</returns>
        string GetPolicyDocument(string policyNo);
    }

    /// <exclude/>
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://apollo.com", ConfigurationName = "ListDocumentPort")]
    public interface ListDocumentPort
    {

        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        /// <exclude/>
        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(rt.fs.documentum.emc.com.ServiceException), Action = "", Name = "ServiceException", Namespace = "http://rt.fs.documentum.emc.com/")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        getBravaURLResponse getBravaURL(getBravaURLRequest request);

        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        /// <exclude/>
        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(rt.fs.documentum.emc.com.ServiceException), Action = "", Name = "ServiceException", Namespace = "http://rt.fs.documentum.emc.com/")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        getListOfDocumentsResponse getListOfDocuments(getListOfDocumentsRequest request);

        // CODEGEN: Parameter 'return' requires additional schema information that cannot be captured using the parameter mode. The specific attribute is 'System.Xml.Serialization.XmlElementAttribute'.
        /// <exclude/>
        [System.ServiceModel.OperationContractAttribute(Action = "", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(rt.fs.documentum.emc.com.ServiceException), Action = "", Name = "ServiceException", Namespace = "http://rt.fs.documentum.emc.com/")]
        [System.ServiceModel.XmlSerializerFormatAttribute()]
        [return: System.ServiceModel.MessageParameterAttribute(Name = "return")]
        getPolicyDocumentResponse getPolicyDocument(getPolicyDocumentRequest request);
    }

    /// <exclude/>
    [System.ServiceModel.MessageContractAttribute(WrapperName = "getBravaURL", WrapperNamespace = "http://apollo.com", IsWrapped = true)]
    public partial class getBravaURLRequest
    {

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string objectId;

        /// <exclude/>
        public getBravaURLRequest()
        {
        }

        /// <exclude/>
        public getBravaURLRequest(string objectId)
        {
            this.objectId = objectId;
        }
    }

    /// <exclude/>
    [System.ServiceModel.MessageContractAttribute(WrapperName = "getBravaURLResponse", WrapperNamespace = "http://apollo.com", IsWrapped = true)]
    public partial class getBravaURLResponse
    {

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string @return;

        /// <exclude/>
        public getBravaURLResponse()
        {
        }

        /// <exclude/>
        public getBravaURLResponse(string @return)
        {
            this.@return = @return;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://apollo.com/")]
    public partial class ListBean
    {

        private string objectNameField;

        private string objectIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string objectName
        {
            get
            {
                return this.objectNameField;
            }
            set
            {
                this.objectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string objectId
        {
            get
            {
                return this.objectIdField;
            }
            set
            {
                this.objectIdField = value;
            }
        }
    }

    /// <exclude/>
    [System.ServiceModel.MessageContractAttribute(WrapperName = "getListOfDocuments", WrapperNamespace = "http://apollo.com", IsWrapped = true)]
    public partial class getListOfDocumentsRequest
    {

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string docNumber;

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 1)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public int type;

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 2)]
        [System.Xml.Serialization.XmlElementAttribute("docTypes", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string[] docTypes;

        /// <exclude/>
        public getListOfDocumentsRequest()
        {
        }

        /// <exclude/>
        public getListOfDocumentsRequest(string docNumber, int type, string[] docTypes)
        {
            this.docNumber = docNumber;
            this.type = type;
            this.docTypes = docTypes;
        }
    }

    /// <exclude/>
    [System.ServiceModel.MessageContractAttribute(WrapperName = "getListOfDocumentsResponse", WrapperNamespace = "http://apollo.com", IsWrapped = true)]
    public partial class getListOfDocumentsResponse
    {

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute("return", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ListBean[] @return;

        /// <exclude/>
        public getListOfDocumentsResponse()
        {
        }

        /// <exclude/>
        public getListOfDocumentsResponse(ListBean[] @return)
        {
            this.@return = @return;
        }
    }

    /// <exclude/>
    [System.ServiceModel.MessageContractAttribute(WrapperName = "getPolicyDocument", WrapperNamespace = "http://apollo.com", IsWrapped = true)]
    public partial class getPolicyDocumentRequest
    {

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string policyNo;

        /// <exclude/>
        public getPolicyDocumentRequest()
        {
        }

        /// <exclude/>
        public getPolicyDocumentRequest(string policyNo)
        {
            this.policyNo = policyNo;
        }
    }

    /// <exclude/>
    [System.ServiceModel.MessageContractAttribute(WrapperName = "getPolicyDocumentResponse", WrapperNamespace = "http://apollo.com", IsWrapped = true)]
    public partial class getPolicyDocumentResponse
    {

        /// <exclude/>
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://apollo.com", Order = 0)]
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string @return;

        /// <exclude/>
        public getPolicyDocumentResponse()
        {
        }

        /// <exclude/>
        public getPolicyDocumentResponse(string @return)
        {
            this.@return = @return;
        }
    }

    /// <exclude/>
    public interface ListDocumentPortChannel : ListDocumentPort, System.ServiceModel.IClientChannel
    {
    }

    /// <exclude/>
    public partial class ListDocumentPortClient : System.ServiceModel.ClientBase<ListDocumentPort>, ListDocumentPort
    {

        /// <exclude/>
        public ListDocumentPortClient()
        {
        }

        /// <exclude/>
        public ListDocumentPortClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        /// <exclude/>
        public ListDocumentPortClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        /// <exclude/>
        public ListDocumentPortClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        /// <exclude/>
        public ListDocumentPortClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        /// <exclude/>
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        getBravaURLResponse ListDocumentPort.getBravaURL(getBravaURLRequest request)
        {
            return base.Channel.getBravaURL(request);
        }

        /// <exclude/>
        [System.CLSCompliantAttribute(false)]
        public string getBravaURL(string objectId)
        {
            getBravaURLRequest inValue = new getBravaURLRequest();
            inValue.objectId = objectId;
            getBravaURLResponse retVal = ((ListDocumentPort)(this)).getBravaURL(inValue);
            return retVal.@return;
        }

        /// <exclude/>
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        getListOfDocumentsResponse ListDocumentPort.getListOfDocuments(getListOfDocumentsRequest request)
        {
            return base.Channel.getListOfDocuments(request);
        }

        /// <exclude/>
        [System.CLSCompliantAttribute(false)]
        public ListBean[] getListOfDocuments(string docNumber, int type, string[] docTypes)
        {
            getListOfDocumentsRequest inValue = new getListOfDocumentsRequest();
            inValue.docNumber = docNumber;
            inValue.type = type;
            inValue.docTypes = docTypes;
            getListOfDocumentsResponse retVal = ((ListDocumentPort)(this)).getListOfDocuments(inValue);
            return retVal.@return;
        }

        /// <exclude/>
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        getPolicyDocumentResponse ListDocumentPort.getPolicyDocument(getPolicyDocumentRequest request)
        {
            return base.Channel.getPolicyDocument(request);
        }

        /// <exclude/>
        [System.CLSCompliantAttribute(false)]
        public string getPolicyDocument(string policyNo)
        {
            getPolicyDocumentRequest inValue = new getPolicyDocumentRequest();
            inValue.policyNo = policyNo;
            getPolicyDocumentResponse retVal = ((ListDocumentPort)(this)).getPolicyDocument(inValue);
            return retVal.@return;
        }

        /// <exclude/>
        public string GetBravaURL(string objectId)
        {
            getBravaURLRequest inValue = new getBravaURLRequest();
            inValue.objectId = objectId;
            getBravaURLResponse retVal = ((ListDocumentPort)(this)).getBravaURL(inValue);
            return retVal.@return;
        }

        /// <exclude/>
        public List<ListBean> GetListOfDocuments(string docNumber, int type, List<System.String> docTypes)
        {
            getListOfDocumentsRequest inValue = new getListOfDocumentsRequest();
            inValue.docNumber = docNumber;
            inValue.type = type;
            if (docTypes != null) { inValue.docTypes = docTypes.ToArray(); };
            getListOfDocumentsResponse retVal = ((ListDocumentPort)(this)).getListOfDocuments(inValue);
            return retVal.@return != null ? new List<ListBean>(retVal.@return) : null; ;
        }

        /// <exclude/>
        public string GetPolicyDocument(string policyNo)
        {
            getPolicyDocumentRequest inValue = new getPolicyDocumentRequest();
            inValue.policyNo = policyNo;
            getPolicyDocumentResponse retVal = ((ListDocumentPort)(this)).getPolicyDocument(inValue);
            return retVal.@return;
        }
    }
}
namespace rt.fs.documentum.emc.com
{
    using System.Xml.Serialization;
    using System.Runtime.Serialization;


    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Xml.Serialization.XmlSchemaProviderAttribute("ExportSchema")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public partial class ServiceException : object, System.Xml.Serialization.IXmlSerializable
    {

        private System.Xml.XmlNode[] nodesField;

        private static System.Xml.XmlQualifiedName typeName = new System.Xml.XmlQualifiedName("ServiceException", "http://rt.fs.documentum.emc.com/");

        public System.Xml.XmlNode[] Nodes
        {
            get
            {
                return this.nodesField;
            }
            set
            {
                this.nodesField = value;
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.nodesField = System.Runtime.Serialization.XmlSerializableServices.ReadNodes(reader);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            System.Runtime.Serialization.XmlSerializableServices.WriteNodes(writer, this.Nodes);
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public static System.Xml.XmlQualifiedName ExportSchema(System.Xml.Schema.XmlSchemaSet schemas)
        {
            System.Runtime.Serialization.XmlSerializableServices.AddDefaultSchema(schemas, typeName);
            return typeName;
        }
    }
}

