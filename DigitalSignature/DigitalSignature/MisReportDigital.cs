﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Net;
using System.Data.OracleClient;
using System.IO;

namespace DigitalSignature
{
    class MisReportDigital
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");

        public void MisReportMethod()
        {
            try
            {
                logger.Info("=============MIs Report Job Start =============");
               
                string filepath = Convert.ToString(ConfigurationSettings.AppSettings["MisFilePath"].Trim());
                string path = filepath;
                int attachmentVariable = 0;
                FileInfo file = new FileInfo(path);
                logger.Info("Checking file Exist or not");
                if (file.Exists)//check file exsit or not
                {
                    file.Delete();

                }
                string From_Date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.AddDays(-1));


                string strtitle = "Please find below Policy Schedule & E-card Mailer report for the " + From_Date;
                string sub = "MIS Report for Policy Schedule and E-card Mailer   ";
                attachmentVariable = TotalMailFailureData(filepath);

                SendMail(sub, strtitle, filepath, attachmentVariable);
            }
            catch (Exception ex)
            {
                //logger.Error("Error : " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Mail  sending done.....");
                logger.Info("Mail  sending done.....");
            }
        }
        public static Boolean SendMail(string strsubject, string strtitle, string filepath,int attachIsempty)
        {
            try
            {
                DataTable dt = new DataTable();
                StringBuilder sb1 = new StringBuilder("");
                dt = Generate_Mail(@"SELECT DISTINCT (TotalmailerCOunt+TotalhstCOunt)TotalPolicyNoDaily,
  emailIdNullDaily,
  (emailIdNotNullDailyMailer+emailIdNotNullDailyhst)emailIdNotNullDaily,
  EmptyemailWithFileDaily,
  errorMailDaily,
  totalerrorLogDaily,
  totalMailSentDaily,
  (TotalPolicyNoMonthlyMailer +TotalPolicyNoMonthlyhst)TotalPolicyNoMonthly,
  emailIdNullMonthly,
  totalMailSentMonthly,
  emailIdNotNullMonthly,
  EmptyemailWithFileMonthly,
  errorMailMonthly,
  totalerrorLogMonthly
FROM
  (SELECT
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE TRUNC(created_date)=TRUNC(sysdate)
    AND businesstype        IS NULL
    ) AS TotalmailerCOunt,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mail_hst
    WHERE TRUNC(created_date)=TRUNC(sysdate)
    AND businesstype        IS NULL
    ) AS TotalhstCOunt,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
    AND businesstype                       IS NULL
    ) AS TotalPolicyNoMonthlyMailer,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mail_hst
    WHERE EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
    AND businesstype                       IS NULL
    ) AS TotalPolicyNoMonthlyhst,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE TRUNC(created_date)=TRUNC(sysdate)
    AND email_id            IS NULL
    AND businesstype        IS NULL
    ) AS emailIdNullDaily,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
    AND email_id                           IS NULL
    AND businesstype                       IS NULL
    ) AS emailIdNullMonthly,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mail_hst
    WHERE status           =1
    AND businesstype      IS NULL
    AND TRUNC(sent_date)=TRUNC(sysdate)
    ) AS totalMailSentDaily,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mail_hst
    WHERE status                          =1
    AND businesstype                     IS NULL
    AND EXTRACT( MONTH FROM sent_date) = EXTRACT(MONTH FROM sysdate)
    ) AS totalMailSentMonthly,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE TRUNC(created_date)=TRUNC(sysdate)
    AND email_id            IS NOT NULL
    AND businesstype        IS NULL
    AND pol_Number          IS NOT NULL
    ) AS emailIdNotNullDailyMailer,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mail_hst
    WHERE TRUNC(created_date)=TRUNC(sysdate)
    AND email_id            IS NOT NULL
    AND businesstype        IS NULL
    AND pol_Number          IS NOT NULL
    ) AS emailIdNotNullDailyhst,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
    AND email_id                           IS NOT NULL
    AND businesstype                       IS NULL
    AND pol_Number                         IS NOT NULL
    ) AS emailIdNotNullMonthly,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE TRUNC(created_date)=TRUNC(sysdate)

    AND pol_Number          IS NOT NULL
    AND error_log ='File Not Found'
    AND businesstype        IS NULL
    ) AS EmptyemailWithFileDaily,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
   
    AND pol_Number                         IS NOT NULL
    AND error_log ='File Not Found'
    AND businesstype                       IS NULL
    ) AS EmptyemailWithFileMonthly,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE status           =0
    AND TRUNC(created_date)=TRUNC(sysdate)
    AND businesstype      IS NULL
    ) AS errorMailDaily,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE status                          =0
    AND EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
    AND businesstype                     IS NULL
    ) AS errorMailMonthly,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE TRUNC(created_date)=TRUNC(sysdate)
    AND error_log           IS NOT NULL
    AND businesstype        IS NULL
    AND error_log           <>'File Not Found'
    AND status               =0
    ) AS totalerrorLogDaily,
    (SELECT COUNT(pol_Number)
    FROM tbl_digital_signature_mailer
    WHERE EXTRACT( MONTH FROM created_date) = EXTRACT(MONTH FROM sysdate)
   
    AND error_log                          IS NOT NULL
    AND businesstype                       IS NULL
    AND error_log                          <>'File Not Found'
    AND status                              =0
    ) AS totalerrorLogMonthly
  FROM dual
  ) a");


       //         dt = Generate_Mail(@"select distinct  (select count(pol_Number) from tbl_digital_signature_mailer where trunc(created_date)=trunc(sysdate-1)) as TotalPolicyNoDaily," +
       //" (SELECT count(pol_Number)  FROM tbl_digital_signature_mailer WHERE EXTRACT( Month FROM created_date) = EXTRACT(Month FROM sysdate-1)) as TotalPolicyNoMonthly," +
       //" (select count(pol_Number) from tbl_digital_signature_mailer where trunc(created_date)=trunc(sysdate-1) and email_id is  null) as emailIdNullDaily," +
       //" (select count(pol_Number) from tbl_digital_signature_mailer WHERE EXTRACT( Month FROM created_date) = EXTRACT(Month FROM sysdate-1) and email_id is  null) as emailIdNullMonthly," +
       //" (select  count(pol_Number) from tbl_digital_signature_mailer where status=1 and trunc(created_date)=trunc(sysdate-1)) as totalMailSentDaily," +
       // " (select  count(pol_Number) from tbl_digital_signature_mailer where status=1 and EXTRACT( Month FROM created_date) = EXTRACT(Month FROM sysdate-1)) as totalMailSentMonthly," +
       //  " (select count(pol_Number) from tbl_digital_signature_mailer where trunc(created_date)=trunc(sysdate-1) and email_id is not  null and pol_Number is not  null) as emailIdNotNullDaily," +
       //" (select count(pol_Number) from tbl_digital_signature_mailer WHERE EXTRACT( Month FROM created_date) = EXTRACT(Month FROM sysdate-1) and email_id is not  null and pol_Number is not  null) as emailIdNotNullMonthly," +
       //" (select count(pol_Number) from tbl_digital_signature_mailer where trunc(created_date)=trunc(sysdate-1) and email_id is   null and pol_Number is not  null and dms_image_id is  null) as  EmptyemailWithFileDaily," +
       //" (select count(pol_Number) from tbl_digital_signature_mailer WHERE EXTRACT( Month FROM created_date) = EXTRACT(Month FROM sysdate-1) and email_id is   null and pol_Number is not  null and dms_image_id is  null) as EmptyemailWithFileMonthly," +
       // " (select  count(pol_Number) from tbl_digital_signature_mailer where status=0 and trunc(created_date)=trunc(sysdate-1)) as errorMailDaily," +
       // " (select  count(pol_Number) from tbl_digital_signature_mailer where status=0 and EXTRACT( Month FROM created_date) = EXTRACT(Month FROM sysdate-1)) as errorMailMonthly" +
       //" from tbl_digital_signature_mailer");
                if (dt.Rows.Count > 0 && dt != null)
                {
                    dt = dt.DefaultView.ToTable();
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total Policy </td>");
                    sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(dt.Rows[0]["TotalPolicyNoDaily"]) + "</td>");
                    sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(dt.Rows[0]["TotalPolicyNoMonthly"]) + "</td>");
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Email Address available</td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["emailIdNotNullDaily"])) + "</td>");
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["emailIdNotNullMonthly"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Email Address not available </td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(dt.Rows[0]["emailIdNullDaily"]) + "</td>");
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(dt.Rows[0]["emailIdNullMonthly"]) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Policy Doc not available </td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["EmptyemailWithFileDaily"])) + "</td>");
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["EmptyemailWithFileMonthly"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total Sent </td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["totalMailSentDaily"])) + "</td>");
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["totalMailSentMonthly"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total Mail Not Sent </td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["errorMailDaily"])) + "</td>");
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["errorMailMonthly"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    sb1.AppendFormat("<tr>");
                    sb1.AppendFormat("<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>Total Error </td>");
                    if (dt.Rows.Count > 0)
                    {
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["totalerrorLogDaily"])) + "</td>");
                        sb1.AppendFormat("<td align='center' valign='middle' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(Convert.ToString(dt.Rows[0]["totalerrorLogMonthly"])) + "</td>");
                    }
                    else
                    {
                        sb1.AppendFormat("<td align='right' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>0</td>");
                    }
                    sb1.AppendFormat("</tr>");
                    String strPath = @"F:\DigitalSignature\DigitalSignature\easycap_tracker.htm";
                    string strBody = File.ReadAllText(strPath);
                    strBody = strBody.Replace("<easycapstatus>", sb1.ToString());
                    strBody = strBody.Replace("<name>", "All");
                    strBody = strBody.Replace("<strtitle>", strtitle);
                    string From_Date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now);
                    strBody = strBody.Replace("<date123>", From_Date);

                    MailMessage message = new MailMessage();

                    //string from = ConfigurationSettings.AppSettings["from"].ToString().Trim();
                    string msg_from = Convert.ToString(ConfigurationSettings.AppSettings["From"].Trim());

                    //string pwd = ConfigurationSettings.AppSettings["Password"].ToString().Trim();
                    //string msg_from = from;
                    message.From = new MailAddress(msg_from);
                  

                  
                    message.To.Add("amit.gahlyan@apollomunichinsurance.com");
              
                    message.To.Add("vikesh.bhandari@apollomunichinsurance.com");
                    message.To.Add("saurabh.verma@apollomunichinsurance.com");
                    message.CC.Add("ravi.kuhar@apollomunichinsurance.com");
                    message.To.Add("mukesh.tiwari@apollomunichinsurance.com");
                    message.CC.Add("suvir.chopra@apollomunichinsurance.com");
              

                    message.Subject = strsubject;
                    message.Body = strBody;
                    message.IsBodyHtml = true;
                    if (attachIsempty == 1)
                    {
                        Attachment attachmentmde = new System.Net.Mail.Attachment(filepath);
                        attachmentmde.Name = filepath.Substring(filepath.LastIndexOf('\\') + 1); ;
                        message.Attachments.Add(attachmentmde);
                    }

                    SmtpClient client = new SmtpClient(); // Live
                    client.Send(message);
                }
                    return true;
              
            }
            catch (Exception ex)
            {

                return false;
            }

        }

        public static int TotalMailFailureData(string filepath)
        {
            int status = 0;
            DataTable dtFailure = new DataTable();
            dtFailure = Generate_Mail(@"   select distinct   pol_number ,main_member_name,(select  ccc.STRCONTACTNBR from com_pol_prod_dtl@w2e cppd , COM_CLIENT_CONTACT@W2E ccc where strpolnbr = pol_number and NSACCD =1 and cppd.STRCLIENTCD = ccc.STRCLIENTCD and ccc.STRCONTACTTYPECD=7 and rownum =1 ) mobile ,username,email_id,  (CASE   WHEN email_id IS  NULL  THEN 'Email Id Not Available'  else  error_log  END) AS error_log
 from tbl_digital_signature_mailer where status=0  and TRUNC(created_date)=TRUNC(sysdate)");
            if(dtFailure.Rows.Count>0)
            {
            dtFailure.Columns["pol_number"].ColumnName = "Policy No";
            dtFailure.Columns["username"].ColumnName = "User Name";
            dtFailure.Columns["email_id"].ColumnName = "Email Id";
            dtFailure.Columns["main_member_name"].ColumnName = "Proposar Name";
            dtFailure.Columns["mobile"].ColumnName = "Mobile";
            dtFailure.Columns["error_log"].ColumnName = "Error Detail";
            ExportToExcel obj = new ExportToExcel();

            obj.ExportToExcelSheet(dtFailure, filepath);
              status=1;
            }
            return status;


        }

        public static DataTable Generate_Mail(string strquery)
        {
            try
            {
                logger.Info("Database Slect Comand"+strquery);

                DataTable dt = new DataTable();
                string oradb = ConfigurationSettings.AppSettings["OracleApolloT"].ToString().Trim();

                OracleConnection conn = new OracleConnection(oradb);
                conn.Open();

                OracleCommand oraCom;
                OracleDataAdapter oraDa;
                oraDa = new OracleDataAdapter();

                oraCom = new OracleCommand(strquery, conn);
                oraCom.CommandType = CommandType.Text;
                oraDa.SelectCommand = oraCom;

                oraDa.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                logger.Error("MIs Report  : " + ex.Message);
                throw;
            }
        }
 

        public class ExportToExcel
        {
            public void ExportToExcelSheet(DataTable myDataTable, string filePath)
            {
                try
                {
                    // Create the DataGrid and perform the databinding 
                    var myDataGrid = new System.Web.UI.WebControls.DataGrid();
                    myDataGrid.HeaderStyle.Font.Bold = true;
                    myDataGrid.DataSource = myDataTable;
                    myDataGrid.DataBind();
                    string myFile = filePath;
                    var myFileStream = new FileStream(myFile, FileMode.Create, FileAccess.ReadWrite);
                    // Render the DataGrid control to a file 
                    using (var myStreamWriter = new StreamWriter(myFileStream))
                    {
                        using (var myHtmlTextWriter = new System.Web.UI.HtmlTextWriter(myStreamWriter))
                        {
                            myDataGrid.RenderControl(myHtmlTextWriter);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.Write("Error coming in Export");
                }
                finally
                {

                    Console.Write("Executed Successfully");

                }
            }
        }

    }
}
