﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Net;
using Ionic.Zip;
using Ionic.Zlib;
using System.Net.Mail;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using dbAutoTrack.PDFWriter;
using dbAutoTrack.PDFWriter.Graphics;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;


namespace DigitalSignature
{
    class Program
    {  
        //Create Logger object 
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
        public static string policy_Number;
        static void Main(string[] args)
        {
            try
            {               
                 logger.Info("============= Job Start =============");

                Off_line Offline = new Off_line();
                Offline.Off_line_Mailer();

                //testrelay test = new testrelay();
                // test.test();
             }
             catch (Exception ex)
             {
                 logger.Error("Policy_schedule_Data  : " + ex.Message );
                 
             }
        }  
        }
    }

