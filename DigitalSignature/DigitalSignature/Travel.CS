﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Net;
using Ionic.Zip;
using Ionic.Zlib;
using System.Net.Mail;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using dbAutoTrack.PDFWriter;
using dbAutoTrack.PDFWriter.Graphics;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Mime;

//[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace DigitalSignature
{
    class Travel
    {
        public static log4net.ILog logger1 = log4net.LogManager.GetLogger("frmMain");
        public static string policy_Number;
        public static string Email_id;
        public static string Main_member;
        public static int serialno = 1;

        #region < Travel_Mailer >
        public void Travel_Mailer()
        {
            try
            {

                logger1.Info("=============Travel Job Start =============");
                DataTable dt = new DataTable();
                //get List Of Record From table 
                dt = Policy_schedule_Data("select pol_number, email_id from TBL_Digital_Signature_mailer where status=0 and email_id is not null and Trunc(created_date) = Trunc(sysdate) and username ='Travel'");
                logger1.Info("Total Policy Number : " + dt.Rows.Count.ToString());
                if (dt.Rows.Count != 0) // If datatable Has More than 0 record
                {
                    for (int x = 0; x < dt.Rows.Count; x++) // Loop Upto last record from Data Table
                    {
                        policy_Number = dt.Rows[x][0].ToString(); // Get Policy number
                        Email_id = dt.Rows[x][1].ToString(); //get Email -ID                        
                        logger1.Info("Policy Number : " + dt.Rows[x][0].ToString());
                        Travel_PDF(policy_Number); // Call Method Travel_PDF prospect of particular Policy Number
                    }
                }
                else
                {
                    logger1.Info("Record Not Found "); // if Record Not found 
                }
            }
            catch (Exception ex)
            {
                logger1.Error("Policy_schedule_Data  : " + ex.Message);
                //throw;
            }
        }

        #endregion

        #region < Travel_PDF >
        public static void Travel_PDF(string policy_Number)
        {
            try
            {
                string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                string GetFilelocation = Convert.ToString(ConfigurationSettings.AppSettings["PDFGetPath"].Trim());
                
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                string FileName = policy_Number + ".PDF";
                string Inputpath1 = savelocation + "Travel\\" + date + @"\"; // File Save Location 
                if (!Directory.Exists(Inputpath1)) // Check If Directory Exist or not
                {
                    Directory.CreateDirectory(Inputpath1); // Create Directory
                }
                string Inputpath = savelocation + "Travel\\" + date + @"\"+policy_Number+".pdf";
                string[] filesPath = Directory.GetFiles(GetFilelocation.ToString());
                string PDF_Path=GetFilelocation + policy_Number+".pdf";
                int t=0;

                
                for (int i = 0; i < filesPath.Length; i++) 
                {
                    if (filesPath[i] == PDF_Path)  // Check File Exist In Location Or not
                    {
                        string policy_number = policy_Number;
                        string main_member = "Customer";
                        string E_mailID = Email_id;
                        if (!File.Exists(Inputpath1)) 
                        {
                           // File.Copy(PDF_Path, Inputpath);// if So File Save On  different location

                            byte[] bytes = System.IO.File.ReadAllBytes(PDF_Path);

                            UATsrv.TestWebService Obj_Service = new UATsrv.TestWebService();
                            UATsrv.AuthHeader User_Authorization = new UATsrv.AuthHeader();

                            User_Authorization.Username = Convert.ToString(ConfigurationSettings.AppSettings["SignPDF_Username"].Trim());
                            User_Authorization.Password = Convert.ToString(ConfigurationSettings.AppSettings["SignPDF_Password"].Trim());
                            Obj_Service.AuthHeaderValue = User_Authorization;

                            UATsrv.Res Obj_Response = Obj_Service.SignPDF(bytes, Convert.ToString(ConfigurationSettings.AppSettings["SignPDF_SignerName"].Trim()));
                            
                            System.IO.File.WriteAllBytes(Inputpath, Obj_Response.OutputFile);
                            
                            t = 1;
                            
                        }
                        // Send mail method call with Attachment
                        sendMail(E_mailID, Inputpath, "Apollo Munich Health Insurance Policy Schedule : " + policy_number, policy_number, main_member); 
                    }

                }
                if (t == 0)
                {
                    // If No File Found For particular Policy number
                    string PDF_File = "PDF file not Exist";
                    logger1.Info(PDF_File);
                    Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + PDF_File + "' where pol_number ='" + policy_Number + "' ;");
                }
            }
            catch (Exception ex)
            {
                //update Exception
                logger1.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }
        }

        #endregion

        #region < Policy_schedule_Data  >
        private static DataTable Policy_schedule_Data(string strquery)
        {
            DataTable dtReport = new DataTable();
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = strquery;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = Cmd;
                da.Fill(dtReport);
                return dtReport;
            }
            catch (Exception ex)
            {
                logger1.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + strquery);
                //throw;
                return dtReport;
            }

        }
        #endregion

        #region  < sendMail >

        public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number, string main_member)
        {
            try
            {
                // Get Body part Of mail
                string strPath = Convert.ToString(ConfigurationSettings.AppSettings["IndexHTMLPath"].Trim());
                //string strPath = @"F:\DigitalSignature\DigitalSignature\ops-final.html";  //index.html               
                string strBody = File.ReadAllText(strPath);

                //strBody = strBody.Replace("Customer", main_member);
                //strBody = strBody.Replace("Customer", main_member);

                MailMessage message = new MailMessage();
                string msg_from = "relay<relay@apollomunichinsurance.com>";
                message.From = new MailAddress(msg_from);
                string MessageTo = mailTo;
                //string MessageTo = "vishal.saxena1@apollomunichinsurance.com";
                //message.CC.Add(new MailAddress("mukesh.tiwari@apollomunichinsurance.com"));
                //message.CC.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));
                message.To.Add(new MailAddress(MessageTo));
                message.Subject = subject;
                message.Body = strBody;
                

                LinkedResource logo = new LinkedResource("ops-mailer1_1-01_03.jpg");
                logo.ContentId = "ops-mailer1_1-01_03";
                LinkedResource header = new LinkedResource("ops-mailer1_1-01_06.jpg");
                header.ContentId = "ops-mailer1_1-01_06";
                LinkedResource Step_process = new LinkedResource("step-process.jpg");
                Step_process.ContentId = "step-process";
                LinkedResource policy = new LinkedResource("policy-process.jpg");
                policy.ContentId = "policy-process";
                LinkedResource newimg1 = new LinkedResource("ops-mailer1-142.jpg");
                newimg1.ContentId = "ops-mailer1-142";
                LinkedResource newimg2 = new LinkedResource("ops-mailer1-145.jpg");
                newimg2.ContentId = "ops-mailer1-145";
                LinkedResource newimg3 = new LinkedResource("ops-mailer1-148.jpg");
                newimg3.ContentId = "ops-mailer1-148";
                LinkedResource newimg4 = new LinkedResource("ops-mailer1-151.jpg");
                newimg4.ContentId = "ops-mailer1-151";
                LinkedResource newimg5 = new LinkedResource("ops-mailer1-152.jpg");
                newimg5.ContentId = "ops-mailer1-152";
                LinkedResource newimg6 = new LinkedResource("ops-mailer1-154.jpg");
                newimg6.ContentId = "ops-mailer1-154";
                LinkedResource newimg7 = new LinkedResource("ops-mailer1-155.jpg");
                newimg7.ContentId = "ops-mailer1-155";
                LinkedResource newimg8 = new LinkedResource("ops-mailer-156.jpg");
                newimg8.ContentId = "ops-mailer-156";
                LinkedResource newimg9 = new LinkedResource("ops-mailer1-157.jpg");
                newimg9.ContentId = "ops-mailer1-157";
                LinkedResource newimg10 = new LinkedResource("ops-mailer1-158.jpg");
                newimg10.ContentId = "ops-mailer1-158";
                LinkedResource newimg11 = new LinkedResource("ops-mailer1_1-01_159.jpg");
                newimg11.ContentId = "ops-mailer1_1-01_159";
                LinkedResource newimg12 = new LinkedResource("ops-mailer1_1-01_160.jpg");
                newimg12.ContentId = "ops-mailer1_1-01_160";
                LinkedResource newimg14 = new LinkedResource("ops-mailer1-162.jpg");
                newimg14.ContentId = "ops-mailer1-162";
                LinkedResource newimg13 = new LinkedResource("ops-mailer1-164.jpg");
                newimg13.ContentId = "ops-mailer1-164";

               

                AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);
               
                av1.LinkedResources.Add(logo);
                av1.LinkedResources.Add(header);
                av1.LinkedResources.Add(Step_process);
                av1.LinkedResources.Add(policy);
                av1.LinkedResources.Add(newimg1);
                av1.LinkedResources.Add(newimg2);
                av1.LinkedResources.Add(newimg3);
                av1.LinkedResources.Add(newimg4);
                av1.LinkedResources.Add(newimg5);
                av1.LinkedResources.Add(newimg6);
                av1.LinkedResources.Add(newimg7);
                av1.LinkedResources.Add(newimg8);
                av1.LinkedResources.Add(newimg9);
                av1.LinkedResources.Add(newimg10);
                av1.LinkedResources.Add(newimg11);
                av1.LinkedResources.Add(newimg12);
                av1.LinkedResources.Add(newimg13);
                av1.LinkedResources.Add(newimg14);

                message.AlternateViews.Add(av1);
               

                message.IsBodyHtml = true;

                Attachment attachment = new System.Net.Mail.Attachment(FilePath);

                attachment.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                message.Attachments.Add(attachment);
               
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient(); // Live                
                client.Send(message);
                logger1.Info("Mail Sent : " + mailTo);
               
                Insert_Log(" INSERT INTO tbl_digital_signature_mail_hst (POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,status,SENT_DATE,CREATED_DATE,DMS_IMAGE_ID) ( select POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,1 status,sysdate,CREATED_DATE,DMS_IMAGE_ID from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "') ;");
                Delete_Log(" Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "' ;");

                // Report for End user Console
                Console.WriteLine(+serialno + ". " + "E-mail Send for the : " + Policy_Number + " E-Mail ID - " + mailTo);
                serialno++;

            }
            catch (Exception ex)
            {
                // update Exception
                logger1.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where  email_id ='" + mailTo + "' and pol_number ='" + Policy_Number + "' ;");
                //throw;

            }
            finally
            {

            }
        }

        #endregion


        #region < Update_Log >
        private static void Update_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger1.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Insert_Log >
        private static void Insert_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger1.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Delete_Log >
        private static void Delete_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger1.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion


        // \\172.16.34.80\PolicyPDFs
    }
}
