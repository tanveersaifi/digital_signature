﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using dbAutoTrack.PDFWriter;
using dbAutoTrack.PDFWriter.Graphics;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Health_On_Mailer
{
    class Program
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
        static void Main(string[] args)
        {
            try
            {
               logger.Info("============= Health On Mailer Start =============");

                health_on health = new health_on();
                health.Health_Mailer();
            }
            catch (Exception ex)
            {
                logger.Error("Error  : " + ex.Message);

            }
        }
    }
}
