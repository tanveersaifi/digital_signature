﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Net.Mail;
using System.Linq;
using System.Net.Mime;
using System.Net;
using System.Text;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.DataModel.Core.Context;
using System.Collections.Generic;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Content;
using System.Net;
using DigitalSignature;

namespace Health_On_Mailer
{
    class health_on
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
        public static string policy_Number;
        public static string Email_id;
        public static string Main_member;
        public static int serialno = 1;
        public static string DMS_Image_ID;
        public static string SUM_INSURED;
        public static int flag = 0;
        public static string skipProposal = string.Empty;
        public static string ScheduleFileName = string.Empty;
        public static string Policy_schedulepath = string.Empty;
        public static string Prospect_number = string.Empty;
        public static string fileArray = string.Empty;
        public static string Mobile_number = string.Empty;
        public static string product_code = string.Empty;
        public static string APPLICATIONNO = string.Empty;
        public static string Username = string.Empty;
        DataTable newdt = new DataTable();
        DataTable Recorddt = new DataTable();
        DataTable chkduprecorddt = new DataTable();
        DataTable isStandalone_GPA = new DataTable();
        #region < Health_Mailer >
        public void Health_Mailer()
        {
            try
            {
                logger.Info("=============Health_On Job Start =============");

                //sendMail("a", "", "Test Optima En", "AXE000001", "Gaurav", "", "", 1, "", "Optima_Enhance", "Gaurav");
               

                string squery = " SELECT cpd.strpolnbr policyno, "
                                 + " cpm.strcreatedby, "
                                + "cce.stremailaddr, "
                                + "cpd.dtappln dtappln, "
                                + "0 status, "
                                + "cch.strfilename filename, "
                                + "(SELECT dsa "
                                + "FROM com_pol_prod_dtl@w2e "
                                + "WHERE 1=1 "
                                + "AND nsaccd       =1 "
                                + "AND strpolnbr    = cpd.strpolnbr "
                                + ") SI , "
                                + "(SELECT strprodcd "
                                + "FROM com_pol_prod_dtl@w2e "
                                + "WHERE nsaccd  =1 and nprodtype=1 "
                                + " AND strpolnbr = cpd.strpolnbr "
                                + " AND rownum    =1 "
                                + ") product_Code , "
                                + "(SELECT STRPROPNBR FROM nb_prop_pol_lnk@w2e WHERE STRPOLNBR = cpd.strpolnbr "
                                + ") STRPROPNBR, "
                                + "(select  ccc.STRCONTACTNBR from com_pol_prod_dtl@w2e cppd , COM_CLIENT_CONTACT@w2e ccc  "
                                + "where strpolnbr = cpd.strpolnbr and NSACCD =1 and cppd.STRCLIENTCD = ccc.STRCLIENTCD and ccc.STRCONTACTTYPECD=7 and rownum =1 ) mobile ,cpm.strservagentcd AgentCode, "
                                + "(select strapplnnbr from com_policy_dtl@w2e where strpolnbr = cpd.strpolnbr)application_number, "
                                + "(select ccm.strtitlecd || ' ' ||   ccm.strfirstname || ' ' ||   ccm.strmiddlename || ' ' ||   ccm.strlastname from com_client_m@w2e ccm where ccm.strclientcd = cce.strclientcd ) Main_Member_Name, "
                                + " cpd.strproducttype "
                                + " FROM hsasys.com_policy_dtl@w2e cpd, "
                                + "hsasys.com_contact_hst@w2e cch , "
                                + "com_client_email @w2e cce , "
                                + "com_policy_m @w2e cpm "
                                + "WHERE                  "
                                + "cpd.strpolnbr            = cch.strpolnbr "
                                + "AND cch.strpolnbr            = cpm.strpolnbr "
                                + "AND cce.strclientcd          = cch.strclientcd "
                                + "AND upper(cpm.strcreatedby) IN "
                                + "(SELECT DISTINCT upper(username) FROM tbl_usertable WHERE bstatus=1) "
                                + "AND cch.strfilename IS NOT NULL "
                                + "AND cpm.dtpolcomc   IS NOT NULL "
                                + "AND cpd.strpolnbr not in (select pol_number from tbl_digital_signature_mail_hst) "
                                + "AND cpd.strpolnbr not in (select pol_number from tbl_digital_signature_mailer) "
                                + "AND trunc(to_date(cpm.dtcreated, 'DD-MM-YY HH24:MI:SS')) between trunc(sysdate-1) and trunc(sysdate) "
                                + "AND (cpm.strpolnbr LIKE 'AXP%' OR cpm.strpolnbr LIKE 'AXE%') ";
                                
                
                newdt = Policy_schedule_Data(squery);
                if (newdt.Rows.Count > 0)
                {
                    for (int p = 0; p < newdt.Rows.Count; p++)
                    {
                        string NewPolicyNo = newdt.Rows[p][0].ToString();
                        string Username = newdt.Rows[p][1].ToString();
                        string NewEmail = newdt.Rows[p][2].ToString();
                        string CreatedDate = newdt.Rows[p][3].ToString();
                        string FileId = newdt.Rows[p][5].ToString();
                        string SI = newdt.Rows[p][6].ToString();                       
                        string Product_code = newdt.Rows[p][7].ToString();
                        string Prospect_No = newdt.Rows[p][8].ToString();
                        string Mobile_No = newdt.Rows[p][9].ToString();
                        string AgentCode = newdt.Rows[p][10].ToString();
                        string Application_no = newdt.Rows[p][11].ToString();
                        string Member_Name = newdt.Rows[p][12].ToString();
                        string Strprodcttype = newdt.Rows[p][13].ToString();
                        if (SI == "")                        
                            SI = null;

                        //========================================For GPA Policy==============================
                        if (Product_code == "22300" || Product_code == "22301" || Product_code == "22302" || Product_code == "22303")
                        {
                            if (Strprodcttype == "8")
                            {
                                Insert_Log(" INSERT INTO tbl_digital_signature_mailer (POL_NUMBER,USERNAME,EMAIL_ID,ERROR_LOG,status,CREATED_DATE,DMS_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No,BUSINESSTYPE,MobileNo,AGENTCODE,APPLICATIONNO,standalone,MAIN_MEMBER_NAME) values ('" + NewPolicyNo.ToString() + "','" + Username.ToString() + "','" + NewEmail.ToString() + "',null,'0',sysdate,'" + FileId.ToString() + "','" + SI + "','" + Product_code + "','" + Prospect_No + "','Online_GPA','" + Mobile_No + "','" + AgentCode + "','" + Application_no + "','1','" + Member_Name.ToString() + "') ;");
                                logger.Info("Record insert into Mailer Table : policy Number : '" + NewPolicyNo.ToString() + "'");
                                //SMSrecord = Policy_schedule_Data("select pol_number from TBL_DIGITAL_SMS where pol_number='" + NewPolicyNo.ToString() + "'");                                
                                Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS,MAIN_MEMBER_NAME) values ('" + NewPolicyNo.ToString() + "','" + Mobile_No + "','0','" + Member_Name.ToString() + "') ;");
                            }
                        }
                        //==================================For Optima Enhance Policy============================
                        else if (Product_code == "12765" || Product_code == "12766")
                        {
                            Insert_Log(" INSERT INTO tbl_digital_signature_mailer (POL_NUMBER,USERNAME,EMAIL_ID,ERROR_LOG,status,CREATED_DATE,DMS_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No,BUSINESSTYPE,MobileNo,AGENTCODE,APPLICATIONNO,standalone,MAIN_MEMBER_NAME) values ('" + NewPolicyNo.ToString() + "','" + Username.ToString() + "','" + NewEmail.ToString() + "',null,'0',sysdate,'" + FileId.ToString() + "','" + SI + "','" + Product_code + "','" + Prospect_No + "','Online_Optima_Enhance','" + Mobile_No + "','" + AgentCode + "','" + Application_no + "','1','" + Member_Name.ToString() + "') ;");
                            logger.Info("Record insert into Mailer Table : policy Number : '" + NewPolicyNo.ToString() + "'");
                            Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS,MAIN_MEMBER_NAME) values ('" + NewPolicyNo.ToString() + "','" + Mobile_No + "','0','" + Member_Name.ToString() + "') ;");
                        }
                    }
                }


                DataTable dt_health_on = new DataTable();
               //dt_health_on = Policy_schedule_Data("Select pol_number, email_id,main_member_name,dms_image_id,SUM_INSURED,Prospect_No,MobileNo,product_code,APPLICATIONNO from tbl_digital_signature_mailer where email_id IS NOT NULL and dms_image_id IS NOT NULL and status=0 and Product_Code in ('11228','11229','11230','11231','22300','22301','22302','22303') and pol_number in ('AX00041703') order by pol_number desc");
                dt_health_on = Policy_schedule_Data("Select pol_number,Username, email_id,main_member_name,dms_image_id,SUM_INSURED,Prospect_No,MobileNo,product_code,APPLICATIONNO from tbl_digital_signature_mailer where email_id IS NOT NULL and dms_image_id IS NOT NULL and status=0 and Product_Code in ('11228','11229','11230','11231','22300','22301','22302','22303','12765','12766') and Trunc(created_date) between trunc(sysdate-1) and trunc(sysdate) ");


                Console.WriteLine("Total Numbers of Health_On Records : " + dt_health_on.Rows.Count);
                logger.Info("Total Health_On Policy Number : " + dt_health_on.Rows.Count.ToString());

                if (dt_health_on.Rows.Count > 0)
                {
                    for (int x = 0; x < dt_health_on.Rows.Count; x++)
                    {
                        policy_Number = dt_health_on.Rows[x]["pol_number"].ToString();
                        Username = dt_health_on.Rows[x]["username"].ToString();
                        Email_id = dt_health_on.Rows[x]["email_id"].ToString();
                        Main_member = dt_health_on.Rows[x]["main_member_name"].ToString();
                        DMS_Image_ID = dt_health_on.Rows[x]["dms_image_id"].ToString().Trim();
                        SUM_INSURED = dt_health_on.Rows[x]["SUM_INSURED"].ToString();
                        Prospect_number = dt_health_on.Rows[x]["Prospect_No"].ToString();
                        Mobile_number = dt_health_on.Rows[x]["MobileNo"].ToString();
                        product_code = dt_health_on.Rows[x]["product_code"].ToString();
                        APPLICATIONNO = dt_health_on.Rows[x]["APPLICATIONNO"].ToString();
                        logger.Info("Policy Number : " + dt_health_on.Rows[x][0].ToString());
                        string Location = Convert.ToString(ConfigurationSettings.AppSettings["FileArray"].Trim());
                        fileArray = Location + DMS_Image_ID + "";
                        string Product = string.Empty;
                        DataTable proddt = new DataTable();
                        DataTable bundleddt = new DataTable();
                        DataTable Clientcd = new DataTable();
                        DataTable selectImg = new DataTable();
                        int allok = 0;
                        string pol_no = string.Empty;
                        string gpa_img = string.Empty;
                        skipProposal = string.Empty;
                        Policy_schedulepath = string.Empty;

                        //==================================For Health On Policy============================
                        if (product_code == "11228" || product_code == "11229" || product_code == "11230" || product_code == "11231")
                        {
                            logger.Info("Health On product");
                            proddt = Policy_schedule_Data("select STRPRODUCTTYPE from COM_POLICY_DTL@w2e where strpolnbr='" + policy_Number.ToString() + "' ");
                            string PRODTYPE = string.Empty;
                            if (proddt.Rows.Count > 0)
                            {
                                logger.Info("Bundled Product in loop Health On");
                                PRODTYPE = proddt.Rows[0][0].ToString();
                                if (PRODTYPE == "")
                                {
                                    skipProposal = "true";  // Skip Proposal form for cases which belong from buyonline 
                                }
                                else
                                {
                                    skipProposal = string.Empty;
                                }
                                if (PRODTYPE == "9")
                                {
                                    Product = "Bundled";
                                    bundleddt = Policy_schedule_Data("select strclientcd from com_policy_m@w2e where  strpolnbr='" + policy_Number.ToString() + "'");                                    
                                    if (bundleddt.Rows.Count > 0)
                                    {
                                       string clientcd = bundleddt.Rows[0][0].ToString();
                                       logger.Info("Client code : " + clientcd.ToString());
                                       Clientcd = Policy_schedule_Data("select strpolnbr from com_policy_m@w2e where strclientcd='" + clientcd.ToString() + "' and strpolnbr not in ('" + policy_Number.ToString() + "')");
                                        if (Clientcd.Rows.Count > 0)
                                       {
                                           allok = 1;
                                           pol_no = Clientcd.Rows[0][0].ToString();
                                           logger.Info("Bundled policy Number : " + pol_no.ToString());
                                       }
                                    }                                 
                                    selectImg = Policy_schedule_Data("select strfilename from com_contact_hst@w2e where strpolnbr='" + pol_no.ToString() + "'");
                                    if (selectImg.Rows.Count > 0)
                                    {
                                        gpa_img = selectImg.Rows[0][0].ToString().Trim();
                                        logger.Info("bundled Image ID : " + gpa_img.ToString());
                                        Update_Log("update tbl_digital_signature_mailer set gpaimageid ='" + gpa_img.ToString().Trim() + "',standalone=0,bundled_pol_no='" + pol_no.ToString() + "'  where pol_number ='" + policy_Number.ToString() + "';");
                                        // Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS,MAIN_MEMBER_NAME) values ('" + pol_no.ToString() + "','" + Mobile_number + "','0','"+Main_member.ToString() +"') ;");
                                    }
                                   // gpa_img = "610AA00179363270916130024.PDF";
                                   // Update_Log("update tbl_digital_signature_mailer set gpaimageid ='" + gpa_img.ToString().Trim() + "',standalone=0,bundled_pol_no='" + pol_no.ToString() + "'  where pol_number ='" + policy_Number.ToString() + "';");
                                }
                                else if (PRODTYPE == "7" || PRODTYPE == "")
                                {
                                    allok = 1;
                                    Product = "Health_On";
                                }
                            }
                        }

                        //==================================For GPA Policy============================
                        else if (product_code == "22300" || product_code == "22301" || product_code == "22302" || product_code == "22303")
                        {
                            proddt = Policy_schedule_Data("select STRPRODUCTTYPE from COM_POLICY_DTL@w2e where strpolnbr='" + policy_Number.ToString() + "' ");
                            string PRODTYPE = string.Empty;
                            if (proddt.Rows.Count > 0)
                            {
                                logger.Info("Bundled Product in loop GPA");
                                PRODTYPE = proddt.Rows[0][0].ToString();
                                if (PRODTYPE == "9")
                                {
                                    Product = "Bundled";
                                    bundleddt = Policy_schedule_Data("select strclientcd from com_policy_m@w2e where  strpolnbr='" + policy_Number.ToString() + "'");
                                    if (bundleddt.Rows.Count > 0)
                                    {
                                        string clientcd = bundleddt.Rows[0][0].ToString();
                                        Clientcd = Policy_schedule_Data("select strpolnbr from com_policy_m@w2e where strclientcd='" + clientcd.ToString() + "' and strpolnbr not in ('" + policy_Number.ToString() + "')");
                                        if (Clientcd.Rows.Count > 0)
                                        {
                                            allok = 1;
                                            pol_no = Clientcd.Rows[0][0].ToString();
                                        }
                                    }
                                    selectImg = Policy_schedule_Data("select strfilename from com_contact_hst@w2e where strpolnbr='" + pol_no.ToString() + "'");
                                    if (selectImg.Rows.Count > 0)
                                    {
                                        gpa_img = selectImg.Rows[0][0].ToString().Trim();
                                        Update_Log("update tbl_digital_signature_mailer set gpaimageid ='" + gpa_img.ToString().Trim() + "',standalone=0,bundled_pol_no='" + pol_no.ToString() + "'  where pol_number ='" + policy_Number.ToString() + "';");
                                        // Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS) values ('" + pol_no.ToString() + "','" + Mobile_number + "','0') ;");
                                    }
                                }
                                else if (PRODTYPE == "8")
                                {
                                    allok = 1;
                                    Product = "GPA";
                                }
                            }
                        }

                        //==================================For Optima Enhance Policy============================
                        else if (product_code == "12765" || product_code == "12766")
                        {
                            allok = 1;
                            Product = "Optima_Enhance";
                        }
                        //if (allok == 1)
                        //{
                        //    GetWebsiteHtml(fileArray, policy_Number + ".pdf", Product);
                        //}
                        //else
                        //{
                        //    logger.Info("Bundled(GPA) policy not found with client code");
                        //    Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Bundled(GPA) policy not found with client code' where pol_number ='" + policy_Number.ToString() + "' ;");
                        //}

                        if (allok == 1)
                        {
                            //GetWebsiteHtml(fileArray, policy_Number + ".pdf", Product);

                            bool isapollo = Email_id.Contains("@apollomunichinsurance.com");
                            if (isapollo == true)
                            {
                                GetWebsiteHtml(fileArray, policy_Number + ".pdf", Product);
                            }
                            else
                            {
                                logger.Info("Email can not trigger out of domain");
                                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Email can not trigger out of domain' where pol_number ='" + policy_Number.ToString() + "' ;");
                            }
                        }
                        else
                        {
                            logger.Info("Bundled(GPA) policy not found with client code");
                            Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Bundled(GPA) policy not found with client code' where pol_number ='" + policy_Number.ToString() + "' ;");
                        }

                    }
                }
                else
                {
                    logger.Info("Record Not Found ");
                }
            }

            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message);
            }
        }
        #endregion

        #region < GetWebsiteHtml >
        private static void GetWebsiteHtml(string url, string strfilene, string Product)
        {
            try
            {
                logger.Info(strfilene + " Product : " + Product);
                byte[] bytes = null;
                GenerateArchivePDF(bytes, strfilene, Product);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }
        }
        #endregion

        #region < GenerateArchivePDF >
        protected static void GenerateArchivePDF(byte[] buffer, string strfilene, string Product)
        {
            try
            {
                string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                string FileName = strfilene;
                flag = 0;
                string Inputpath = savelocation + "Health_On_UAT\\" + date + @"\";

                #region Health On
                if (Product == "Health_On")
                {
                    if (!Directory.Exists(Inputpath))
                    {
                        Directory.CreateDirectory(Inputpath);
                    }
                    try
                    {
                        WebClient webClient = new WebClient();
                        webClient.DownloadFile(fileArray, Inputpath + FileName);
                        logger.Info("Policy Download at specified location");
                        webClient.Dispose();
                        if (skipProposal == string.Empty)
                        {
                            try
                            {
                                logger.Info("DMS function call..");
                                DMS_Get_Image(APPLICATIONNO);
                                // flag = 1;
                            }
                            catch (Exception ex)
                            {
                                flag = 0;
                                logger.Error(ex.Message);
                                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Unable to Download proposal from DMS service' where pol_number ='" + policy_Number + "' ;");
                            }
                        }
                        else
                        {
                            flag = 1;
                        }

                        int SIFlag = 0;
                        if (flag == 1)
                        {
                            //if (Convert.ToInt32(SUM_INSURED) <= 1000000)
                            //{
                            //    SIFlag = 0;
                            //}
                            //else
                            //{
                            //    SIFlag = 1;
                            //}
                            string policy_number = policy_Number;
                            string Subject = "Apollo Munich Health Insurance Policy :Health On " + policy_number;
                            //string policyschedule = "F:\\611AX00041703220217214142.PDF.pdf";
                            // sendMail(Email_id, policyschedule, Subject, policy_number, Main_member, Policy_schedulepath + ScheduleFileName, product_code, SIFlag, "", "Health_on");
                            sendMail(Email_id, Inputpath + FileName, Subject, policy_number, Main_member, Policy_schedulepath + ScheduleFileName, product_code, SIFlag, "", "Health_on", Username);
                        }
                    }

                    catch (Exception ex)
                    {
                        string Err = "Failure to write PDF";
                        logger.Error(Err.ToString());
                        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                    }
                }
                #endregion

                #region GPA
                else if (Product == "GPA")
                {
                    if (!Directory.Exists(Inputpath))
                    {
                        Directory.CreateDirectory(Inputpath);
                    }
                    try
                    {
                        int newflag = 0;
                        //Download COI
                        WebRequest request = WebRequest.Create(fileArray);
                        WebResponse response = request.GetResponse();
                        Stream stream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(stream);
                        byte[] bytesss = Encoding.UTF8.GetBytes(reader.ReadToEnd());   // Convert image into stream of bytes
                        stream.Dispose();
                        reader.Dispose();
                        try
                        {
                            File.WriteAllBytes(Inputpath + FileName, bytesss);
                            newflag = 1;
                        }
                        catch (Exception ex)
                        {
                            string Err = "Failure to write COI";
                            newflag = 0;
                            logger.Error(ex.Message);
                            Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                        }
                        if (newflag == 1)
                        {
                            string policy_number = policy_Number;
                            string Subject = "Apollo Munich Health Insurance Policy :GPA " + policy_number;
                            sendMail(Email_id, Inputpath + FileName, Subject, policy_number, Main_member, "", "", 2, "", "GPA", "");
                        }
                    }
                    catch (Exception ex)
                    {
                        string Err = "Failure to download policy schedule";
                        logger.Error(ex.Message.ToString());
                        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                    }
                    //WebClient webClient = new WebClient();
                    //webClient.DownloadFile(fileArray, Inputpath + FileName);
                    //webClient.Dispose();
                    //string policy_number = policy_Number;
                    //string Subject = "Apollo Munich Health Insurance Policy :GPA " + policy_number;
                    //sendMail(Email_id, Inputpath + FileName, Subject, policy_number, Main_member, "", "", 2, "", "GPA");
                }
                //string policy_number1 = policy_Number;
                // string Subject1 = "Apollo Munich Health Insurance Policy" + policy_number1;
                //sendMail(Email_id, Inputpath + FileName, Subject1, policy_number1, Main_member, Policy_schedulepath + ScheduleFileName, product_code, "", "");


                #endregion

                #region Bundled
                else if (Product == "Bundled")
                {
                    if (!Directory.Exists(Inputpath))
                    {
                        Directory.CreateDirectory(Inputpath);
                    }
                    try
                    {
                        DataTable gpaimgiddt = new DataTable();
                        string gpaimgid = string.Empty;
                        string Gpaps = string.Empty;
                        string bundled_pol_no = string.Empty;
                        gpaimgiddt = Policy_schedule_Data("select gpaimageid,bundled_pol_no  from tbl_digital_signature_mailer where pol_number ='" + policy_Number + "'");

                        if (gpaimgiddt.Rows.Count > 0)
                        {
                            gpaimgid = gpaimgiddt.Rows[0][0].ToString();
                            bundled_pol_no = gpaimgiddt.Rows[0][1].ToString();
                        }

                        string Location = Convert.ToString(ConfigurationSettings.AppSettings["FileArray"].Trim());
                        WebClient webClient1 = new WebClient();
                        string filearry1 = Location + gpaimgid;
                        Gpaps = Inputpath + bundled_pol_no + ".pdf";

                        WebRequest request = WebRequest.Create(filearry1);
                        WebResponse response = request.GetResponse();
                        Stream stream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(stream);
                        byte[] bytesss = Encoding.UTF8.GetBytes(reader.ReadToEnd());   // Convert image into stream of bytes
                        stream.Dispose();
                        reader.Dispose();
                        try
                        {
                            File.WriteAllBytes(Gpaps, bytesss);
                            flag = 1;
                        }
                        catch (Exception ex)
                        {
                            string Err = "Failure to write bundled COI";
                            flag = 0;
                            logger.Error(ex.Message);
                            Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                        }

                        //Download GPA policy schedule
                        //string Location = Convert.ToString(ConfigurationSettings.AppSettings["FileArray"].Trim());
                        //WebClient webClient1 = new WebClient();
                        //string filearry1 = Location + gpaimgid;
                        //Gpaps = Inputpath + bundled_pol_no + ".pdf";
                        //webClient1.DownloadFile(filearry1, Gpaps);
                        //webClient1.Dispose();

                        //Download Health_On policy Schedule
                        WebClient webClient = new WebClient();
                        webClient.DownloadFile(fileArray, Inputpath + FileName);
                        webClient.Dispose();
                        try
                        {
                            DMS_Get_Image(APPLICATIONNO);
                            // flag = 1;
                        }
                        catch (Exception ex)
                        {
                            flag = 0;
                            logger.Error(ex.Message);
                            Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Unable to Download proposal from DMS service' where pol_number ='" + policy_Number + "' ;");
                        }
                        int SIFlag = 0;
                        if (flag == 1)
                        {
                            //if (Convert.ToInt32(SUM_INSURED) <= 1000000)
                            //{
                            //    SIFlag = 0;
                            //}
                            //else
                            //{
                            //    SIFlag = 1;
                            //}
                            string policy_number = policy_Number;
                            string Subject = "Apollo Munich Health Insurance Policy : Bundled " + policy_number;
                            sendMail(Email_id, Inputpath + FileName, Subject, policy_number, Main_member, Policy_schedulepath + ScheduleFileName, product_code, SIFlag, Gpaps, "Bundled", "");
                        }
                    }
                    catch (Exception ex)
                    {
                        string Err = "Failure to write PDF";
                        logger.Error(Err.ToString());
                        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                    }
                }
                #endregion

                #region Optima_Enhance
                else if (Product == "Optima_Enhance")
                {
                    if (!Directory.Exists(Inputpath))
                    {
                        Directory.CreateDirectory(Inputpath);
                    }
                    try
                    {
                        int newflag = 0;
                        //Download COI
                        try
                        {
                            WebRequest request = WebRequest.Create(fileArray);
                            WebResponse response = request.GetResponse();
                            Stream stream = response.GetResponseStream();
                            StreamReader reader = new StreamReader(stream);
                            byte[] bytesss = Encoding.UTF8.GetBytes(reader.ReadToEnd());   // Convert image into stream of bytes
                            stream.Dispose();
                            reader.Dispose();
                            File.WriteAllBytes(Inputpath + FileName, bytesss);
                            newflag = 1;
                        }
                        catch (Exception ex)
                        {
                            string Err = "Failure to write COI";
                            newflag = 0;
                            logger.Error(ex.Message);
                            Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                        }

                        try
                        {
                            if (newflag == 1)
                            {
                                DMS_Get_Image(APPLICATIONNO);
                                newflag = flag;
                            }
                        }
                        catch (Exception ex)
                        {
                            newflag = 0;
                            logger.Error(ex.Message);
                            Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Unable to Download proposal from DMS service' where pol_number ='" + policy_Number + "' ;");
                        }

                        if (newflag == 1)
                        {
                            string policy_number = policy_Number;
                            string Subject = "Apollo Munich Health Insurance Policy :Optima Enhance " + policy_number;
                            sendMail(Email_id, Inputpath + FileName, Subject, policy_number, Main_member, Policy_schedulepath + ScheduleFileName, "", 2, "", "Optima_Enhance", "");
                        }
                    }
                    catch (Exception ex)
                    {
                        string Err = "Failure to download policy schedule";
                        logger.Error(ex.Message.ToString());
                        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }

        }
        #endregion

        #region < DMS_Get_Image >

        public static void DMS_Get_Image(string ProspectNumber)
        {
            try
            {
                String moduleName = Convert.ToString(ConfigurationSettings.AppSettings["Img_moduleName"].Trim());
                String contextRoot = Convert.ToString(ConfigurationSettings.AppSettings["Img_contextRoot"].Trim());
                String repository = Convert.ToString(ConfigurationSettings.AppSettings["Img_repository"].Trim());
                String user = Convert.ToString(ConfigurationSettings.AppSettings["Img_user"].Trim());
                String password = Convert.ToString(ConfigurationSettings.AppSettings["Img_password"].Trim());

                ContextFactory contextFactory = ContextFactory.Instance;
                IServiceContext context = contextFactory.NewContext();
                RepositoryIdentity repoId = new RepositoryIdentity();

                repoId.RepositoryName = repository;
                repoId.UserName = user;
                repoId.Password = (password);
                context.AddIdentity(repoId);
                IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);
                ServiceFactory serviceFactory = ServiceFactory.Instance;
                IListDocument service = serviceFactory.GetRemoteService<IListDocument>(registeredContext, moduleName, contextRoot);
                int type = 2;

                string[] arr = { "adkv_proposal_doc" };//adkv_printer_doc
                List<string> docTypes = new List<string>(arr);
                List<ListBean> policySchdLst = service.GetListOfDocuments(ProspectNumber, type, docTypes);

                object obj = policySchdLst;

                if (policySchdLst.Count >= 1)
                {
                    bool find = false;
                    for (int i = 0; i < policySchdLst.Count; i++)
                    {
                        //if (Convert.ToString(policySchdLst[i].objectName).Contains("Proposer"))//Policy Schedule
                        //{
                        // update image Id in Database For Particualr Policy Number 
                        //Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate,DMS_Image_id='" + policySchdLst[i].objectId + "' where pol_number ='" + policy_Number + "' ;");
                        find = true;
                        GetDMSReports(policySchdLst[i].objectId, ProspectNumber);
                        break;
                        //}
                    }
                }
                else
                {
                    flag = 0;
                    string Image_Error = "Image Id Not Found";
                    logger.Info(Image_Error);
                    Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Image_Error + "' where pol_number ='" + policy_Number + "' ;");
                }
            }
            catch (Exception ex)
            {
                flag = 0;
                logger.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }
        }

        #endregion

        #region < GetDMSReports >
        public static void GetDMSReports(string strImageId, string ProspectNumber)
        {
            logger.Info("GetDMSreport Function call...");
            string strFileName = string.Empty;
            try
            {
                String moduleName = Convert.ToString(ConfigurationSettings.AppSettings["File_moduleName"].Trim());
                String contextRoot = Convert.ToString(ConfigurationSettings.AppSettings["File_contextRoot"].Trim());
                String repository = Convert.ToString(ConfigurationSettings.AppSettings["File_repository"].Trim());
                String user = Convert.ToString(ConfigurationSettings.AppSettings["File_user"].Trim());
                String password = Convert.ToString(ConfigurationSettings.AppSettings["File_password"].Trim());

                ContextFactory contextFactory = ContextFactory.Instance;
                IServiceContext context = contextFactory.NewContext();
                RepositoryIdentity repoId = new RepositoryIdentity();

                repoId.RepositoryName = repository;
                repoId.UserName = user;
                repoId.Password = (password);
                context.AddIdentity(repoId);

                IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);

                ServiceFactory serviceFactory = ServiceFactory.Instance;
                IObjectService service = serviceFactory.GetRemoteService<IObjectService>(registeredContext, moduleName, contextRoot);

                ContentTransferProfile transferProfile = new ContentTransferProfile();
                transferProfile.TransferMode = ContentTransferMode.MTOM;
                transferProfile.Geolocation = "";
                registeredContext.SetProfile(transferProfile);

                ObjectIdentity objectIdentity = new ObjectIdentity(repository);
                ObjectId objId = new ObjectId(strImageId);
                objectIdentity.Value = objId;
                byte[] byteArray;
                ContentProfile contentProfile = new ContentProfile();
                contentProfile.FormatFilter = FormatFilter.ANY;

                OperationOptions operationOptions = new OperationOptions();
                operationOptions.ContentProfile = contentProfile;

                operationOptions.SetProfile(contentProfile);
                ObjectIdentitySet objectIdSet = new ObjectIdentitySet();

                List<ObjectIdentity> objIdList = objectIdSet.Identities;
                objIdList.Add(objectIdentity);
                DataPackage dataPackage = service.Get(objectIdSet, operationOptions);
                DataObject dataObject = dataPackage.DataObjects[0];
                Emc.Documentum.FS.DataModel.Core.Content.Content resultContent = dataObject.Contents[0];

                ScheduleFileName = string.Empty;
                Policy_schedulepath = string.Empty;

                if (resultContent.CanGetAsFile())
                {
                    strFileName = strImageId + "." + Convert.ToString(resultContent.Format);
                    byteArray = resultContent.GetAsByteArray();
                    string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                    string date = DateTime.Now.ToString("dd-MM-yyyy");
                    string type = resultContent.Format;
                    ScheduleFileName = ProspectNumber + "." + type;
                    Policy_schedulepath = savelocation + "Health_On_Policy_Schedule\\" + date + @"\";
                    if (!Directory.Exists(Policy_schedulepath))
                    {
                        Directory.CreateDirectory(Policy_schedulepath);
                    }
                    try
                    {
                        File.WriteAllBytes(Policy_schedulepath + ScheduleFileName, byteArray);
                        logger.Info("Proposal form Download at location...");
                        flag = 1;
                    }
                    catch (Exception ex)
                    {
                        flag = 0;
                        string error = "Exception at the time of write proposal form";
                        logger.Error(ex.Message);
                        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + error.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                    }
                }
                else
                {
                    flag = 0;
                    string Error = "PDF File Not Found in resultcontent ";
                    logger.Info(Error.ToString());
                    Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Error + "' where pol_number ='" + policy_Number + "' ;");
                }
            }
            catch (Exception ex)
            {
                flag = 0;
                string error = "DMS Report Exception";
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + error.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                logger.Error("DMS Report Exception  : " + ex.Message);
            }
        }
        #endregion

        #region < Policy_schedule_Data >
        private static DataTable Policy_schedule_Data(string strquery)
        {
            DataTable dtReport = new DataTable();
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = strquery;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = Cmd;
                da.Fill(dtReport);
                return dtReport;
            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + strquery);
                //throw;
                return dtReport;
            }

        }
        #endregion

        #region < sendMail >

        public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number, string main_member, string proposal_form, string product_code, int SIFlag, string Gpapath, string type,string username)
        {
            try
            {
                logger.Info("Send mail function call :" + type);
                string strPath = string.Empty;
                string ProductType = string.Empty;
                if (skipProposal != string.Empty)
                {
                    proposal_form = string.Empty;
                }
                if (type == "Health_on")
                {
                    if (username == "AxisADC")
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["standalone_health_on_proposal"].Trim());
                    }
                    else
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["standalone_health_on"].Trim());
                        //ProductType = "Health_on";
                    }
                }
                else if (type == "GPA")
                {
                    strPath = Convert.ToString(ConfigurationSettings.AppSettings["GPA_standalone"].Trim());
                    // ProductType = "GPA";
                }
                else if (type == "Bundled")
                {
                    strPath = Convert.ToString(ConfigurationSettings.AppSettings["Bundled_GPA"].Trim());
                    // ProductType = "Bundled";
                }
                else if (type == "Optima_Enhance")
                {
                    strPath = Convert.ToString(ConfigurationSettings.AppSettings["Optima_Enhance"].Trim());                   
                }

                string ImagesPathMain = Convert.ToString(ConfigurationSettings.AppSettings["ImagesPath"].Trim());

                string strBody = File.ReadAllText(strPath);
                MailMessage message = new MailMessage();
                string msg_from = "relay<relay@apollomunichinsurance.com>";
                message.From = new MailAddress(msg_from);
                string MessageTo = mailTo;
                //string MessageTo = "gaurav.kumar1@apollomunichinsurance.com";
                message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));              
                message.To.Add(new MailAddress(MessageTo));
                message.Subject = subject;
                message.Body = strBody;
                message.IsBodyHtml = true;

                if (type == "Health_on")
                {
                    if (username == "AxisADC")
                    {
                        #region standloane health On without proposal form
                        LinkedResource logo = new LinkedResource(ImagesPathMain + "skip_proposal_for\\head.jpg");
                        logo.ContentId = "head";
                        LinkedResource header = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_14.jpg");
                        header.ContentId = "ops-mailer1_1-01_14";
                        LinkedResource Step_process = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_15.jpg");
                        Step_process.ContentId = "ops-mailer1_1-01_15";
                        LinkedResource policy = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_17.jpg");
                        policy.ContentId = "ops-mailer1_1-01_17";
                        LinkedResource newimg1 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_18.jpg");
                        newimg1.ContentId = "ops-mailer1_1-01_18";
                        LinkedResource newimg2 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_20.jpg");
                        newimg2.ContentId = "ops-mailer1_1-01_20";
                        LinkedResource newimg3 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_21.jpg");
                        newimg3.ContentId = "ops-mailer1_1-01_21";
                        LinkedResource newimg4 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_46.jpg");
                        newimg4.ContentId = "ops-mailer1_1-01_46";
                        LinkedResource newimg5 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_26.jpg");
                        newimg5.ContentId = "ops-mailer1_1-01_26";
                        // LinkedResource newimg6 = new LinkedResource(ImagesPathMain + "GPA_standalone\\ops-mailer1_1-01_27.jpg");

                        LinkedResource newimg9 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_27.jpg");
                        newimg9.ContentId = "ops-mailer1_1-01_27";

                        LinkedResource newimg8 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_31.jpg");
                        newimg8.ContentId = "ops-mailer1_1-01_31";


                        LinkedResource newimg10 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_34.jpg");
                        newimg10.ContentId = "ops-mailer1_1-01_34";

                        LinkedResource newimg12 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_39.jpg");
                        newimg12.ContentId = "ops-mailer1_1-01_39";

                        LinkedResource newimg13 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_40.jpg");
                        newimg13.ContentId = "ops-mailer1_1-01_40";

                        LinkedResource newimg14 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_42.jpg");
                        newimg14.ContentId = "ops-mailer1_1-01_42";

                        LinkedResource newimg15 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_43.jpg");
                        newimg15.ContentId = "ops-mailer1_1-01_43";

                        LinkedResource newimg16 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_45.jpg");
                        newimg16.ContentId = "ops-mailer1_1-01_45";

                        LinkedResource newimg18 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_48.jpg");
                        newimg18.ContentId = "ops-mailer1_1-01_48";

                        LinkedResource newimg19 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_49.jpg");
                        newimg19.ContentId = "ops-mailer1_1-01_49";

                        LinkedResource newimg21 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_51.jpg");
                        newimg21.ContentId = "ops-mailer1_1-01_51";

                        LinkedResource newimg25 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_56.jpg");
                        newimg25.ContentId = "ops-mailer1_1-01_56";

                        LinkedResource newimg26 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_57.jpg");
                        newimg26.ContentId = "ops-mailer1_1-01_57";

                        LinkedResource newimg27 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_59.jpg");
                        newimg27.ContentId = "ops-mailer1_1-01_59";

                        LinkedResource newimg28 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_60.jpg");
                        newimg28.ContentId = "ops-mailer1_1-01_60";

                        LinkedResource newimg29 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\AMHI-logo.jpg");
                        newimg29.ContentId = "AMHI-logo";

                        LinkedResource newimg30 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\Axis-logo.jpg");
                        newimg30.ContentId = "Axis-logo";

                        LinkedResource newimg31 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_162.jpg");
                        newimg31.ContentId = "ops-mailer1_1-01_162";

                        LinkedResource newimg32 = new LinkedResource(ImagesPathMain + "skip_proposal_for\\ops-mailer1_1-01_152.jpg");
                        newimg32.ContentId = "ops-mailer1_1-01_152";


                        AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                        av1.LinkedResources.Add(logo);
                        av1.LinkedResources.Add(header);
                        av1.LinkedResources.Add(Step_process);
                        av1.LinkedResources.Add(policy);
                        av1.LinkedResources.Add(newimg1);
                        av1.LinkedResources.Add(newimg2);
                        av1.LinkedResources.Add(newimg3);
                        av1.LinkedResources.Add(newimg4);
                        av1.LinkedResources.Add(newimg5);
                        // av1.LinkedResources.Add(newimg6);

                        av1.LinkedResources.Add(newimg8);
                        av1.LinkedResources.Add(newimg9);
                        av1.LinkedResources.Add(newimg10);
                        av1.LinkedResources.Add(newimg12);
                        av1.LinkedResources.Add(newimg13);
                        av1.LinkedResources.Add(newimg14);
                        av1.LinkedResources.Add(newimg15);
                        av1.LinkedResources.Add(newimg16);
                        av1.LinkedResources.Add(newimg18);
                        av1.LinkedResources.Add(newimg19);
                        av1.LinkedResources.Add(newimg21);
                        av1.LinkedResources.Add(newimg25);
                        av1.LinkedResources.Add(newimg26);
                        av1.LinkedResources.Add(newimg27);
                        av1.LinkedResources.Add(newimg28);
                        av1.LinkedResources.Add(newimg29);
                        av1.LinkedResources.Add(newimg30);
                        av1.LinkedResources.Add(newimg31);
                        av1.LinkedResources.Add(newimg32);

                        message.AlternateViews.Add(av1);
                        #endregion
                    }
                    else
                    {
                        #region Standalone Health On

                        LinkedResource logo = new LinkedResource(ImagesPathMain + "standalone_health_on\\head.jpg");
                        logo.ContentId = "head";
                        LinkedResource header = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_14.jpg");
                        header.ContentId = "ops-mailer1_1-01_14";
                        LinkedResource Step_process = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_15.jpg");
                        Step_process.ContentId = "ops-mailer1_1-01_15";
                        LinkedResource policy = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_17.jpg");
                        policy.ContentId = "ops-mailer1_1-01_17";
                        LinkedResource newimg1 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_18.jpg");
                        newimg1.ContentId = "ops-mailer1_1-01_18";
                        LinkedResource newimg2 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_20.jpg");
                        newimg2.ContentId = "ops-mailer1_1-01_20";
                        LinkedResource newimg3 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_21.jpg");
                        newimg3.ContentId = "ops-mailer1_1-01_21";
                        LinkedResource newimg4 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_46.jpg");
                        newimg4.ContentId = "ops-mailer1_1-01_46";
                        LinkedResource newimg5 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_26.jpg");
                        newimg5.ContentId = "ops-mailer1_1-01_26";
                        // LinkedResource newimg6 = new LinkedResource(ImagesPathMain + "GPA_standalone\\ops-mailer1_1-01_27.jpg");

                        LinkedResource newimg9 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_27.jpg");
                        newimg9.ContentId = "ops-mailer1_1-01_27";

                        LinkedResource newimg8 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_31.jpg");
                        newimg8.ContentId = "ops-mailer1_1-01_31";


                        LinkedResource newimg10 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_34.jpg");
                        newimg10.ContentId = "ops-mailer1_1-01_34";

                        LinkedResource newimg12 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_39.jpg");
                        newimg12.ContentId = "ops-mailer1_1-01_39";

                        LinkedResource newimg13 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_40.jpg");
                        newimg13.ContentId = "ops-mailer1_1-01_40";

                        LinkedResource newimg14 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_42.jpg");
                        newimg14.ContentId = "ops-mailer1_1-01_42";

                        LinkedResource newimg15 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_43.jpg");
                        newimg15.ContentId = "ops-mailer1_1-01_43";

                        LinkedResource newimg16 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_45.jpg");
                        newimg16.ContentId = "ops-mailer1_1-01_45";

                        LinkedResource newimg18 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_48.jpg");
                        newimg18.ContentId = "ops-mailer1_1-01_48";

                        LinkedResource newimg19 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_49.jpg");
                        newimg19.ContentId = "ops-mailer1_1-01_49";

                        LinkedResource newimg21 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_51.jpg");
                        newimg21.ContentId = "ops-mailer1_1-01_51";

                        LinkedResource newimg25 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_56.jpg");
                        newimg25.ContentId = "ops-mailer1_1-01_56";

                        LinkedResource newimg26 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_57.jpg");
                        newimg26.ContentId = "ops-mailer1_1-01_57";

                        LinkedResource newimg27 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_59.jpg");
                        newimg27.ContentId = "ops-mailer1_1-01_59";

                        LinkedResource newimg28 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_60.jpg");
                        newimg28.ContentId = "ops-mailer1_1-01_60";

                        LinkedResource newimg29 = new LinkedResource(ImagesPathMain + "standalone_health_on\\AMHI-logo.jpg");
                        newimg29.ContentId = "AMHI-logo";

                        LinkedResource newimg30 = new LinkedResource(ImagesPathMain + "standalone_health_on\\Axis-logo.jpg");
                        newimg30.ContentId = "Axis-logo";

                        LinkedResource newimg31 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_162.jpg");
                        newimg31.ContentId = "ops-mailer1_1-01_162";

                        LinkedResource newimg32 = new LinkedResource(ImagesPathMain + "standalone_health_on\\ops-mailer1_1-01_152.jpg");
                        newimg32.ContentId = "ops-mailer1_1-01_152";


                        AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                        av1.LinkedResources.Add(logo);
                        av1.LinkedResources.Add(header);
                        av1.LinkedResources.Add(Step_process);
                        av1.LinkedResources.Add(policy);
                        av1.LinkedResources.Add(newimg1);
                        av1.LinkedResources.Add(newimg2);
                        av1.LinkedResources.Add(newimg3);
                        av1.LinkedResources.Add(newimg4);
                        av1.LinkedResources.Add(newimg5);
                        // av1.LinkedResources.Add(newimg6);

                        av1.LinkedResources.Add(newimg8);
                        av1.LinkedResources.Add(newimg9);
                        av1.LinkedResources.Add(newimg10);
                        av1.LinkedResources.Add(newimg12);
                        av1.LinkedResources.Add(newimg13);
                        av1.LinkedResources.Add(newimg14);
                        av1.LinkedResources.Add(newimg15);
                        av1.LinkedResources.Add(newimg16);
                        av1.LinkedResources.Add(newimg18);
                        av1.LinkedResources.Add(newimg19);
                        av1.LinkedResources.Add(newimg21);
                        av1.LinkedResources.Add(newimg25);
                        av1.LinkedResources.Add(newimg26);
                        av1.LinkedResources.Add(newimg27);
                        av1.LinkedResources.Add(newimg28);
                        av1.LinkedResources.Add(newimg29);
                        av1.LinkedResources.Add(newimg30);
                        av1.LinkedResources.Add(newimg31);
                        av1.LinkedResources.Add(newimg32);

                        message.AlternateViews.Add(av1);
                        # endregion
                    }
                }
                else if (type == "GPA")
                {
                    #region GPA_standalone

                    //LinkedResource logo = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\head.jpg");
                    //logo.ContentId = "head";
                    //LinkedResource header = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_14.jpg");
                    //header.ContentId = "ops-mailer1_1-01_14";
                    //LinkedResource Step_process = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_15.jpg");
                    //Step_process.ContentId = "ops-mailer1_1-01_15";
                    //LinkedResource policy = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_17.jpg");
                    //policy.ContentId = "ops-mailer1_1-01_17";
                    //LinkedResource newimg1 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_18.jpg");
                    //newimg1.ContentId = "ops-mailer1_1-01_18";
                    //LinkedResource newimg2 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_20.jpg");
                    //newimg2.ContentId = "ops-mailer1_1-01_20";
                    //LinkedResource newimg3 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_21.jpg");
                    //newimg3.ContentId = "ops-mailer1_1-01_21";
                    //LinkedResource newimg4 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_46.jpg");
                    //newimg4.ContentId = "ops-mailer1_1-01_46";
                    //LinkedResource newimg5 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_26.jpg");
                    //newimg5.ContentId = "ops-mailer1_1-01_26";
                    //// LinkedResource newimg6 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_27.jpg");

                    //LinkedResource newimg9 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_27.jpg");
                    //newimg9.ContentId = "ops-mailer1_1-01_27";

                    //LinkedResource newimg8 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_31.jpg");
                    //newimg8.ContentId = "ops-mailer1_1-01_31";


                    //LinkedResource newimg10 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_34.jpg");
                    //newimg10.ContentId = "ops-mailer1_1-01_34";

                    //LinkedResource newimg12 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_39.jpg");
                    //newimg12.ContentId = "ops-mailer1_1-01_39";

                    //LinkedResource newimg13 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_40.jpg");
                    //newimg13.ContentId = "ops-mailer1_1-01_40";

                    //LinkedResource newimg14 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_42.jpg");
                    //newimg14.ContentId = "ops-mailer1_1-01_42";

                    //LinkedResource newimg15 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_43.jpg");
                    //newimg15.ContentId = "ops-mailer1_1-01_43";

                    //LinkedResource newimg16 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_45.jpg");
                    //newimg16.ContentId = "ops-mailer1_1-01_45";

                    //LinkedResource newimg18 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_48.jpg");
                    //newimg18.ContentId = "ops-mailer1_1-01_48";

                    //LinkedResource newimg19 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_49.jpg");
                    //newimg19.ContentId = "ops-mailer1_1-01_49";

                    //LinkedResource newimg21 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_51.jpg");
                    //newimg21.ContentId = "ops-mailer1_1-01_51";

                    //LinkedResource newimg25 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_56.jpg");
                    //newimg25.ContentId = "ops-mailer1_1-01_56";

                    //LinkedResource newimg26 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_57.jpg");
                    //newimg26.ContentId = "ops-mailer1_1-01_57";

                    //LinkedResource newimg27 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_59.jpg");
                    //newimg27.ContentId = "ops-mailer1_1-01_59";

                    //LinkedResource newimg28 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_60.jpg");
                    //newimg28.ContentId = "ops-mailer1_1-01_60";

                    //LinkedResource newimg29 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\AMHI-logo.jpg");
                    //newimg29.ContentId = "AMHI-logo";

                    //LinkedResource newimg30 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\Axis-logo.jpg");
                    //newimg30.ContentId = "Axis-logo";

                    //LinkedResource newimg31 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_162.jpg");
                    //newimg31.ContentId = "ops-mailer1_1-01_162";

                    //LinkedResource newimg32 = new LinkedResource("F:\\Health_On_Mailer_PROD\\Health_On_Mailer\\bin\\Debug\\GPA_standalone\\ops-mailer1_1-01_152.jpg");
                    //newimg32.ContentId = "ops-mailer1_1-01_152";


                    //AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    //av1.LinkedResources.Add(logo);
                    //av1.LinkedResources.Add(header);
                    //av1.LinkedResources.Add(Step_process);
                    //av1.LinkedResources.Add(policy);
                    //av1.LinkedResources.Add(newimg1);
                    //av1.LinkedResources.Add(newimg2);
                    //av1.LinkedResources.Add(newimg3);
                    //av1.LinkedResources.Add(newimg4);
                    //av1.LinkedResources.Add(newimg5);
                    //// av1.LinkedResources.Add(newimg6);

                    //av1.LinkedResources.Add(newimg8);
                    //av1.LinkedResources.Add(newimg9);
                    //av1.LinkedResources.Add(newimg10);
                    //av1.LinkedResources.Add(newimg12);
                    //av1.LinkedResources.Add(newimg13);
                    //av1.LinkedResources.Add(newimg14);
                    //av1.LinkedResources.Add(newimg15);
                    //av1.LinkedResources.Add(newimg16);
                    //av1.LinkedResources.Add(newimg18);
                    //av1.LinkedResources.Add(newimg19);
                    //av1.LinkedResources.Add(newimg21);
                    //av1.LinkedResources.Add(newimg25);
                    //av1.LinkedResources.Add(newimg26);
                    //av1.LinkedResources.Add(newimg27);
                    //av1.LinkedResources.Add(newimg28);
                    //av1.LinkedResources.Add(newimg29);
                    //av1.LinkedResources.Add(newimg30);
                    //av1.LinkedResources.Add(newimg31);
                    //av1.LinkedResources.Add(newimg32);

                    //message.AlternateViews.Add(av1);
                    #endregion

                    #region GPA_standalone_New
                    LinkedResource logo = new LinkedResource(ImagesPathMain + "GPA_standloane_new\\head.jpg");
                    logo.ContentId = "head";
                    LinkedResource header = new LinkedResource(ImagesPathMain + "GPA_standloane_new\\ops-mailer1_1-01_152.jpg");
                    header.ContentId = "ops-mailer1_1-01_152";
                    LinkedResource Step_process = new LinkedResource(ImagesPathMain + "GPA_standloane_new\\AMHI-logo.jpg");
                    Step_process.ContentId = "AMHI-logo";
                    LinkedResource policy = new LinkedResource(ImagesPathMain + "GPA_standloane_new\\Axis-logo.jpg");
                    policy.ContentId = "Axis-logo";
                    LinkedResource newimg1 = new LinkedResource(ImagesPathMain + "GPA_standloane_new\\ops-mailer1_1-01_162.jpg");
                    newimg1.ContentId = "ops-mailer1_1-01_162";
                    

                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(logo);
                    av1.LinkedResources.Add(header);
                    av1.LinkedResources.Add(Step_process);
                    av1.LinkedResources.Add(policy);
                    av1.LinkedResources.Add(newimg1);
                   
                    message.AlternateViews.Add(av1);

                    #endregion

                }
                else if (type == "Bundled")
                {
                    #region Bundled with GPA
                    LinkedResource logo = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\head.jpg");
                    logo.ContentId = "head";
                    LinkedResource header = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_14.jpg");
                    header.ContentId = "ops-mailer1_1-01_14";
                    LinkedResource Step_process = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_15.jpg");
                    Step_process.ContentId = "ops-mailer1_1-01_15";
                    LinkedResource policy = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_17.jpg");
                    policy.ContentId = "ops-mailer1_1-01_17";
                    LinkedResource newimg1 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_18.jpg");
                    newimg1.ContentId = "ops-mailer1_1-01_18";
                    LinkedResource newimg2 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_20.jpg");
                    newimg2.ContentId = "ops-mailer1_1-01_20";
                    LinkedResource newimg3 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_21.jpg");
                    newimg3.ContentId = "ops-mailer1_1-01_21";
                    LinkedResource newimg4 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_46.jpg");
                    newimg4.ContentId = "ops-mailer1_1-01_46";
                    LinkedResource newimg5 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_26.jpg");
                    newimg5.ContentId = "ops-mailer1_1-01_26";
                    LinkedResource newimg6 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_27.jpg");
                    newimg6.ContentId = "ops-mailer1_1-01_27";
                    //LinkedResource newimg7 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_17.jpg");
                    //newimg7.ContentId = "ops-mailer1_1-01_17";
                    LinkedResource newimg8 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_31.jpg");
                    newimg8.ContentId = "ops-mailer1_1-01_31";

                    //LinkedResource newimg9 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_20.jpg");
                    // newimg9.ContentId = "ops-mailer1_1-01_20";

                    LinkedResource newimg10 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_34.jpg");
                    newimg10.ContentId = "ops-mailer1_1-01_34";

                    // LinkedResource newimg11 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_46.jpg");
                    // newimg11.ContentId = "ops-mailer1_1-01_46";

                    LinkedResource newimg12 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_39.jpg");
                    newimg12.ContentId = "ops-mailer1_1-01_39";

                    LinkedResource newimg13 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_40.jpg");
                    newimg13.ContentId = "ops-mailer1_1-01_40";

                    LinkedResource newimg14 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_42.jpg");
                    newimg14.ContentId = "ops-mailer1_1-01_42";

                    LinkedResource newimg15 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_43.jpg");
                    newimg15.ContentId = "ops-mailer1_1-01_43";

                    LinkedResource newimg16 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_45.jpg");
                    newimg16.ContentId = "ops-mailer1_1-01_45";

                    //LinkedResource newimg17 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_46.jpg");
                    //newimg17.ContentId = "ops-mailer1_1-01_46";

                    LinkedResource newimg18 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_48.jpg");
                    newimg18.ContentId = "ops-mailer1_1-01_48";

                    LinkedResource newimg19 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_49.jpg");
                    newimg19.ContentId = "ops-mailer1_1-01_49";

                    LinkedResource newimg21 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_51.jpg");
                    newimg21.ContentId = "ops-mailer1_1-01_51";

                    // LinkedResource newimg22 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_43.jpg");
                    // newimg22.ContentId = "ops-mailer1_1-01_43";

                    //  LinkedResource newimg23 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_45.jpg");
                    //  newimg23.ContentId = "ops-mailer1_1-01_45";

                    //  LinkedResource newimg24 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_46.jpg");
                    //  newimg24.ContentId = "ops-mailer1_1-01_46";

                    LinkedResource newimg25 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_56.jpg");
                    newimg25.ContentId = "ops-mailer1_1-01_56";

                    LinkedResource newimg26 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_57.jpg");
                    newimg26.ContentId = "ops-mailer1_1-01_57";

                    LinkedResource newimg27 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_59.jpg");
                    newimg27.ContentId = "ops-mailer1_1-01_59";

                    LinkedResource newimg28 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_60.jpg");
                    newimg28.ContentId = "ops-mailer1_1-01_60";

                    LinkedResource newimg29 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\AMHI-logo.jpg");
                    newimg29.ContentId = "AMHI-logo";

                    LinkedResource newimg30 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\Axis-logo.jpg");
                    newimg30.ContentId = "Axis-logo";

                    LinkedResource newimg31 = new LinkedResource(ImagesPathMain + "bundled_with_GPA\\ops-mailer1_1-01_162.jpg");
                    newimg31.ContentId = "ops-mailer1_1-01_162";


                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(logo);
                    av1.LinkedResources.Add(header);
                    av1.LinkedResources.Add(Step_process);
                    av1.LinkedResources.Add(policy);
                    av1.LinkedResources.Add(newimg1);
                    av1.LinkedResources.Add(newimg2);
                    av1.LinkedResources.Add(newimg3);
                    av1.LinkedResources.Add(newimg4);
                    av1.LinkedResources.Add(newimg5);
                    av1.LinkedResources.Add(newimg6);
                    //av1.LinkedResources.Add(newimg7);
                    av1.LinkedResources.Add(newimg8);
                    // av1.LinkedResources.Add(newimg9);
                    av1.LinkedResources.Add(newimg10);
                    // av1.LinkedResources.Add(newimg11);
                    av1.LinkedResources.Add(newimg12);
                    av1.LinkedResources.Add(newimg13);
                    av1.LinkedResources.Add(newimg14);
                    av1.LinkedResources.Add(newimg15);
                    av1.LinkedResources.Add(newimg16);
                    // av1.LinkedResources.Add(newimg17);
                    av1.LinkedResources.Add(newimg18);
                    av1.LinkedResources.Add(newimg19);
                    //av1.LinkedResources.Add(newimg20);
                    av1.LinkedResources.Add(newimg21);
                    //av1.LinkedResources.Add(newimg22);
                    //av1.LinkedResources.Add(newimg23);
                    //av1.LinkedResources.Add(newimg24);
                    av1.LinkedResources.Add(newimg25);
                    av1.LinkedResources.Add(newimg26);
                    av1.LinkedResources.Add(newimg27);
                    av1.LinkedResources.Add(newimg28);
                    av1.LinkedResources.Add(newimg29);
                    av1.LinkedResources.Add(newimg30);
                    av1.LinkedResources.Add(newimg31);


                    message.AlternateViews.Add(av1);

                    #endregion
                }
                else if (type == "Optima_Enhance")
                {
                    #region Optima_Enhance

                    LinkedResource logo = new LinkedResource(ImagesPathMain + "Optima_Enhance\\head.jpg");
                    logo.ContentId = "head";

                    LinkedResource newimg29 = new LinkedResource(ImagesPathMain + "Optima_Enhance\\AMHI-logo.jpg");
                    newimg29.ContentId = "AMHI-logo";

                    LinkedResource newimg30 = new LinkedResource(ImagesPathMain + "Optima_Enhance\\Axis-logo.jpg");
                    newimg30.ContentId = "Axis-logo";

                    LinkedResource newimg31 = new LinkedResource(ImagesPathMain + "Optima_Enhance\\ops-mailer1_1-01_162.jpg");
                    newimg31.ContentId = "ops-mailer1_1-01_162";                   

                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);
                    av1.LinkedResources.Add(logo);
                    av1.LinkedResources.Add(newimg29);
                    av1.LinkedResources.Add(newimg30);
                    av1.LinkedResources.Add(newimg31);
                    message.AlternateViews.Add(av1);

                    #endregion
                }


                DataTable riderdt = new DataTable();
                string Axis_Gold = Convert.ToString(ConfigurationSettings.AppSettings["Axis_Gold"].Trim());
                string Axis_Platinum = Convert.ToString(ConfigurationSettings.AppSettings["Axis_Platinum"].Trim());
                //  string Policy_Wording = Convert.ToString(ConfigurationSettings.AppSettings["Policy_Wording"].Trim());
                //  string Health_On_Claim_Procedure = Convert.ToString(ConfigurationSettings.AppSettings["Health_On_Claim_Procedure"].Trim());
                //  string CA_rider = Convert.ToString(ConfigurationSettings.AppSettings["CA_rider"].Trim());

                //riderdt = Policy_schedule_Data("select strprodcd from com_pol_prod_dtl@w2e where strpolnbr='" + Policy_Number + "' and nprodtype=2");
                //if (riderdt.Rows.Count > 0)
                //{
                //    for (int i = 0; i < riderdt.Rows.Count; i++)
                //    {
                //        string ridercode = riderdt.Rows[i][0].ToString();
                //        if (ridercode == "11197" || ridercode == "11196")
                //        {
                //            //CA rider
                //            Attachment attachment = new Attachment(CA_rider);
                //            attachment.Name = CA_rider.Substring(CA_rider.LastIndexOf('\\') + 1);
                //            message.Attachments.Add(attachment);
                //        }                        
                //    }
                //}

                //Attachment attachment = new Attachment(Health_On_PW);
                //attachment.Name = Health_On_PW.Substring(Health_On_PW.LastIndexOf('\\') + 1);
                //message.Attachments.Add(attachment);

                //Attachment attachment2 = new Attachment(Policy_Wording);
                //attachment2.Name = Policy_Wording.Substring(Policy_Wording.LastIndexOf('\\') + 1);
                //message.Attachments.Add(attachment2);
                #region Health_on
                if (type == "Health_on")
                {
                    if (!string.IsNullOrEmpty(FilePath))
                    {
                        Attachment attachment3 = new Attachment(FilePath);
                        attachment3.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment3);
                        logger.Info("Health_on policy Schedule: " + FilePath);
                    }
                    if (!string.IsNullOrEmpty(proposal_form))
                    {
                        Attachment attachment4 = new Attachment(proposal_form);
                        attachment4.Name = proposal_form.Substring(proposal_form.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment4);
                        logger.Info("Health_on proposal_form: " + proposal_form);
                    }
                    //if (SIFlag == 0)
                    //{
                    //    // Axis Gold
                    //    Attachment attachment1 = new Attachment(Axis_Gold);
                    //    attachment1.Name = Axis_Gold.Substring(Axis_Gold.LastIndexOf('\\') + 1);
                    //    message.Attachments.Add(attachment1);
                    //    logger.Info("Health_on Axis Gold: " + Axis_Gold);
                    //}
                    //else
                    //{
                    //    // Axis Platinum
                    //    Attachment attachment1 = new Attachment(Axis_Platinum);
                    //    attachment1.Name = Axis_Platinum.Substring(Axis_Platinum.LastIndexOf('\\') + 1);
                    //    message.Attachments.Add(attachment1);
                    //    logger.Info("Health_on Axis Platinum: " + Axis_Platinum);
                    //}
                }
                #endregion

                #region GPA
                else if (type == "GPA")
                {
                    if (!string.IsNullOrEmpty(FilePath))
                    {
                        Attachment attachment3 = new Attachment(FilePath);
                        attachment3.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment3);
                        logger.Info("GPA COI: " + FilePath);
                    }
                }

                #endregion

                #region Bundled
                else if (type == "Bundled")
                {
                    if (!string.IsNullOrEmpty(FilePath))
                    {
                        Attachment attachment3 = new Attachment(FilePath);
                        attachment3.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment3);
                        logger.Info("Bundled Health On policy Schedule: " + FilePath);
                    }
                    if (!string.IsNullOrEmpty(proposal_form))
                    {
                        Attachment attachment4 = new Attachment(proposal_form);
                        attachment4.Name = proposal_form.Substring(proposal_form.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment4);
                        logger.Info("Bundled Health On proposal form: " + proposal_form);
                    }
                    //if (SIFlag == 0)
                    //{
                    //    // Axis Gold
                    //    Attachment attachment1 = new Attachment(Axis_Gold);
                    //    attachment1.Name = Axis_Gold.Substring(Axis_Gold.LastIndexOf('\\') + 1);
                    //    message.Attachments.Add(attachment1);
                    //    logger.Info("Bundled Health On Axis gold: " + Axis_Gold);
                    //}
                    //else
                    //{
                    //    // Axis Platinum
                    //    Attachment attachment1 = new Attachment(Axis_Platinum);
                    //    attachment1.Name = Axis_Platinum.Substring(Axis_Platinum.LastIndexOf('\\') + 1);
                    //    message.Attachments.Add(attachment1);
                    //    logger.Info("Bundled  Health On Axis platinnum: " + Axis_Platinum);
                    //}
                    //COI GPA
                    if (!string.IsNullOrEmpty(Gpapath))
                    {
                        Attachment attachment4 = new Attachment(Gpapath);
                        attachment4.Name = Gpapath.Substring(Gpapath.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachment4);
                        logger.Info("Bundled GPA COI: " + Gpapath);
                    }

                }
                #endregion

                #region Optima_Enhance
                else if (type == "Optima_Enhance")
                {
                    if (!string.IsNullOrEmpty(FilePath))
                    {
                        Attachment attachmentCOI = new Attachment(FilePath);
                        attachmentCOI.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachmentCOI);
                        logger.Info("Optima Enhance COI: " + FilePath);
                    }
                    if (!string.IsNullOrEmpty(proposal_form))
                    {
                        Attachment attachmentPF = new Attachment(proposal_form);
                        attachmentPF.Name = proposal_form.Substring(proposal_form.LastIndexOf('\\') + 1);
                        message.Attachments.Add(attachmentPF);
                        logger.Info("Optima Enhance Proposal Form: " + proposal_form);
                    }
                }

                #endregion

                SmtpClient client = new SmtpClient(); // Live                
                client.Send(message);
                message.Dispose();
                logger.Info("Mail Sent : " + mailTo);

                // Insert_Log(" INSERT INTO tbl_digital_signature_mail_hst (POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,status,SENT_DATE,CREATED_DATE,DMS_IMAGE_ID,ELIXIR_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No) ( select POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,null,1 status,sysdate,CREATED_DATE,DMS_IMAGE_ID,ELIXIR_IMAGE_ID,SUM_INSURED,Product_Code,Prospect_No from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "') ;");
                // Delete_Log(" Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "' ;");
                Update_Log("update tbl_digital_signature_mailer set status =1 , sent_date = sysdate  where pol_number ='" + Policy_Number + "' ;");
                Console.WriteLine(+serialno + ". " + "E-mail Send for the : " + Policy_Number + " E-Mail ID - " + mailTo);
                serialno++;
            }
            catch (Exception ex)
            {
                logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Error in mail sending' where  email_id ='" + mailTo + "' and pol_number ='" + Policy_Number + "' ;");
            }
            finally
            {

            }
        }

        #endregion

        #region < Update_Log >
        private static void Update_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Insert_Log >
        private static void Insert_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Delete_Log >
        private static void Delete_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

    }
}
