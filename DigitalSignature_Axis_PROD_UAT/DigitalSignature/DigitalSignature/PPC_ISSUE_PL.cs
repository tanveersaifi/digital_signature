﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Configuration;
using System.Data.OracleClient;
using Ionic.Zip;
using Ionic.Zlib;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using System.Data.Common;
using System.Data.OleDb;
using Microsoft.Practices.EnterpriseLibrary.Data;

//anand
namespace DigitalSignature
{
    class PPC_ISSUE_PL : System.Web.UI.Page
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("PPC_ISSUE_PL");
        static Database dbClient = DatabaseFactory.CreateDatabase("OracleApolloT");
        static DbCommand dbCommand = null;

        #region get ppc issue policy data
        public void get_ppc_issue()
        {

            try
            {
               
                string fileArray;
                logger.Info("============= Job Start =============");
                DataTable dt = new DataTable();

                //Without Health Policy Table join
               // dt = PDF_Data("Select APPLICATION_NUMBER,  imageid,  \"EMAIL\",  main_member_name,  password,ProductName from TBL_PPC_LETTER_MAILER where letter_type ='PPC' and SEND_STATUS =0 and \"EMAIL\" is not null and APPLICATION_NUMBER is not null and main_member_name is not null and Trunc(UPLOAD_DATE)  =Trunc(sysdate)");

                //With Health Policy Table join
                dt = PDF_Data("Select MLR.APPLICATION_NUMBER,  MLR.imageid,  \"EMAIL\",   MLR.main_member_name,  MLR.password,MLR.ProductName from TBL_PPC_LETTER_MAILER MLR INNER JOIN healthpolicy_new HPN ON TO_CHAR(hpn.application_no)=TO_CHAR(mlr.application_number) where MLR.letter_type ='PPC' and MLR.SEND_STATUS =0 and \"EMAIL\" is not null and MLR.APPLICATION_NUMBER is not null and MLR.main_member_name is not null and Trunc(MLR.UPLOAD_DATE)  =Trunc(sysdate)");
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int x = 0; x < dt.Rows.Count; x++)
                        {
                            logger.Info("Policy Number : " + dt.Rows[x][1].ToString());
                            GetDMSReports(dt.Rows[x][1].ToString(), dt.Rows[x][0].ToString(), dt.Rows[x][2].ToString(), dt.Rows[x][3].ToString(), dt.Rows[x][4].ToString(), dt.Rows[x][5].ToString());
                        }
                    }
                    else
                    {
                        logger.Info("Policy Number not found : ");
                    }
                }
                else
                {
                    logger.Info("Policy Number not found : ");
                }
                logger.Info("============= Mis Report Sent to User =============");

                // PPC_Sending_Log();
                logger.Info("============= Job End =============");

            }
            catch (Exception ex)
            {
                logger.Error("PPC_ISSUE_Data  : " + ex.Message);
                throw;
            }

        }
        #endregion end

        #region Get DMS Image id
        public string GetDMSReports(string strImageId, string policy_number, string E_mailID, string main_member, string password1, string prod)
        {

            string strFileName = string.Empty;
            try
            {
                //for UAT
                //String moduleName = "core";
                //String contextRoot = "http://wcorupdms06.apollomunichinsurance.com:9081/services";
                //String repository = "ADKVUAT";
                //String user = "webuser";
                //String password = "amhi@123";

                String moduleName = "core";
                String contextRoot = "http://wcorppdms01.apollomunichinsurance.com:9081/services";
                String repository = "ADKVPROD";
                String user = "webuser";
                String password = "amhi@123";

                ContextFactory contextFactory = ContextFactory.Instance;

                IServiceContext context = contextFactory.NewContext();

                RepositoryIdentity repoId = new RepositoryIdentity();

                repoId.RepositoryName = repository;
                repoId.UserName = user;
                repoId.Password = (password);
                context.AddIdentity(repoId);

                IServiceContext registeredContext = contextFactory.Register(context, moduleName, contextRoot);

                ServiceFactory serviceFactory = ServiceFactory.Instance;
                IObjectService service = serviceFactory.GetRemoteService<IObjectService>(registeredContext, moduleName, contextRoot);

                ContentTransferProfile transferProfile = new ContentTransferProfile();
                transferProfile.TransferMode = ContentTransferMode.MTOM;
                transferProfile.Geolocation = "";
                registeredContext.SetProfile(transferProfile);

                ObjectIdentity objectIdentity = new ObjectIdentity(repository);
                ObjectId objId = new ObjectId(strImageId);
                objectIdentity.Value = objId;
                byte[] byteArray;
                ContentProfile contentProfile = new ContentProfile();
                contentProfile.FormatFilter = FormatFilter.ANY;


                OperationOptions operationOptions = new OperationOptions();
                operationOptions.ContentProfile = contentProfile;

                operationOptions.SetProfile(contentProfile);
                ObjectIdentitySet objectIdSet = new ObjectIdentitySet();

                List<ObjectIdentity> objIdList = objectIdSet.Identities;
                objIdList.Add(objectIdentity);
                DataPackage dataPackage = service.Get(objectIdSet, operationOptions);
                DataObject dataObject = dataPackage.DataObjects[0];
                Emc.Documentum.FS.DataModel.Core.Content.Content resultContent = dataObject.Contents[0];
                if (resultContent.CanGetAsFile())
                {
                    strFileName = strImageId + "." + Convert.ToString(resultContent.Format);
                    byteArray = resultContent.GetAsByteArray();
                    BinaryWriter bw;
                    string ReportDirectroy = ConfigurationSettings.AppSettings["ReportPath"].ToString();
                    string date = DateTime.Now.ToString("dd-MM-yyyy");
                    DataTable dt1 = new DataTable();
                    string inputpath = ReportDirectroy + "\\" + date + @"\\" + policy_number + @"\";
                    string zippath = ReportDirectroy + "\\" + date + @"\\";

                    if (!Directory.Exists(inputpath))
                        Directory.CreateDirectory(inputpath);

                    bw = new BinaryWriter(File.OpenWrite(inputpath + strFileName));
                    bw.Write(byteArray);

                    bw.Flush();
                    bw.Close();
                    logger.Info("PPC Issue plolicy details Generated " + policy_number);

                    GenerateZip(zippath, policy_number, password1);
                    logger.Info("Zip File Generated " + policy_number);
                    sendMail(E_mailID, zippath + policy_number + ".zip", prod + " Policy No." + policy_number, policy_number, main_member);

                    Update_Log("update TBL_PPC_LETTER_MAILER set SEND_STATUS =1 , send_date = sysdate  where  application_number  ='" + policy_number + "'");
                    Console.WriteLine("E-mail Send for the : " + policy_number + " E-Mail ID - " + E_mailID);

                }

            }
            catch (Exception ex)
            {
                logger.Error("PPC_ISSUE_Data  : " + ex.Message);
                Update_Log("update TBL_PPC_LETTER_MAILER set status =0 , send_date = sysdate , error ='Image generation failed' where  application_number ='" + policy_number + "';");
                throw;
            }
            return strImageId;

        }
        #endregion end image id

        #region Zip generatiion code
        public static string GenerateZip(string _Directory, string _SubDirectory, string _Password)
        {

            string zipfile = _Directory + "\\" + _SubDirectory + ".zip";
            if (File.Exists(zipfile))
                File.Delete(zipfile);
            ZipFile createZip = new ZipFile();
            if (!string.IsNullOrEmpty(_Password))
                createZip.Password = _Password;
            createZip.CompressionLevel = CompressionLevel.Level9;
            createZip.AddDirectory(_Directory + "\\" + _SubDirectory);
            createZip.Encryption = EncryptionAlgorithm.PkzipWeak;
            createZip.Save(zipfile);
            Directory.Delete(_Directory + "\\" + _SubDirectory, true);
            return zipfile;
        }
        #endregion end Zip

        #region PPC Mail Code
        public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number, string main_member)
        {
            try
            {

                String strPath = @"D:\My Docs\PDF_GEN\PPC_ISSUE_Policy.html";
                string strBody = File.ReadAllText(strPath);

                strBody = strBody.Replace("<Customer>", main_member);
                strBody = strBody.Replace("<Pol>", Policy_Number);

                MailMessage message = new MailMessage();
                string msg_from = "relay<relay@apollomunichinsurance.com>";
                message.From = new MailAddress(msg_from);

                //string MessageTo = mailTo;
                    //string MessageTo = "mukesh.tiwari@apollomunichinsurance.com";               \
                string MessageTo = "vishal.saxena1@apollomunichinsurance.com";               
                message.To.Add(new MailAddress(MessageTo));     
           
                //message.CC.Add(new MailAddress("gaurav.kumar1@apollomunichinsurance.com")); 

                message.Subject = subject;
                message.Body = strBody;
                Attachment attachment = new System.Net.Mail.Attachment(FilePath);
                attachment.Name = FilePath.Substring(FilePath.LastIndexOf('/') + 1);
                message.Attachments.Add(attachment);
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient(); // Live
                client.Send(message);
                logger.Info("Mail Sent  " + mailTo);
                if (!Regex.IsMatch(mailTo, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                {
                    Update_Log("update TBL_PPC_LETTER_MAILER set send_status =0 , send_date = sysdate , error ='Wrong EmailID' where   APPLICATION_NUMBER ='" + Policy_Number + "' ");
                }
                else
                {
                    Update_Log("update TBL_PPC_LETTER_MAILER set SEND_STATUS =1 , send_date = sysdate  where  application_number  ='" + Policy_Number + "'");
                }

            }
            catch (Exception ex)
            {

                logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
                if (mailTo.Equals(string.Empty))
                {
                    Update_Log("update TBL_PPC_LETTER_MAILER set send_status =0 , send_date = sysdate , error ='EmailId not exist' where   APPLICATION_NUMBER ='" + Policy_Number + "' ");

                }
                else if (!Regex.IsMatch(mailTo, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                {
                    Update_Log("update TBL_PPC_LETTER_MAILER set send_status =0 , send_date = sysdate , error ='Wrong EmailID' where   APPLICATION_NUMBER ='" + Policy_Number + "' ");
                }
                else
                {
                    Update_Log("update TBL_PPC_LETTER_MAILER set send_status =0 , send_date = sysdate , error ='Mail sending failed' where   APPLICATION_NUMBER ='" + Policy_Number + "' ");
                }

                long length = new System.IO.FileInfo(FilePath).Length;
                string filesize = GetFileSize(length);

                logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message + " File Size" + filesize);
                Update_Log("update TBL_PPC_LETTER_MAILER set SEND_STATUS =0 , SEND_DATE = sysdate , ERROR = '" + ex.Message.ToString() + " File Size " + filesize + "' where APPLICATION_NUMBER ='" + Policy_Number + "';");
                throw;
            }
            finally
            {

            }
        }
        #endregion end mail code

        //#region Unsucesscase data
        //private static DataTable report_unsucesscase()
        //{
        //    DataTable dt1 = new DataTable();
        //    StringBuilder sb1 = new StringBuilder("select application_number,to_char(send_date,'dd-mon-yyyy')as Sdate,Error from TBL_PPC_LETTER_MAILER where send_status in(0) and error is not null and letter_type ='PPC'  and  Trunc(send_date)  ='08-jun-15'");
        //    return dt1 = Policy_schedule_Data(sb1.ToString());
        //}
        //#endregion end

        //#region report data
        //private static DataTable Report_query(string todate, string frodate)
        //{
        //    DataTable dt = new DataTable();
        //    StringBuilder sb = new StringBuilder("");
        //    sb.AppendFormat("		select S_mo , Case , Cancellation_Letter,Add_Info_Letter,PPC_Letter from (	");
        //    sb.AppendFormat("		select to_number('1') S_mo,  'Upload Data' Case,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Cancellation Letter') ) Cancellation_Letter ,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Add Info Letter') ) Add_Info_Letter,	");
        //    sb.AppendFormat("       (select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('PPC') ) PPC_Letter	");
        //    sb.AppendFormat("		from dual	");
        //    sb.AppendFormat("		union 	");
        //    sb.AppendFormat("		select to_number('2') S_mo,'E-mail ID Found' Case,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Cancellation Letter') and EMAIL is not null ) Cancellation_Letter ,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Add Info Letter') and EMAIL is not null ) Add_Info_Letter,	");
        //    sb.AppendFormat("       (select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('PPC') and EMAIL is not null ) PPC_Letter");
        //    sb.AppendFormat("		from dual	");
        //    sb.AppendFormat("		union 	");
        //    sb.AppendFormat("		select to_number('3') S_mo, 'E-mail ID not Found' Case,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Cancellation Letter') and EMAIL is null ) Cancellation_Letter ,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Add Info Letter') and EMAIL is null ) Add_Info_Letter,	");
        //    sb.AppendFormat("       (select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('PPC') and EMAIL is null ) PPC_Letter");
        //    sb.AppendFormat("		from dual	");
        //    sb.AppendFormat("		union 	");
        //    sb.AppendFormat("		select to_number('4') S_mo, 'E-Mail Sent' Case,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Cancellation Letter') and SEND_STATUS =1 and ERROR is null ) Cancellation_Letter ,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Add Info Letter') and SEND_STATUS =1 and ERROR is null) Add_Info_Letter,	");
        //    sb.AppendFormat("       (select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('PPC') and SEND_STATUS =1 and ERROR is null) PPC_Letter");
        //    sb.AppendFormat("		from dual	");
        //    sb.AppendFormat("		union 	");
        //    sb.AppendFormat("		select to_number('5') S_mo, 'Mail Sending Failed' Case,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Cancellation Letter')  and ERROR is not null ) Cancellation_Letter ,	");
        //    sb.AppendFormat("		(select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('Add Info Letter')  and ERROR is not null) Add_Info_Letter,	");
        //    sb.AppendFormat("       (select count(1) from TBL_PPC_LETTER_MAILER where Trunc(UPLOAD_DATE) between '" + todate + "' and '" + frodate + "' and Upper(LETTER_TYPE)  =Upper('PPC')  and ERROR is not null) PPC_Letter");
        //    sb.AppendFormat("		from dual )  order by S_mo ASC	");
        //    return dt = Policy_schedule_Data(sb.ToString());

        //  }

        //#endregion end report data

        #region get datatable data

        private static DataTable Policy_schedule_Data(string strquery)
        {
            DataTable dtReport = new DataTable();
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = strquery;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = Cmd;
                da.Fill(dtReport);
                return dtReport;
            }
            catch (Exception ex)
            {
                logger.Error("PPC_Data  : " + ex.Message + "SQL Query " + strquery);
                //throw;
                return dtReport;
            }

        }

        #endregion end datatable data

        //#region MIS Generation data
        //public void PPC_Sending_Log()
        //{
        //    try
        //    {
        //        String strPath = @"D:\AnandWork\code\PDF_GEN\PPC_MIS.html";
        //        string strBody = File.ReadAllText(strPath);

        //        StringBuilder sb = new StringBuilder("");
        //        StringBuilder sb1 = new StringBuilder("");
        //        StringBuilder sb2 = new StringBuilder("");
        //        string From_Date = String.Format("{0:dd-MMM-yyyy}", DateTime.Now.AddDays(-0));
        //        string month = "1-" + String.Format("{0:MMM-yyyy}", DateTime.Now.AddDays(-0));

        //        DataTable fd = Report_query(From_Date, From_Date);
        //        DataTable fy = Report_query("1-Apr-2014", From_Date);
        //        DataTable pd = report_unsucesscase();

        //       for (int j = 0; j < fd.Rows.Count; j++)
        //        {

        //            sb.AppendFormat("	<tr>");
        //            sb.AppendFormat("	<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fd.Rows[j][1]) + "</td>");
        //            sb.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fd.Rows[j][2]) + "</td>");
        //            sb.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fd.Rows[j][3]) + "</td>");
        //            sb.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fd.Rows[j][4]) + "</td>");
        //            sb.AppendFormat("	</tr>");
        //        }
        //        strBody = strBody.Replace("<ForthedaySummary>", sb.ToString());

        //        for (int k = 0; k < fy.Rows.Count; k++)
        //        {

        //            sb1.AppendFormat("	<tr>");
        //            sb1.AppendFormat("	<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fy.Rows[k][1]) + "</td>");
        //            sb1.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fy.Rows[k][2]) + "</td>");
        //            sb1.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fy.Rows[k][3]) + "</td>");
        //            sb1.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(fy.Rows[k][4]) + "</td>");
        //            sb1.AppendFormat("	</tr>");
        //        }
        //        strBody = strBody.Replace("<FortheYearSummary>", sb1.ToString());
        //        strBody = strBody.Replace("<date>", From_Date);

        //        for (int p = 0; p < pd.Rows.Count; p++)
        //        {

        //            sb2.AppendFormat("	<tr>");
        //            sb2.AppendFormat("	<td align='left' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(pd.Rows[p][0]) + "</td>");
        //            sb2.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(pd.Rows[p][1]) + "</td>"); //ltrlNotlar.Text = dt.ToString("dd.MM.yyyy");
        //            sb2.AppendFormat("	<td align='center' valign='top' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000; padding-bottom: 8px; padding-top: 5px; padding-left: 5px;'>" + Convert.ToString(pd.Rows[p][2]) + "</td>");
        //            sb2.AppendFormat("	</tr>");
        //        }
        //        strBody = strBody.Replace("<unsuccessfulSummary>", sb2.ToString());
        //        strBody = strBody.Replace("<misdate>", From_Date);



        //        MailMessage message = new MailMessage();
        //        string msg_from = "relay<relay@apollomunichinsurance.com>";
        //        message.From = new MailAddress(msg_from);
        //        string MessageTo = "anand.gupta@apollomunichinsurance.com";
        //        //string MessageTo = "mukesh.tiwari@apollomunichinsurance.com";


        //        //message.Bcc.Add(new MailAddress(MessageTo));
        //       message.To.Add(new MailAddress("anand.gupta@apollomunichinsurance.com"));
        //        // message.To.Add(new MailAddress("mousumi.dasgupta@apollomunichinsurance.com"));
        //        // message.To.Add(new MailAddress("amit2.kumar@apollomunichinsurance.com"));
        //        // message.To.Add(new MailAddress("deepak.pandey@apollomunichinsurance.com"));
        //        // message.Bcc.Add(new MailAddress("ranjay.kumar@apollomunichinsurance.com"));
        //        // message.Bcc.Add(new MailAddress("vishal.saksena@apollomunichinsurance.com"));
        //      // if ((!string.IsNullOrEmpty(Statusattach)) && File.Exists(Statusattach))
        //      //     message.Attachments.Add(new Attachment(Statusattach));
        //        message.Subject = "PPC Mailer sending Report " + From_Date;
        //        message.Body = strBody;
        //        message.IsBodyHtml = true;
        //        SmtpClient client = new SmtpClient(); // Live
        //        client.Send(message);


        //    }
        //    catch (Exception ex)
        //    {

        //       // logger.Error("Error in mail sending for the  : " + mailto + " Error Log - " + ex.Message);
        //        //Update_Log("update policy_schedule_mailer set status =0 , send_date = sysdate , error ='" + ex.Message + "' where  \"Email_Address\" ='" + mailTo + "' and \"Policy_Number\" ='" + Policy_Number + "' ;");

        //        throw;

        //    }
        //    finally
        //    {

        //    }
        //}
        //#endregion Mis generation end

        #region update data code
        private static void Update_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                // sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                logger.Error("Update status  : " + ex.Message + "SQL Query " + sQuery);
                throw;
            }

        }
        #endregion end update

        #region check file size
        public static string GetFileSize(double byteCount)
        {
            string size = "0 Bytes";
            if (byteCount >= 1073741824.0)
                size = String.Format("{0:##.##}", byteCount / 1073741824.0) + " GB";
            else if (byteCount >= 1048576.0)
                size = String.Format("{0:##.##}", byteCount / 1048576.0) + " MB";
            else if (byteCount >= 1024.0)
                size = String.Format("{0:##.##}", byteCount / 1024.0) + " KB";
            else if (byteCount > 0 && byteCount < 1024.0)
                size = byteCount.ToString() + " Bytes";

            return size;
        }
        #endregion end

        #region get image data
        public static DataTable PDF_Data(string strquery)
        {
            DataTable dtReport = new DataTable();
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = strquery;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = Cmd;
                da.Fill(dtReport);
                return dtReport;
            }
            catch (Exception ex)
            {
                logger.Error("PPC_ISSUE_POL  : " + ex.Message + "SQL Query " + strquery);
                //throw;
                return dtReport;
            }
        }
        #endregion end image data
    }
}
