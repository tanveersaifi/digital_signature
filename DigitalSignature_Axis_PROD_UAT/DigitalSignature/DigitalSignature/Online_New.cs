﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.Net;
using Ionic.Zip;
using Ionic.Zlib;
using System.Net.Mail;
using Emc.Documentum.FS.DataModel.Core.Context;
using Emc.Documentum.FS.Runtime.Context;
using Emc.Documentum.FS.Services.Core;
using Emc.Documentum.FS.DataModel.Core;
using Emc.Documentum.FS.DataModel.Core.Profiles;
using Emc.Documentum.FS.DataModel.Core.Content;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using dbAutoTrack.PDFWriter;
using dbAutoTrack.PDFWriter.Graphics;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Net.Mime;
//using System.Net;

namespace DigitalSignature
{
    class Online_New
    {
        public static log4net.ILog logger = log4net.LogManager.GetLogger("frmMain");
        public static string policy_Number;
        public static string Email_id;
        public static string Main_member;
        public static int serialno = 1;
        public static string DMS_Image_ID;
        public static string fileArray = string.Empty;
        public static string Mobile_number = string.Empty;
        public static string agentcode = string.Empty;
        public static string PRODUCT_CODE = string.Empty;
        public static string PROSPECT_NO = string.Empty;
        WhtsAppPolicy objWhtsAppPolicy = null;

        #region < On_line_Mailer >
        public void On_line_Mailer()
        {
            try
            {
                // sendMail("", "", "Policy Bazzar Mailer copy", "AA00123456", "", "80157608");

                //Job Start Here
                logger.Info("=============BuyOnline Job Start =============");

                DataTable dt = new DataTable();
                //get List Of Record From table 

                //dt = Policy_schedule_Data("select pol_number, email_id,main_member_name,dms_image_id,MobileNo,agentcode,Product_Code from TBL_Digital_Signature_mailer where status=0 and email_id is not null  and username  in (select distinct username from tbl_usertable where bstatus=1) and Product_Code not in ('11173','11174','11175','11176','11200','11201','11202','11203','33003','34001','34002','34003','34004','34005','11228','11229','11230','11231','22300','22301','22302','22303','12765','12766','11300','11301','11410','11411','11412','11413','11414','11415','11416','11417') and dms_image_id is not null and Trunc(created_date) = trunc(sysdate) ");
                //dt = Policy_schedule_Data("select pol_number, email_id,main_member_name,dms_image_id,MobileNo,agentcode,Product_Code,PROSPECT_NO,USERNAME from TBL_Digital_Signature_mailer where status=0 and email_id is not null  and Product_Code not in (Select PRODUCT_CODE from tbl_mailer_productlist where STATUS =1 and PRODUCT_SHORT_CODE not in ('HWT','HDFCOFF')) and dms_image_id is not null and Trunc(created_date) between trunc(sysdate-2) and trunc(sysdate) ");
                dt = Policy_schedule_Data("select pol_number, email_id,main_member_name,dms_image_id,MobileNo,agentcode,Product_Code,PROSPECT_NO,USERNAME from TBL_Digital_Signature_mailer where status=0 and Product_Code not in (Select PRODUCT_CODE from tbl_mailer_productlist where STATUS =1 and PRODUCT_SHORT_CODE not in ('HWT','HDFCOFF')) and dms_image_id is not null and Trunc(created_date) between trunc(sysdate-2) and trunc(sysdate) ");

                //dt = Policy_schedule_Data("select pol_number, email_id,main_member_name,dms_image_id,SUM_INSURED from tbl_digital_signature_mailer where status=0 and POL_NUMBER in ('AA00163786')");
                int Noofrecords = dt.Rows.Count;

                Console.WriteLine("Total Numbers of Records : " + Noofrecords);
                logger.Info("Total Policy Number : " + dt.Rows.Count.ToString());

                string WhatsAppMainMember = string.Empty;
                string WhatsAppEmail = string.Empty;
                string User_Name = string.Empty;
                int ImproperRecordNo = 0;

                int skip = 0;
                int Take = 100;
                int i = 0;
                int j = 0;
                for (i = j; i < Take; i++)
                {
                    dt.Select().Skip(skip).Take(Take);
                    var a = dt.Select().Skip(skip).Take(Take);// Select Data Datatable From 0 record Take 100 Record

                    DataTable dt1 = new DataTable();
                    dt1 = a.CopyToDataTable();
                    skip = skip + 100;
                    if (dt1.Rows.Count != 0) // If datatable Has More than 0 record
                    {
                        for (int x = 0; x < dt1.Rows.Count; x++)   // Loop Upto last record from Data Table
                        {
                            policy_Number = dt1.Rows[x]["pol_number"].ToString();   // Get Policy number
                            Email_id = dt1.Rows[x]["email_id"].ToString(); //get Email -ID
                            Main_member = dt1.Rows[x]["main_member_name"].ToString();
                            DMS_Image_ID = dt1.Rows[x]["dms_image_id"].ToString().Trim();
                            Mobile_number = dt1.Rows[x]["MobileNo"].ToString().Trim();
                            agentcode = dt1.Rows[x]["agentcode"].ToString().Trim();
                            PRODUCT_CODE = dt1.Rows[x]["Product_Code"].ToString().Trim();
                            PROSPECT_NO = dt1.Rows[x]["PROSPECT_NO"].ToString().Trim();
                            User_Name = dt1.Rows[x]["USERNAME"].ToString().Trim();

                            if (string.IsNullOrEmpty(Main_member))
                            {
                                WhatsAppMainMember = "Customer";
                            }
                            else
                            {
                                WhatsAppMainMember = Main_member;
                            }

                            if (string.IsNullOrEmpty(Email_id))
                            {
                                WhatsAppEmail = "Email not available";
                            }
                            else
                            {
                                WhatsAppEmail = Email_id;
                            }

                            logger.Info("Policy Number : " + dt1.Rows[x][0].ToString());
                            string Location = Convert.ToString(ConfigurationSettings.AppSettings["FileArray"].Trim());
                            fileArray = Location + DMS_Image_ID + "";
                            string[] tomail = Email_id.Split('@');
                            string domain = tomail[1].ToString();
                            //if (domain == "apollomunichinsurance.com")
                            // {
                            bool ISAXW = policy_Number.Contains("AXW");
                            if (ISAXW == false)
                            {
                                if (!string.IsNullOrEmpty(Email_id))
                                {
                                    GetWebsiteHtml(fileArray, policy_Number + ".pdf", PRODUCT_CODE, PROSPECT_NO);
                                }
                                #region WhatsApp Integration
                                //-------- WhatsApp only selected UserIDs ---------- 
                                objWhtsAppPolicy = new WhtsAppPolicy();

                                if (!string.IsNullOrEmpty(User_Name))
                                {
                                    if (objWhtsAppPolicy.CheckForAllowedUserName(User_Name))
                                    {
                                        ImproperRecordNo = objWhtsAppPolicy.CheckInputValues(new string[5] { WhatsAppMainMember, policy_Number, Mobile_number, WhatsAppEmail, fileArray });

                                        if (ImproperRecordNo == 0)  // all five variables contain values.
                                        {
                                            objWhtsAppPolicy.UploadPolicyPDF(WhatsAppMainMember, policy_Number, Mobile_number, Email_id, fileArray);
                                        }
                                    }
                                }
                                #endregion
                            }
                            // }
                        }
                        j = j + 100;
                    }
                    else
                    {
                        logger.Info("Record Not Found ");  // if  Record Not found 
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message);
                //throw;
            }
        }
        #endregion

        #region < GetWebsiteHtml >
        private static void GetWebsiteHtml(string url, string strfilene, string PRODUCT_CODE, string PROSPECT_NO)
        {
            try
            {
                //WebRequest request = WebRequest.Create(url);
                //WebResponse response = request.GetResponse();
                //Stream stream = response.GetResponseStream();
                //StreamReader reader = new StreamReader(stream);
                byte[] bytes = null;
                //Encoding.UTF8.GetBytes(reader.ReadToEnd());
                //stream.Dispose();
                //reader.Dispose();
                GenerateArchivePDF(bytes, strfilene, PRODUCT_CODE, PROSPECT_NO);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }
        }
        #endregion

        #region < GenerateArchivePDF >
        protected static void GenerateArchivePDF(byte[] buffer, string strfilene, string PRODUCT_CODE, string PROSPECT_NO)
        {
            try
            {
                int flag = 0;
                string savelocation = Convert.ToString(ConfigurationSettings.AppSettings["pdfsavepath"].Trim());
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                string FileName = strfilene;
                string Inputpath = savelocation + "On_Line\\" + date + @"\";
                if (!Directory.Exists(Inputpath)) //check If Directory Exist or not for specific path
                {
                    Directory.CreateDirectory(Inputpath);
                }
                try
                {

                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(fileArray, Inputpath + FileName);
                    webClient.Dispose();
                    flag = 1;
                    //webClient.DownloadFile("http://172.16.25.6:9083/WSWEBAPP/ReportRedirect?FileName=591AX00000861190116111221.PDF", @"c:\AX00000861.PDF");


                    //                    File.WriteAllBytes(Inputpath + FileName, buffer); // Write Byte Array into Format Of PDF and Save it in specific Location
                }
                catch (Exception ex)
                {
                    //update Exception
                    flag = 0;
                    string Err = "Failure to write PDF";
                    logger.Error(Err.ToString());
                    Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + Err.ToString() + "' where pol_number ='" + policy_Number + "' ;");
                }
                if (flag == 1)
                {
                    string policy_number = policy_Number;
                    string E_mailID = Email_id;
                    logger.Info("Send Mail Function start");
                    string Subject = "Apollo Munich Health Insurance Policy Schedule : " + policy_number;
                    DataTable dtAngelBroker = new DataTable();
                    dtAngelBroker = Policy_schedule_Data("Select Agentcode from tbl_agentcode where agentcode='" + agentcode + "' and isAngelbroker is not null");
                    if (dtAngelBroker.Rows.Count > 0)
                    {
                        Subject = "Apollo Munich Health Insurance Policy Schedule : " + policy_number + " : " + PROSPECT_NO;
                    }
                    sendMail(E_mailID, Inputpath + FileName, Subject, policy_number, Main_member, agentcode, PRODUCT_CODE);
                }
                //sendMail(E_mailID, Inputpath + FileName, "Apollo Munich Health Insurance Policy Schedule : " + policy_number, policy_number, Main_member);
            }
            catch (Exception ex)
            {
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='" + ex.Message + "' where pol_number ='" + policy_Number + "' ;");
            }

        }
        #endregion

        #region < sendMail >
        public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number, string main_member, string Agent, string PRODUCT_CODE)
        {
            try
            {
                string strPath = string.Empty;
                int flag = 0;
                if (Agent == "80157608" || Agent == "80189584" || Agent == "80180174" || Agent == "80180362")
                {
                    //strPath = Convert.ToString(ConfigurationSettings.AppSettings["PolicyBazaar"].Trim());
                    strPath = Convert.ToString(ConfigurationSettings.AppSettings["AMHI"].Trim());
                    //flag = 1;
                    flag = 1;
                }
                if (PRODUCT_CODE == "34011" || PRODUCT_CODE == "34012" || PRODUCT_CODE == "34013" || PRODUCT_CODE == "34014")
                {
                    flag = 2;
                    strPath = Convert.ToString(ConfigurationSettings.AppSettings["HDFCofflinePath"].Trim());
                }
                else
                {
                    DataTable agentcodeHDFCdt = new DataTable();
                    agentcodeHDFCdt = Policy_schedule_Data("Select Agentcode from tbl_agentcode where agentcode='" + Agent + "' and ISEMAIL=0");
                    if (agentcodeHDFCdt.Rows.Count > 0)
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["HDFCofflinePath"].Trim());
                        flag = 2;
                    }
                    else
                    {
                        strPath = Convert.ToString(ConfigurationSettings.AppSettings["AMHI"].Trim());
                    }
                }

                // Get Body part Of mail
                //string strPath = Convert.ToString(ConfigurationSettings.AppSettings["BuyOnlineHTML"].Trim());
                //string Imagespath = Convert.ToString(ConfigurationSettings.AppSettings["Imagespath"].Trim());

                //string strPath = @"F:\DigitalSignature\DigitalSignature\ops-final.html";  //index.html                
                string strBody = File.ReadAllText(strPath);

                string from = Convert.ToString(ConfigurationSettings.AppSettings["From"].Trim());
                MailMessage message = new MailMessage();
                string msg_from = Convert.ToString(ConfigurationSettings.AppSettings["From"].Trim());
                message.From = new MailAddress(msg_from);
                string MessageTo = mailTo;

                bool agent = false;
                if (Agent != "" && Agent != null)
                {
                    DataTable agentcodedt = new DataTable();
                    agentcodedt = Policy_schedule_Data("Select Agentcode,email_id from tbl_agentcode where agentcode='" + Agent + "' and ISEMAIL=1");
                    if (agentcodedt.Rows.Count > 0)
                    {
                        agent = true;
                        string agentcode = agentcodedt.Rows[0][0].ToString();
                        string Emailaddress = agentcodedt.Rows[0][1].ToString();
                        message.CC.Add(new MailAddress(Emailaddress));
                        // message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));
                    }
                }
                //bool agent = false;
                //if (Agent != "" && Agent != null)
                //{
                //    DataTable agentcodedt = new DataTable();
                //    agentcodedt = Policy_schedule_Data("Select Agentcode,email_id,ISCC,ISBCC from tbl_agentcode where agentcode='" + Agent + "' and ISEMAIL=1");
                //    if (agentcodedt.Rows.Count > 0)
                //    {
                //        agent = true;
                //        string agentcode = agentcodedt.Rows[0][0].ToString();
                //        string Emailaddress = agentcodedt.Rows[0][1].ToString();
                //        int isbcc =Convert.ToInt32(agentcodedt.Rows[0][3]);
                //        if (isbcc == 1)
                //            message.Bcc.Add(new MailAddress(Emailaddress));
                //        else
                //            message.CC.Add(new MailAddress(Emailaddress));                       
                //    }
                //}


                //string MessageTo = "vishal.saxena1@apollomunichinsurance.com";
                //message.CC.Add(new MailAddress("mukesh.tiwari@apollomunichinsurance.com"));
                //message.Bcc.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));
                message.To.Add(new MailAddress(MessageTo));
                message.Subject = subject;
                message.Body = strBody;

                #region <BuyOnline >
                if (flag == 0)
                {
                    LinkedResource logo = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img01.jpg");
                    logo.ContentId = "img01";
                    LinkedResource header = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img02.jpg");
                    header.ContentId = "img02";
                    LinkedResource Step_process = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img03.jpg");
                    Step_process.ContentId = "img03";
                    LinkedResource policy = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img04.jpg");
                    policy.ContentId = "img04";
                    LinkedResource newimg1 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img05.jpg");
                    newimg1.ContentId = "img05";
                    LinkedResource newimg2 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img06.jpg");
                    newimg2.ContentId = "img06";
                    LinkedResource newimg3 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img07.jpg");
                    newimg3.ContentId = "img07";
                    LinkedResource newimg4 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_01.png");
                    newimg4.ContentId = "icon_01";
                    LinkedResource newimg5 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_02.png");
                    newimg5.ContentId = "icon_02";
                    LinkedResource newimg6 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_03.png");
                    newimg6.ContentId = "icon_03";
                    LinkedResource newimg7 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_04.png");
                    newimg7.ContentId = "icon_04";
                    LinkedResource newimg8 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\fb.jpg");
                    newimg8.ContentId = "fb";

                    LinkedResource newimg9 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\gp.jpg");
                    newimg9.ContentId = "gp";

                    LinkedResource newimg10 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\in.jpg");
                    newimg10.ContentId = "in";

                    LinkedResource newimg11 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\tw.jpg");
                    newimg11.ContentId = "tw";

                    LinkedResource newimg12 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\ytube.jpg");
                    newimg12.ContentId = "ytube";


                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(logo);
                    av1.LinkedResources.Add(header);
                    av1.LinkedResources.Add(Step_process);
                    av1.LinkedResources.Add(policy);
                    av1.LinkedResources.Add(newimg1);
                    av1.LinkedResources.Add(newimg2);
                    av1.LinkedResources.Add(newimg3);
                    av1.LinkedResources.Add(newimg4);
                    av1.LinkedResources.Add(newimg5);
                    av1.LinkedResources.Add(newimg6);
                    av1.LinkedResources.Add(newimg7);
                    av1.LinkedResources.Add(newimg8);
                    av1.LinkedResources.Add(newimg9);
                    av1.LinkedResources.Add(newimg10);
                    av1.LinkedResources.Add(newimg11);
                    av1.LinkedResources.Add(newimg12);

                    message.AlternateViews.Add(av1);



                }
                #endregion
                #region HDFC Offline
                else if (flag == 2)
                {
                    LinkedResource Img1 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\topn.jpg");
                    Img1.ContentId = "topn";
                    LinkedResource Img2 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\bullet.png");
                    Img2.ContentId = "bullet";
                    LinkedResource Img3 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\arrow2.jpg");
                    Img3.ContentId = "arrow2";
                    LinkedResource Img4 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\icoa.png");
                    Img4.ContentId = "icoa";
                    LinkedResource Img5 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\icob.png");
                    Img5.ContentId = "icob";
                    LinkedResource Img6 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\icoc.png");
                    Img6.ContentId = "icoc";
                    LinkedResource Img7 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic1.png");
                    Img7.ContentId = "ic1";
                    LinkedResource Img8 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic2.png");
                    Img8.ContentId = "ic2";
                    LinkedResource Img9 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic3.png");
                    Img9.ContentId = "ic3";
                    LinkedResource Img10 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic4.png");
                    Img10.ContentId = "ic4";
                    LinkedResource Img11 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic5.png");
                    Img11.ContentId = "ic5";
                    LinkedResource Img12 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic7.png");
                    Img12.ContentId = "ic7";
                    LinkedResource Img13 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic8.png");
                    Img13.ContentId = "ic8";
                    LinkedResource Img14 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\HDFC Offline\\ic11.png");
                    Img14.ContentId = "ic11";


                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(Img1);
                    av1.LinkedResources.Add(Img2);
                    av1.LinkedResources.Add(Img3);
                    av1.LinkedResources.Add(Img4);
                    av1.LinkedResources.Add(Img5);
                    av1.LinkedResources.Add(Img6);
                    av1.LinkedResources.Add(Img7);
                    av1.LinkedResources.Add(Img8);
                    av1.LinkedResources.Add(Img9);
                    av1.LinkedResources.Add(Img10);
                    av1.LinkedResources.Add(Img11);
                    av1.LinkedResources.Add(Img12);
                    av1.LinkedResources.Add(Img13);
                    av1.LinkedResources.Add(Img14);

                    message.AlternateViews.Add(av1);

                }
                #endregion
                #region < Policy Bazaar>
                if (flag == 1)
                {
                    LinkedResource logo = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img01.jpg");
                    logo.ContentId = "img01";
                    LinkedResource header = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img02.jpg");
                    header.ContentId = "img02";
                    LinkedResource Step_process = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img03.jpg");
                    Step_process.ContentId = "img03";
                    LinkedResource policy = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img04.jpg");
                    policy.ContentId = "img04";
                    LinkedResource newimg1 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img05.jpg");
                    newimg1.ContentId = "img05";
                    LinkedResource newimg2 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img06.jpg");
                    newimg2.ContentId = "img06";
                    LinkedResource newimg3 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\img07.jpg");
                    newimg3.ContentId = "img07";
                    LinkedResource newimg4 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_01.png");
                    newimg4.ContentId = "icon_01";
                    LinkedResource newimg5 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_02.png");
                    newimg5.ContentId = "icon_02";
                    LinkedResource newimg6 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_03.png");
                    newimg6.ContentId = "icon_03";
                    LinkedResource newimg7 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\icon_04.png");
                    newimg7.ContentId = "icon_04";
                    LinkedResource newimg8 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\fb.jpg");
                    newimg8.ContentId = "fb";

                    LinkedResource newimg9 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\gp.jpg");
                    newimg9.ContentId = "gp";

                    LinkedResource newimg10 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\in.jpg");
                    newimg10.ContentId = "in";

                    LinkedResource newimg11 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\tw.jpg");
                    newimg11.ContentId = "tw";

                    LinkedResource newimg12 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\AMHI\\ytube.jpg");
                    newimg12.ContentId = "ytube";


                    AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

                    av1.LinkedResources.Add(logo);
                    av1.LinkedResources.Add(header);
                    av1.LinkedResources.Add(Step_process);
                    av1.LinkedResources.Add(policy);
                    av1.LinkedResources.Add(newimg1);
                    av1.LinkedResources.Add(newimg2);
                    av1.LinkedResources.Add(newimg3);
                    av1.LinkedResources.Add(newimg4);
                    av1.LinkedResources.Add(newimg5);
                    av1.LinkedResources.Add(newimg6);
                    av1.LinkedResources.Add(newimg7);
                    av1.LinkedResources.Add(newimg8);
                    av1.LinkedResources.Add(newimg9);
                    av1.LinkedResources.Add(newimg10);
                    av1.LinkedResources.Add(newimg11);
                    av1.LinkedResources.Add(newimg12);

                    message.AlternateViews.Add(av1);


                    DataTable riderdt = new DataTable();
                    string PW = Convert.ToString(ConfigurationSettings.AppSettings["PW"].Trim());
                    string OPT_Dental = Convert.ToString(ConfigurationSettings.AppSettings["OPT_Dental"].Trim());
                    string OPT_Hearing = Convert.ToString(ConfigurationSettings.AppSettings["OPT_Hearing"].Trim());
                    string OPT_OPD = Convert.ToString(ConfigurationSettings.AppSettings["OPT_OPD"].Trim());

                    riderdt = Policy_schedule_Data("select distinct strprodcd from com_pol_prod_dtl@w2e where strpolnbr='" + Policy_Number + "'");
                    //riderdt = Policy_schedule_Data("select strprodcd from com_pol_prod_dtl@w2e where strpolnbr='AA00170940'");
                    if (riderdt.Rows.Count > 0)
                    {
                        for (int i = 0; i < riderdt.Rows.Count; i++)
                        {
                            string ridercode = riderdt.Rows[i][0].ToString();
                            if (ridercode == "11181" || ridercode == "11182" || ridercode == "11183" || ridercode == "11184")
                            {
                                //Base :Policy Wording
                                Attachment attachment = new Attachment(PW);
                                attachment.Name = PW.Substring(PW.LastIndexOf('\\') + 1);
                                message.Attachments.Add(attachment);
                            }
                            else if (ridercode == "11185" || ridercode == "11188" || ridercode == "11222" || ridercode == "11225")
                            {
                                //Outpatient benefit – Rider 1
                                Attachment attachment = new Attachment(OPT_OPD);
                                attachment.Name = OPT_OPD.Substring(OPT_OPD.LastIndexOf('\\') + 1);
                                message.Attachments.Add(attachment);
                            }
                            else if (ridercode == "11186" || ridercode == "11220" || ridercode == "11223" || ridercode == "11226")
                            {
                                //Outpatient Dental Benefit – Rider 2
                                Attachment attachment = new Attachment(OPT_Dental);
                                attachment.Name = OPT_Dental.Substring(OPT_Dental.LastIndexOf('\\') + 1);
                                message.Attachments.Add(attachment);
                            }
                            else if (ridercode == "11187" || ridercode == "11221" || ridercode == "11224" || ridercode == "11227")
                            {
                                //Spectacles & contact lenses – Rider 3 
                                Attachment attachment = new Attachment(OPT_Hearing);
                                attachment.Name = OPT_Hearing.Substring(OPT_Hearing.LastIndexOf('\\') + 1);
                                message.Attachments.Add(attachment);
                            }
                        }
                    }
                }
                #endregion

                message.IsBodyHtml = true;

                if (!string.IsNullOrEmpty(FilePath))
                {
                    Attachment attachment1 = new System.Net.Mail.Attachment(FilePath);
                    attachment1.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
                    message.Attachments.Add(attachment1);
                }

                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient(); // Live                
                client.Send(message);
                logger.Info("Mail Sent : " + mailTo);

                //Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS) values ('" + Policy_Number + "','" + Mobile_number + "','0') ;");
                // Insert_Log(" INSERT INTO tbl_digital_signature_mail_hst (POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,status,SENT_DATE,CREATED_DATE,DMS_IMAGE_ID) ( select POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,1 status,sysdate,CREATED_DATE,DMS_IMAGE_ID from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "') ;");
                // Delete_Log(" Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "' ;");
                Update_Log("update tbl_digital_signature_mailer set status =1 , sent_date = sysdate  where pol_number ='" + Policy_Number + "' ;");
                // Report for End user Console
                Console.WriteLine(+serialno + ". " + "E-mail Send for the : " + Policy_Number + " E-Mail ID - " + mailTo);
                serialno++;
            }
            catch (Exception ex)
            {
                // update Exception For Sending mail To user
                logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
                Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Error in mail sending' where  email_id ='" + mailTo + "' and pol_number ='" + Policy_Number + "' ;");
                //throw;

            }
            finally
            {

            }
        }

        //public static void sendMail(string mailTo, string FilePath, string subject, string Policy_Number, string main_member)
        //{
        //    try
        //    {
        //        // Get Body part Of mail
        //        string strPath = Convert.ToString(ConfigurationSettings.AppSettings["BuyOnlineHTML"].Trim());
        //        //string Imagespath = Convert.ToString(ConfigurationSettings.AppSettings["Imagespath"].Trim());

        //        //string strPath = @"F:\DigitalSignature\DigitalSignature\ops-final.html";  //index.html                
        //        string strBody = File.ReadAllText(strPath);

        //        //strBody = strBody.Replace("Customer", main_member);
        //        //strBody = strBody.Replace("Customer", main_member);

        //        MailMessage message = new MailMessage();
        //        string msg_from = "relay<relay@apollomunichinsurance.com>";
        //        message.From = new MailAddress(msg_from);
        //        string MessageTo = mailTo;
        //        //string MessageTo = "vishal.saxena1@apollomunichinsurance.com";
        //        //message.CC.Add(new MailAddress("mukesh.tiwari@apollomunichinsurance.com"));
        //        //message.CC.Add(new MailAddress("vishal.saxena1@apollomunichinsurance.com"));                
        //        message.To.Add(new MailAddress(MessageTo));
        //        message.Subject = subject;
        //        message.Body = strBody;


        //        LinkedResource logo = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\banner.jpg");
        //        logo.ContentId = "banner";
        //        LinkedResource header = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\logo.png");
        //        header.ContentId = "logo";
        //        LinkedResource Step_process = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\right_process.jpg");
        //        Step_process.ContentId = "right_process";
        //        LinkedResource policy = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\process-2.jpg");
        //        policy.ContentId = "process-2";
        //        LinkedResource newimg1 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\1.png");
        //        newimg1.ContentId = "1";
        //        LinkedResource newimg2 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\fb.jpg");
        //        newimg2.ContentId = "fb";
        //        LinkedResource newimg3 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\gp.jpg");
        //        newimg3.ContentId = "gp";
        //        LinkedResource newimg4 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\in.jpg");
        //        newimg4.ContentId = "in";
        //        LinkedResource newimg5 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\tw.jpg");
        //        newimg5.ContentId = "tw";
        //        LinkedResource newimg6 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\ytube.jpg");
        //        newimg6.ContentId = "ytube";
        //        LinkedResource newimg7 = new LinkedResource("F:\\vishal\\UAT\\DigitalSignature_Axis_PROD_UAT\\DigitalSignature\\DigitalSignature\\bin\\Debug\\BuyOnline\\divider.jpg");
        //        newimg7.ContentId = "divider";                

        //        AlternateView av1 = AlternateView.CreateAlternateViewFromString(strBody, null, MediaTypeNames.Text.Html);

        //        av1.LinkedResources.Add(logo);
        //        av1.LinkedResources.Add(header);
        //        av1.LinkedResources.Add(Step_process);
        //        av1.LinkedResources.Add(policy);
        //        av1.LinkedResources.Add(newimg1);
        //        av1.LinkedResources.Add(newimg2);
        //        av1.LinkedResources.Add(newimg3);
        //        av1.LinkedResources.Add(newimg4);
        //        av1.LinkedResources.Add(newimg5);
        //        av1.LinkedResources.Add(newimg6);
        //        av1.LinkedResources.Add(newimg7);

        //        message.AlternateViews.Add(av1);


        //        message.IsBodyHtml = true;

        //        Attachment attachment = new System.Net.Mail.Attachment(FilePath);

        //        attachment.Name = FilePath.Substring(FilePath.LastIndexOf('\\') + 1);
        //        message.Attachments.Add(attachment);

        //        message.IsBodyHtml = true;
        //        SmtpClient client = new SmtpClient(); // Live                
        //        client.Send(message);
        //        logger.Info("Mail Sent : " + mailTo);

        //        //Insert_Log(" INSERT INTO TBL_DIGITAL_SMS (pol_number,MobileNo,SMS_STATUS) values ('" + Policy_Number + "','" + Mobile_number + "','0') ;");
        //       // Insert_Log(" INSERT INTO tbl_digital_signature_mail_hst (POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,status,SENT_DATE,CREATED_DATE,DMS_IMAGE_ID) ( select POL_NUMBER,USERNAME,EMAIL_ID,MAIN_MEMBER_NAME,DATE_OF_BIRTH,POLICY_START_DATE,POLICY_ISSUE_DATE,POLICY_EXPIRY_DATE,ERROR_LOG,1 status,sysdate,CREATED_DATE,DMS_IMAGE_ID from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "') ;");
        //       // Delete_Log(" Delete from tbl_digital_signature_mailer where pol_number ='" + Policy_Number + "' ;");
        //        Update_Log("update tbl_digital_signature_mailer set status =1 , sent_date = sysdate  where pol_number ='" + Policy_Number + "' ;");
        //        // Report for End user Console
        //        Console.WriteLine(+serialno + ". " + "E-mail Send for the : " + Policy_Number + " E-Mail ID - " + mailTo);
        //        serialno++;
        //    }
        //    catch (Exception ex)
        //    {
        //        // update Exception For Sending mail To user
        //        logger.Error("Error in mail sending for the  : " + mailTo + " Error Log - " + ex.Message);
        //        Update_Log("update tbl_digital_signature_mailer set status =0 , sent_date = sysdate , error_log ='Error in mail sending' where  email_id ='" + mailTo + "' and pol_number ='" + Policy_Number + "' ;");
        //        //throw;

        //    }
        //    finally
        //    {

        //    }
        //}

        #endregion

        #region < Policy_schedule_Data >
        private static DataTable Policy_schedule_Data(string strquery)
        {
            DataTable dtReport = new DataTable();
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = strquery;
                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = Cmd;
                da.Fill(dtReport);
                return dtReport;
            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + strquery);
                //throw;
                return dtReport;
            }

        }
        #endregion

        #region < Update_Log >
        private static void Update_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Insert_Log >
        private static void Insert_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion

        #region < Delete_Log >
        private static void Delete_Log(string sQuery)
        {
            try
            {
                OracleConnection Con = new OracleConnection(ConfigurationSettings.AppSettings["OracleApolloT"].ToString());
                OracleCommand Cmd = new OracleCommand();

                int row = 0;
                sQuery = "Begin " + sQuery + " End;";
                if (Con.State != ConnectionState.Open)
                {
                    Con.Open();
                }
                Cmd = new OracleCommand(sQuery, Con);
                Cmd.CommandType = CommandType.Text;
                row = Cmd.ExecuteNonQuery();
                Con.Close();
                Con.Dispose();

            }
            catch (Exception ex)
            {
                logger.Error("Policy_schedule_Data  : " + ex.Message + "SQL Query " + sQuery);
                // throw;
            }
        }
        #endregion
    }
}
